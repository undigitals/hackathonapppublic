import { prisma } from "../src/generated/prisma-client";
import * as fs from "fs";
import * as csv from "csv-parser";

const main = async () => {
  await prisma.deleteManyUniversities();

  fs.createReadStream(`${__dirname}/data.csv`)
    .pipe(csv())
    .on("data", async row => {
      const university = {
        localRank: Number(row.localRank),
        globalRank: Number(row.globalRank),
        nameEng: row.nameEng,
        nameKor: row.nameKor,
        image: row.image,
        logo: row.logo,
        country: row.country,
        departmentFee: row.departmentFee,
        foreignEmail: row.foreignEmail,
        foreignPhoneNumber: row.foreignPhoneNumber,
        documents: {
          set: row.documents.split("\r\n")
        },
        applyUrl: row.applyUrl,
        deadlineUrl: row.deadlineUrl,
        published: true
      };

      console.log(await prisma.createUniversity(university));
    })
    .on("end", () => {
      console.log("Completed.");
    });
};

main();
