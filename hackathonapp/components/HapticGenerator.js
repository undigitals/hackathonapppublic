import React, { useEffect } from 'react'
import { Animated, Easing, Platform } from 'react-native'
import { feedbackGenerator } from '../utils'

const HapticGenerator = ({ children, style, error }) => {
  let errorAnim = new Animated.Value(0)

  useEffect(() => {
    if (error === true) {
      handleErrorAnimation()
    }
  }, [error])

  const handleErrorAnimation = () => {
    feedbackGenerator(Platform.OS === 'ios' ? 'impact' : 'selection')
    Animated.timing(errorAnim, {
      toValue: 5,
      duration: 50,
    }).start()

    setTimeout(() => {
      Animated.timing(errorAnim, {
        toValue: -10,
        duration: 50,
      }).start()
    }, 50)

    setTimeout(() => {
      Animated.timing(errorAnim, {
        toValue: 0,
        duration: 50,
        easing: Easing.bounce,
      }).start()
    }, 100)
  }

  return (
    <Animated.View
      style={[
        {
          width: '100%',
          transform: [
            {
              translateX: errorAnim,
            },
          ],
        },
        style,
      ]}
    >
      {children}
    </Animated.View>
  )
}

export default HapticGenerator
