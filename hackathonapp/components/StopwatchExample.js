import React, { Component } from 'react'
import { Text, View, TouchableHighlight } from 'react-native'
import { Stopwatch, Timer } from './stopwatch-timer'

class StopwatchExample extends Component {
  constructor(props) {
    super(props)
    this.state = {
      timerStart: false,
      stopwatchStart: false,
      totalDuration: 5000,
      timerReset: false,
      stopwatchReset: false,
    }
  }

  toggleTimer = () => {
    this.setState({ timerStart: !this.state.timerStart, timerReset: false })
  }

  resetTimer = () => {
    this.setState({ timerStart: false, timerReset: true })
  }

  toggleStopwatch = () => {
    this.setState({
      stopwatchStart: !this.state.stopwatchStart,
      stopwatchReset: false,
    })
  }

  resetStopwatch = () => {
    this.setState({ stopwatchStart: false, stopwatchReset: true })
  }

  getFormattedTime = time => {
    this.currentTime = time
  }

  render() {
    return (
      <View>
        <Stopwatch
          laps
          msecs
          start={this.state.stopwatchStart}
          reset={this.state.stopwatchReset}
          options={options}
          getTime={this.getFormattedTime}
        />

        <TouchableHighlight onPress={this.toggleStopwatch}>
          <Text style={{ fontSize: 30 }}>
            {!this.state.stopwatchStart ? 'Start' : 'Stop'}
          </Text>
        </TouchableHighlight>
        <TouchableHighlight onPress={this.resetStopwatch}>
          <Text style={{ fontSize: 30 }}>Reset</Text>
        </TouchableHighlight>
        <Timer
          totalDuration={this.state.totalDuration}
          msecs
          start={this.state.timerStart}
          reset={this.state.timerReset}
          options={options}
          handleFinish={handleTimerComplete}
          getTime={this.getFormattedTime}
        />
        <TouchableHighlight onPress={this.toggleTimer}>
          <Text style={{ fontSize: 30 }}>
            {!this.state.timerStart ? 'Start' : 'Stop'}
          </Text>
        </TouchableHighlight>
        <TouchableHighlight onPress={this.resetTimer}>
          <Text style={{ fontSize: 30 }}>Reset</Text>
        </TouchableHighlight>
      </View>
    )
  }
}

const handleTimerComplete = () => alert('custom completion function')

const options = {
  container: {
    borderWidth: 0.5,
  },
  text: {
    fontSize: 30,
  },
}

export default StopwatchExample
