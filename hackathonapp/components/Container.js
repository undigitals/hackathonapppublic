import React, { useState, useContext, useEffect } from 'react'
import {
  View,
  StyleSheet,
  StatusBar,
  Keyboard,
  LayoutAnimation,
  Platform,
  Modal,
} from 'react-native'
import { ScrollView } from 'react-navigation'
import { MaterialIndicator } from 'react-native-indicators'
import RefreshControl from './RefreshControl'
import { ThemeContext } from '../context'
import constants from '../constants'

const Container = ({
  type,
  outerContStyle,
  contStyle,
  refreshing,
  onRefresh,
  loading,
  children,
  scrollIndicatorStyle,
}) => {
  const { theme } = useContext(ThemeContext)
  const [keyboardHeight, setKeyboardHeight] = useState(0.5)

  useEffect(() => {
    const handleKeyboardDidShow = e => {
      setKeyboardHeight(
        Platform.OS === 'ios'
          ? e.endCoordinates.height
          : e.endCoordinates.height / 3
      )
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    }

    const handleKeyboardDidHide = e => {
      setKeyboardHeight(0)
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    }

    Keyboard.addListener('keyboardDidShow', handleKeyboardDidShow)
    Keyboard.addListener('keyboardDidHide', handleKeyboardDidHide)

    return () => {
      Keyboard.removeListener('keyboardDidShow', handleKeyboardDidShow)
      Keyboard.removeListener('keyboardDidHide', handleKeyboardDidHide)
    }
  }, [])

  return (
    <View
      style={[
        styles.outerCont,
        { backgroundColor: constants[theme].backgroundColor },
        outerContStyle,
      ]}
    >
      <StatusBar
        barStyle={
          Platform.OS === 'android' && theme === 'light'
            ? 'light-content'
            : constants[theme].statusBarStyle
        }
        backgroundColor={constants[theme].backgroundColor}
        translucent={true}
      />
      <Modal
        animationType="fade"
        transparent={true}
        visible={loading || false}
        onRequestClose={() => {}}
      >
        <View
          style={[
            styles.modalCont,
            { backgroundColor: constants[theme].modalContBackground },
          ]}
        >
          <MaterialIndicator
            size={45}
            color={constants[theme].spinnerColor}
            animationDuration={1000}
          />
        </View>
      </Modal>
      {(() => {
        if (type === 'view') {
          return (
            <View
              style={[
                styles.viewCont,
                { backgroundColor: constants[theme].backgroundColor },
                contStyle,
              ]}
            >
              {children}
            </View>
          )
        } else {
          return (
            <ScrollView
              contentContainerStyle={[
                styles.scrollCont,
                { backgroundColor: constants[theme].backgroundColor },
                contStyle,
              ]}
              refreshControl={
                onRefresh && (
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                  />
                )
              }
              indicatorStyle={
                scrollIndicatorStyle ||
                constants[theme].scrollViewIndicatorStyle
              }
            >
              {children}
              <View style={{ height: keyboardHeight }} />
            </ScrollView>
          )
        }
      })()}
    </View>
  )
}

export default Container

const styles = StyleSheet.create({
  outerCont: {
    flexGrow: 1,
  },
  viewCont: {
    flexGrow: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  scrollCont: {
    flexGrow: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  modalCont: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: constants.paddingHorizontal,
  },
})
