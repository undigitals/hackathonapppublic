import React, { useContext } from 'react'
import { StyleSheet } from 'react-native'
import { withNavigation } from 'react-navigation'
import { LinearGradient } from 'expo-linear-gradient'
import { Entypo, AntDesign } from '@expo/vector-icons'
import AppIntroSlider from 'react-native-app-intro-slider'
import i18n from 'i18n-js'
import View from './View'
import Text from './Text'
import { ThemeContext } from '../context'
import { introSlides } from '../utils'
import constants from '../constants'

const IntroSlider = ({ navigation }) => {
  const { theme } = useContext(ThemeContext)

  const renderItem = ({ item, dimensions }) => (
    <LinearGradient
      style={[styles.mainContent, dimensions]}
      colors={item.colors[theme]}
      start={{ x: 0, y: 0.1 }}
      end={{ x: 0.1, y: 1 }}
    >
      <Entypo
        style={[styles.icon]}
        name={item.icon}
        size={200}
        color={constants[theme].sliderIconColor}
      />
      <View style={[styles.textCont]}>
        <Text
          style={[styles.title, { color: constants[theme].sliderTextColor }]}
        >
          {i18n.t(item.title)}
        </Text>
        <Text
          style={[styles.text, { color: constants[theme].sliderTextColor }]}
        >
          {i18n.t(item.text)}
        </Text>
      </View>
    </LinearGradient>
  )

  const renderButton = type => {
    return (
      <View
        style={[
          styles.buttonCircle,
          { backgroundColor: constants[theme].sliderButtonBackgroundColor },
        ]}
      >
        <AntDesign
          name={type === 'done' ? 'check' : 'arrowright'}
          color={constants[theme].sliderButtonIconColor}
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    )
  }

  return (
    <AppIntroSlider
      slides={introSlides}
      renderItem={renderItem}
      renderDoneButton={() => renderButton('done')}
      renderNextButton={() => renderButton('next')}
      onDone={() => {
        navigation.navigate('Login')
      }}
      dotStyle={{ backgroundColor: constants[theme].sliderDotColor }}
      activeDotStyle={{
        backgroundColor: constants[theme].sliderActiveDotColor,
      }}
    />
  )
}

// class IntroSlider extends React.Component {
//   _renderItem = ({ item, dimensions }) => (
//     <ThemeContext.Consumer>
//       {({ theme }) => {
//         return (
//           <LinearGradient
//             style={[styles.mainContent, dimensions]}
//             colors={item.colors[theme]}
//             start={{ x: 0, y: 0.1 }}
//             end={{ x: 0.1, y: 1 }}
//           >
//             <Entypo
//               style={[styles.icon]}
//               name={item.icon}
//               size={200}
//               color={constants[theme].sliderIconColor}
//             />
//             <View style={[styles.textCont]}>
//               <Text
//                 style={[
//                   styles.title,
//                   { color: constants[theme].sliderTextColor },
//                 ]}
//               >
//                 {i18n.t(item.title)}
//               </Text>
//               <Text
//                 style={[
//                   styles.text,
//                   { color: constants[theme].sliderTextColor },
//                 ]}
//               >
//                 {i18n.t(item.text)}
//               </Text>
//             </View>
//           </LinearGradient>
//         )
//       }}
//     </ThemeContext.Consumer>
//   )

//   _renderButton = type => {
//     return (
//       <ThemeContext.Consumer>
//         {({ theme }) => (
//           <View
//             style={[
//               styles.buttonCircle,
//               { backgroundColor: constants[theme].sliderButtonBackgroundColor },
//             ]}
//           >
//             <AntDesign
//               name={type === 'done' ? 'check' : 'arrowright'}
//               color={constants[theme].sliderButtonIconColor}
//               size={24}
//               style={{ backgroundColor: 'transparent' }}
//             />
//           </View>
//         )}
//       </ThemeContext.Consumer>
//     )
//   }
//   render() {
//     const { navigation } = this.props
//     return (
//       <ThemeContext.Consumer>
//         {({ theme }) => {
//           return (
//             <AppIntroSlider
//               slides={introSlides}
//               renderItem={this._renderItem}
//               renderDoneButton={() => this._renderButton('done')}
//               renderNextButton={() => this._renderButton('next')}
//               onDone={() => {
//                 navigation.navigate('Login')
//               }}
//               dotStyle={{ backgroundColor: constants[theme].sliderDotColor }}
//               activeDotStyle={{
//                 backgroundColor: constants[theme].sliderActiveDotColor,
//               }}
//             />
//           )
//         }}
//       </ThemeContext.Consumer>
//     )
//   }
// }

export default withNavigation(IntroSlider)

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  icon: {
    marginVertical: 100,
  },
  textCont: {
    backgroundColor: 'transparent',
    paddingHorizontal: constants.paddingHorizontal,
  },
  text: {
    backgroundColor: 'transparent',
    textAlign: 'center',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 22,
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  },
  buttonCircle: {
    width: 40,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
