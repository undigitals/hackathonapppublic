import React, { useContext } from 'react'
import { StyleSheet, Text, Platform } from 'react-native'
import { ThemeContext } from '../context'
import constants from '../constants'

const ThemedText = ({ style, ...rest }) => {
  const { theme, font } = useContext(ThemeContext)
  return (
    <Text
      style={[
        styles.text,
        {
          color: constants[theme].fontColor,
          fontFamily: font,
        },
        style,
      ]}
      allowFontScaling={false}
      {...rest}
    />
  )
}

export default ThemedText

const styles = StyleSheet.create({
  text: {
    fontWeight: Platform.OS === 'android' ? constants.fontWeightAndroid : null,
  },
})
