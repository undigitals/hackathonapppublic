import React, { useContext } from 'react'
import { Entypo } from '@expo/vector-icons'
import { ThemeContext } from '../context'
import constants from '../constants'

const TabBarIcon = ({ name, focused }) => {
  const { theme } = useContext(ThemeContext)
  return (
    <Entypo
      name={name}
      size={26}
      style={{}}
      color={
        focused
          ? constants[theme].activeTintColor
          : constants[theme].inactiveTintColor
      }
    />
  )
}

export default TabBarIcon
