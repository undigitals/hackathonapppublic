import React, { useContext } from 'react'
import { StyleSheet } from 'react-native'
import View from './View'
import Text from './Text'
import ScalableImage from './ScalableImage'
import Touchable from './Touchable'
import { ThemeContext } from '../context'
import constants from '../constants'

const ListItemNotif = ({ onPress, data, showFullContent }) => {
  const { imageSrc, disabled, textPrimary, textSecondary, textDate } = data
  const { theme, font } = useContext(ThemeContext)
  let lines = 4

  return (
    <Touchable
      buttonType="opacity"
      style={styles.cont}
      onPress={onPress}
      disabled={disabled}
      activeOpacity={0.8}
    >
      <View style={styles.header}>
        <Text style={[styles.textPrimary, { fontFamily: font }]}>
          {textPrimary}
        </Text>
        <Text style={[styles.textDate, { fontFamily: font }]}>{textDate}</Text>
      </View>

      {imageSrc ? (
        <View style={styles.imageCont}>
          <ScalableImage width={constants.width} source={{ uri: imageSrc }} />
        </View>
      ) : null}

      <View style={styles.content}>
        <View
          style={[
            styles.contentInnerCont,
            { ...constants[theme].listItemBorderBottom },
          ]}
        >
          <Text
            style={[styles.textSecondary, { fontFamily: font }]}
            numberOfLines={showFullContent === true ? null : lines}
            ellipsizeMode="tail"
          >
            {textSecondary}
          </Text>
        </View>
      </View>
    </Touchable>
  )
}

export default ListItemNotif

const styles = StyleSheet.create({
  cont: {
    width: '100%',
  },
  header: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
    paddingTop: 10,
    paddingBottom: 5,
  },
  textPrimary: {
    fontSize: 15,
    fontWeight: '700',
  },
  textDate: {
    fontSize: 12,
    marginTop: 2,
  },
  imageCont: {
    paddingVertical: 5,
  },
  content: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
  },
  contentInnerCont: {
    paddingTop: 5,
    paddingBottom: 10,
  },
  textSecondary: {
    fontSize: 13,
  },
})
