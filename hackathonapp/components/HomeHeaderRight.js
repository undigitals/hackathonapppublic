import React, { useContext } from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation'
import { useSelector } from 'react-redux'
import { ThemeContext } from '../context'
import Icon from './Icon'
import View from './View'
import IconWithBadge from './IconWithBadge'
import constants from '../constants'

const HomeHeaderRight = ({ navigation, to }) => {
  const { theme, changeTheme } = useContext(ThemeContext)
  const { notifs } = useSelector(({ notifs }) => ({ notifs }))
  return (
    <View style={styles.cont}>
      <TouchableOpacity
        onPress={() => {
          if (to === 'deposit') {
            navigation.navigate('Deposit')
          } else {
            navigation.navigate('Notifs')
          }
        }}
        // onLongPress={() => {
        //   if (theme === 'light') {
        //     changeTheme('dark')
        //   } else {
        //     changeTheme('light')
        //   }
        // }}
        style={{ paddingHorizontal: constants.paddingHorizontal }}
      >
        <IconWithBadge
          name={to === 'deposit' ? 'plus' : 'bell'}
          badgeCount={notifs.totalBadges}
        />
      </TouchableOpacity>
    </View>
  )
}

export default withNavigation(HomeHeaderRight)

const styles = StyleSheet.create({
  cont: {
    // borderWidth: 0.5,
  },
})
