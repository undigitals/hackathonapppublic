import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation'
import { useSelector, useDispatch } from 'react-redux'
import { updateBadgeCount } from '../actions'
import Icon from './Icon'
import View from './View'
import constants from '../constants'

const HomeHeaderLeft = ({ navigation, to = 'videoMessages' }) => {
  const { notifs } = useSelector(({ notifs }) => ({ notifs }))
  const dispatch = useDispatch()

  return (
    <View style={styles.cont}>
      <TouchableOpacity
        onPress={() => {
          if (to === 'newRelative') {
            navigation.navigate('RelativeNew')
          } else {
            navigation.navigate('Bookmarks')
          }
        }}
        style={{ paddingHorizontal: constants.paddingHorizontal }}
      >
        <Icon name={to === 'newRelative' ? 'add-user' : 'pin'} />
      </TouchableOpacity>
    </View>
  )
}

export default withNavigation(HomeHeaderLeft)

const styles = StyleSheet.create({
  cont: {
    // borderWidth: 0.5,
  },
})
