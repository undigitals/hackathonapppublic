import React, { useContext } from 'react'
import { StyleSheet, Image, Platform } from 'react-native'
import { withNavigation } from 'react-navigation'
import i18n from 'i18n-js'
import View from './View'
import Text from './Text'
import Touchable from './Touchable'
import Icon from './Icon'
import { Ionicons, AntDesign, Entypo } from '@expo/vector-icons'
import { MaterialIndicator } from 'react-native-indicators'
import { ThemeContext } from '../context'
import constants from '../constants'

const ListItem = ({
  type,
  data,
  onPress,
  onLongPress,
  selected,
  navigaiton,
  text,
  valid = true,
  loading,
  textPrimaryStyle,
}) => {
  const { theme } = useContext(ThemeContext)
  return (
    <View style={styles.outerCont}>
      <Touchable
        buttonType="highlight"
        onPress={onPress}
        onLongPress={onLongPress}
        style={[
          styles.touchableCont,
          { opacity: type === 'paymentMethod' && !valid ? 0.5 : 1 },
        ]}
        disabled={!valid}
      >
        <View
          style={[
            styles.cont,
            // { ...constants[theme].listItemBorderBottom }
          ]}
        >
          {(() => {
            if (type === 'service') {
              const { service_logo } = data
              let imageSrc = ''
              let text = ''
              if (service_logo !== null) {
                imageSrc = service_logo
              }
              return (
                <React.Fragment>
                  <Image
                    source={{ uri: imageSrc }}
                    style={styles.serviceImage}
                  />
                  <View style={styles.serviceTextCont}>
                    <Text
                      style={styles.serviceText}
                      numberOfLines={2}
                      ellipsizeMode="tail"
                    >
                      {data.service_name}
                    </Text>
                  </View>
                </React.Fragment>
              )
            } else if (type === 'help') {
              const { imageSrc, text } = data

              return (
                <React.Fragment>
                  <Image source={imageSrc} style={styles.serviceImage} />
                  <View style={styles.serviceTextCont}>
                    <Text
                      style={styles.serviceText}
                      numberOfLines={2}
                      ellipsizeMode="tail"
                    >
                      {i18n.t(text)}
                    </Text>
                  </View>
                </React.Fragment>
              )
            } else if (type === 'paymentMethod') {
              const { imageSrc, textPrimary, textSecondary } = data
              return (
                <React.Fragment>
                  <Image
                    source={{ uri: imageSrc }}
                    style={styles.depositMethodImage}
                  />
                  <View style={styles.depositMethodTextCont}>
                    <Text
                      style={styles.depositMethodText}
                      numberOfLines={2}
                      ellipsizeMode="tail"
                    >
                      {i18n.t(textPrimary)}
                    </Text>
                    {textSecondary && (
                      <Text
                        style={styles.depositMethodTextSecondary}
                        numberOfLines={2}
                        ellipsizeMode="tail"
                      >
                        {textSecondary}
                      </Text>
                    )}
                  </View>
                  <View style={styles.iconCont}>
                    <Ionicons
                      name={
                        selected
                          ? 'ios-radio-button-on'
                          : 'ios-radio-button-off'
                      }
                      color={selected ? constants.primary : 'rgba(0,0,0,.2)'}
                      size={25}
                      style={styles.radioIcon}
                    />
                  </View>
                </React.Fragment>
              )
            } else if (type === 'contact') {
              return (
                <View
                  style={[styles.contactCont, { opacity: valid ? 1 : 0.5 }]}
                >
                  <View
                    style={[
                      styles.contactIconCont,
                      { borderColor: constants[theme].inactiveTintColor },
                    ]}
                  >
                    {/* {data.imageSrc ? (
                      <Image
                        source={data.imageSrc}
                        style={styles.contactServiceLogo}
                      />
                    ) : null} */}
                    <Entypo
                      name="user"
                      size={30}
                      color={constants[theme].iconColor}
                      style={{}}
                    />
                  </View>
                  <View style={styles.contactTextCont}>
                    <Text
                      style={styles.contactTextName}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {data.name}
                    </Text>
                    <Text
                      style={styles.contactTextNumber}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {data.phoneNumber}
                    </Text>
                    <Text
                      style={styles.contactTextNumber}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {data.email}
                    </Text>
                  </View>
                </View>
              )
            } else if (type === 'flight') {
              const {
                imageSrc,
                flightNumber,
                originName,
                originCity,
                destinationName,
                destinationCity,
                aircraftType,
              } = data
              return (
                <View style={stylesflightrCont}>
                  <View
                    style={[
                      styles.flightImageCont,
                      {
                        backgroundColor: imageSrc ? '#fff' : 'rgba(0,0,0,.04)',
                      },
                    ]}
                  >
                    <Image
                      source={{ uri: imageSrc }}
                      style={styles.flightImage}
                    />
                  </View>
                  <View style={styles.flightTextCont}>
                    <Text
                      style={styles.flightTextPrimary}
                      numberOfLines={2}
                      ellipsizeMode="tail"
                    >
                      {textPrimary}
                    </Text>
                    <Text
                      style={styles.flightTextSecondary}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {textSecondary}
                    </Text>
                  </View>
                  <View style={styles.flightRightCont}>
                    <Text
                      style={styles.flightRightText}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {textRight}
                    </Text>
                  </View>
                </View>
              )
            } else if (type === 'order') {
              const { imageSrc, textPrimary, textSecondary, textRight } = data
              return (
                <View style={styles.orderCont}>
                  <View
                    style={[
                      styles.orderImageCont,
                      {
                        backgroundColor: imageSrc ? '#fff' : 'rgba(0,0,0,.04)',
                      },
                    ]}
                  >
                    <Image source={imageSrc} style={styles.orderImage} />
                  </View>
                  <View style={styles.orderTextCont}>
                    <Text
                      style={styles.orderTextPrimary}
                      numberOfLines={2}
                      ellipsizeMode="tail"
                    >
                      {textPrimary}
                    </Text>
                    <Text
                      style={styles.orderTextSecondary}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {textSecondary}
                    </Text>
                  </View>
                  <View style={styles.orderRightCont}>
                    <Text
                      style={styles.orderRightText}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {textRight}
                    </Text>
                  </View>
                </View>
              )
            } else if (type === 'setting') {
              const { text, icon } = data
              return (
                <View style={styles.settingCont}>
                  <View style={styles.settingIconCont}>
                    <Icon name={icon} size={24} />
                  </View>
                  <View style={styles.settingTextCont}>
                    <Text
                      style={styles.settingText}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {i18n.t(text)}
                    </Text>
                  </View>
                </View>
              )
            } else if (type === 'plain') {
              return (
                <View style={styles.plainCont}>
                  <View style={styles.plainTextCont}>
                    <Text
                      style={[
                        styles.plainText,
                        {
                          color: selected
                            ? constants[theme].selected
                            : constants[theme].fontColor,
                        },
                        textPrimaryStyle,
                      ]}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {text}
                    </Text>
                  </View>
                  <View style={styles.plainIconCont}>
                    {selected ? (
                      <Entypo
                        name="check"
                        color={constants[theme].selected}
                        size={20}
                      />
                    ) : null}
                  </View>
                </View>
              )
            } else if (type === 'lang') {
              const { textPrimary, textSecondary } = data
              return (
                <View style={styles.langCont}>
                  <View style={styles.langTextCont}>
                    <Text
                      style={[
                        styles.langTextPrimary,
                        {
                          color: selected
                            ? constants[theme].selected
                            : constants[theme].fontColor,
                        },
                      ]}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {textPrimary}
                    </Text>
                    <Text
                      style={styles.langTextSecondary}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {textSecondary}
                    </Text>
                  </View>
                  <View style={styles.langIconCont}>
                    {(() => {
                      if (loading && selected) {
                        return (
                          <MaterialIndicator
                            size={20}
                            color={constants[theme].selected}
                            animationDuration={1000}
                          />
                        )
                      } else if (selected) {
                        return (
                          <Entypo
                            name="check"
                            color={constants[theme].selected}
                            size={20}
                          />
                        )
                      } else {
                        return null
                      }
                    })()}
                  </View>
                </View>
              )
            }
          })()}
        </View>
      </Touchable>
    </View>
  )
}

export default withNavigation(ListItem)

const imageWidth = constants.contWidth * 0.16
const imageHeight = imageWidth - 5
const imageMarginRight = constants.paddingHorizontal
const iconWidth = constants.contWidth * 0.1
const textWidth =
  constants.contWidth - (imageWidth + iconWidth + imageMarginRight)

const styles = StyleSheet.create({
  outerCont: {
    width: '100%',
    backgroundColor: 'transparent',
  },
  touchableCont: {
    paddingHorizontal: constants.paddingHorizontal,
    backgroundColor: 'transparent',
  },
  cont: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    backgroundColor: 'transparent',
  },
  iconCont: {
    width: iconWidth,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  // service
  serviceImage: {
    height: imageHeight,
    width: imageWidth,
    resizeMode: 'contain',
    borderColor: 'red',
    marginRight: imageMarginRight,
  },
  serviceTextCont: {
    width: textWidth,
    backgroundColor: 'transparent',
  },
  serviceText: {
    textAlign: 'left',
    fontSize: 15,
    fontWeight: '500',
  },
  // depositMethod
  depositMethodImage: {
    height: imageHeight,
    width: imageWidth,
    resizeMode: 'contain',
    borderColor: 'red',
    marginRight: imageMarginRight,
  },
  depositMethodTextCont: {
    width: textWidth,
    backgroundColor: 'transparent',
  },
  depositMethodText: {
    textAlign: 'left',
    fontSize: 15,
    fontWeight: Platform.OS === 'ios' ? '500' : '200',
  },
  depositMethodTextSecondary: {
    marginTop: 3,
    textAlign: 'left',
    fontSize: 12,
    fontWeight: Platform.OS === 'ios' ? '400' : '200',
  },
  radioIcon: {
    marginBottom: -2,
    backgroundColor: 'transparent',
  },
  // contact
  contactCont: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.1,
    backgroundColor: 'transparent',
  },
  contactProfileCont: {
    height: imageWidth - 5,
    width: imageWidth - 5,
    borderRadius: (imageWidth - 5) / 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: imageMarginRight,
  },
  contactProfileText: {
    fontSize: 18,
  },
  contactTextCont: {
    width: textWidth,
    backgroundColor: 'transparent',
  },
  contactTextName: {
    fontWeight: '600',
    fontSize: 18,
    marginBottom: 3,
  },
  contactTextNumber: {
    fontSize: 14,
  },
  contactIconCont: {
    height: imageWidth - 10,
    width: imageWidth - 10,
    borderRadius: (imageWidth - 10) / 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: imageMarginRight,
    backgroundColor: 'transparent',
    borderWidth: 0.3,
    borderColor: 'transparent',
  },
  contactServiceLogo: {
    height: imageWidth - 5,
    width: imageWidth - 5,
    borderColor: 'red',
  },
  // order
  orderCont: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    // borderWidth: 1,
    paddingVertical: 5,
    backgroundColor: 'transparent',
  },
  orderImageCont: {
    marginRight: imageMarginRight,
    // borderWidth: 0.4,
    borderRadius: (imageWidth - 5) / 2,
    backgroundColor: 'transparent',
  },
  orderImage: {
    height: imageWidth - 5,
    width: imageWidth - 5,
    resizeMode: 'contain',
    borderColor: 'red',
    // borderWidth: 0.3,
  },
  orderTextCont: {
    width: '60%',
    backgroundColor: 'transparent',
  },
  orderTextPrimary: {
    textAlign: 'left',
    fontSize: 15,
    fontWeight: Platform.OS === 'ios' ? '500' : '200',
  },
  orderTextSecondary: {
    textAlign: 'left',
    fontSize: 12,
    fontWeight: Platform.OS === 'ios' ? '300' : '200',
    marginTop: 2,
  },
  orderRightCont: {
    marginLeft: 'auto',
    backgroundColor: 'transparent',
  },
  orderRightText: {
    textAlign: 'right',
    fontSize: 15,
    fontWeight: Platform.OS === 'ios' ? '500' : '200',
  },
  // setting
  settingCont: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  settingTextCont: {
    width: '70%',
    backgroundColor: 'transparent',
  },
  settingText: {
    textAlign: 'left',
    fontSize: 15,
    fontWeight: Platform.OS === 'ios' ? '500' : '200',
  },
  settingIconCont: {
    height: imageWidth - 10,
    borderColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: imageMarginRight,
    backgroundColor: 'transparent',
  },
  // plain
  plainCont: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  plainTextCont: {
    width: '70%',
    backgroundColor: 'transparent',
  },
  plainText: {
    textAlign: 'left',
    fontSize: 15,
  },
  plainIconCont: {
    height: imageWidth - 10,
    borderColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 'auto',
    backgroundColor: 'transparent',
  },
  // lang
  langCont: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  langTextCont: {
    width: '70%',
    backgroundColor: 'transparent',
  },
  langTextPrimary: {
    textAlign: 'left',
    fontSize: 15,
    fontWeight: Platform.OS === 'ios' ? '500' : '200',
  },
  langTextSecondary: {
    textAlign: 'left',
    fontSize: 13,
    fontWeight: Platform.OS === 'ios' ? '300' : '200',
    marginTop: 3,
  },
  langIconCont: {
    height: imageWidth - 10,
    borderColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 'auto',
    backgroundColor: 'transparent',
  },
})
