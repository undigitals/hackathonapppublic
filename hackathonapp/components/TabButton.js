import React, { useContext } from 'react'
import { StyleSheet } from 'react-native'
import { Entypo } from '@expo/vector-icons'
import { ThemeContext } from '../context'
import View from './View'
import Text from './Text'
import constants from '../constants'
import Touchable from './Touchable'

const TabButton = ({ section, focused = false, onPress }) => {
  const { theme } = useContext(ThemeContext)

  const { url, family, name } = section.image
  return (
    <Touchable style={[]} onPress={onPress} buttonType="opacity">
      <View style={[styles.outerCont]}>
        <View
          style={[
            styles.cont,
            {
              backgroundColor: focused
                ? constants[theme].selected
                : constants[theme].selectedRgb,
            },
          ]}
        >
          <Entypo
            name={name}
            size={26}
            style={{}}
            color={
              focused
                ? constants[theme].backgroundColor
                : constants[theme].selected
            }
          />
        </View>
        <Text
          style={[
            {
              color: focused
                ? constants[theme].activeTintColor
                : constants[theme].inactiveTintColor,
              textAlign: 'center',
            },
          ]}
          numberOfLines={1}
          ellipsizeMode="tail"
        >
          {section.name}
        </Text>
      </View>
    </Touchable>
  )
}

export default TabButton

const styles = StyleSheet.create({
  outerCont: {
    // borderWidth: 0.5,
    // height: 80,
    width: 100,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  cont: {
    height: 60,
    width: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
