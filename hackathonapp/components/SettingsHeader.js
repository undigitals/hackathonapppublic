import React, { useState, useContext } from 'react'
import { StyleSheet, Platform } from 'react-native'
import { withNavigation } from 'react-navigation'
import { useSelector, useDispatch } from 'react-redux'
import {} from '../actions'
import { ThemeContext } from '../context'
import Icon from './Icon'
import View from './View'
import Text from './Text'
import i18n from 'i18n-js'
import constants from '../constants'

const SettingsHeader = ({ navigation }) => {
  const { theme, font } = useContext(ThemeContext)
  const dispatch = useDispatch()
  const { user } = useSelector(({ user }) => ({ user }))
  const { data } = user

  return (
    <View style={styles.cont}>
      <View
        style={[
          styles.avatarCont,
          {
            backgroundColor: constants[theme].settingAvatarBackground,
            borderColor: constants[theme].settingAvatarBorderColor,
          },
        ]}
      >
        <Text
          style={[
            styles.avatarText,
            {
              color: constants[theme].settingAvatarTextColor,
              fontFamily: Platform.OS === 'ios' ? 'GillSans-Light' : font,
            },
          ]}
        >{`${data.first_name ? data.first_name.slice(0, 1).toUpperCase() : ''}${
          data.last_name ? data.last_name.slice(0, 1).toUpperCase() : ''
        }`}</Text>
      </View>
      {data.first_name && (
        <View style={[styles.infoCont]}>
          <Text
            style={[styles.name]}
          >{`${data.first_name} ${data.last_name}`}</Text>
          <Text style={[styles.email]}>{data.email}</Text>
          {/* <Text style={[styles.email]}>{`${i18n.t('balance')}: ${i18n.t(
            'usd'
          )} ${data.user_balance}`}</Text> */}
        </View>
      )}
    </View>
  )
}

export default withNavigation(SettingsHeader)

const styles = StyleSheet.create({
  cont: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  avatarCont: {
    height: 60,
    width: 60,
    borderRadius: 30,
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    marginRight: constants.paddingHorizontal,
  },
  avatarText: {
    fontSize: 28,
  },
  infoCont: {
    width: constants.contWidth - (constants.paddingHorizontal + 60),
  },
  name: {
    fontSize: 18,
    fontWeight: '600',
  },
  email: {
    fontSize: 18,
    fontWeight: '600',
  },
})
