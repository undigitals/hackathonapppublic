import React, { useContext } from 'react'
import { View } from 'react-native'
import { ThemeContext } from '../context'
import constants from '../constants'

const ThemedView = ({ style, ...rest }) => {
  const { theme, font } = useContext(ThemeContext)
  return (
    <View
      {...rest}
      style={[
        {
          backgroundColor: constants[theme].backgroundColor,
          borderColor: constants[theme].fontColor,
        },
        style,
      ]}
    />
  )
}

export default ThemedView
