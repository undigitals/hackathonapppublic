import React, { useContext } from 'react'
import { StyleSheet, Platform } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Video } from 'expo-av'
import { LinearGradient } from 'expo-linear-gradient'
import i18n from 'i18n-js'
import View from './View'
import Text from './Text'
import Touchable from './Touchable'
import Icon from './Icon'
import Row from './Row'
import { ThemeContext } from '../context'
import constants from '../constants'
import moment from 'moment'

const ListItemFlight = ({ type, data, onPress, onLongPress, valid = true }) => {
  const { theme } = useContext(ThemeContext)

  if (type === 'flightInfo') {
    const {
      ident,
      origin,
      originName,
      originCity,
      filed_departuretime,
      destination,
      destinationName,
      destinationCity,
      estimatedarrivaltime,
    } = data

    let departureDate = moment(filed_departuretime).format('YYYY/MM/DD')
    let departureTime = moment(filed_departuretime).format('hh:mm A z')
    let arrivalDate = moment(estimatedarrivaltime).format('YYYY/MM/DD')
    let arrivalTime = moment(estimatedarrivaltime).format('hh:mm A z')

    return (
      <View style={styles.outerCont}>
        <Touchable
          buttonType="highlight"
          onPress={onPress}
          style={[styles.touchableCont, { opacity: valid ? 1 : 0.5 }]}
          disabled={!valid}
        >
          <View style={[styles.cont, { ...constants[theme].listItemBorder }]}>
            <View style={[styles.flightNumCont]}>
              <Icon name="aircraft-take-off" />
              <Text style={styles.flightNum}>{ident}</Text>
              <Icon name="aircraft-landing" />
            </View>
            <Row left={origin} right={destination} type="bold" />
            <Row left={originName} right={destinationName} />
            <Row left={originCity} right={destinationCity} />
            <Row left={departureTime} right={arrivalTime} type="semi-bold" />
            <Row left={departureDate} right={arrivalDate} />
          </View>
        </Touchable>
      </View>
    )
  } else if (type === 'videoMsg') {
    const {
      id,
      user_id,
      flight_id,
      from,
      to,
      flight_time,
      flight_status,
      text,
      video_url,
      created_at,
      updated_at,
    } = data

    let date = `${moment(created_at).format('YYYY/MM/DD')} ${moment(
      created_at
    ).format('hh:mm A z')}`

    return (
      <View style={[styles.videoMsgOuterCont]}>
        <Touchable
          buttonType="highlight"
          onPress={onPress}
          onLongPress={onLongPress}
          style={[
            styles.videoMsgTouchableCont,
            {
              opacity: valid ? 1 : 0.5,
            },
          ]}
          disabled={!valid}
        >
          <View
            style={[
              styles.videoMsgCont,
              { ...constants[theme].listItemBorderBottom },
            ]}
          >
            <LinearGradient
              colors={constants[theme].videoMsgVideoBackground}
              style={[styles.videoCont]}
            >
              <Video
                source={{ uri: `https://landed.co.kr/${video_url}` }}
                rate={1.0}
                volume={1.0}
                isMuted={true}
                resizeMode="cover"
                style={[
                  styles.video,
                  { borderColor: constants[theme].galleryBackgroundColor },
                ]}
              />
            </LinearGradient>
            <View style={[styles.videoMsgInfoCont]}>
              <Row left={`${i18n.t('from')}:`} right={from} />
              <Row left={`${i18n.t('to')}:`} right={to} />
              <Row left={`${i18n.t('recordDate')}:`} right={date} />
            </View>
          </View>
        </Touchable>
      </View>
    )
  }
}

// {
//   "id": 1,
//   "user_id": 1,
//   "flight_id": "UZsB333",
//   "from": "Tashkent",
//   "to": "Dubai",
//   "flight_time": "2019-11-09 22:00:00",
//   "flight_status": 0,
//   "text": "Alvido",
//   "video_url": "files/1-file-1573320983.mp4",
//   "created_at": "2019-11-09 17:36:23",
//   "updated_at": "2019-11-09 17:36:23"
// }

export default withNavigation(ListItemFlight)

const styles = StyleSheet.create({
  videoMsgOuterCont: {
    width: '100%',
    backgroundColor: 'transparent',
    // borderWidth: 0.5,
  },
  videoMsgTouchableCont: {
    paddingHorizontal: constants.paddingHorizontal,
    // borderWidth: 0.5,
  },
  videoMsgCont: {
    paddingVertical: constants.paddingHorizontal / 1.5,
    flexDirection: 'row',
    backgroundColor: 'transparent',
  },
  videoCont: {
    height: constants.pictureCardHeight,
    width: constants.pictureCardWidth,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    borderRadius: 2,
    // borderWidth: 0.5,
  },
  video: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
    borderWidth: 0.3,
    borderColor: 'transparent',
    borderRadius: 2,
    // borderWidth: 0.5,
  },
  videoMsgInfoCont: {
    // borderWidth: 0.5,
    width:
      constants.contWidth -
      constants.pictureCardWidth -
      constants.paddingHorizontal,
    marginLeft: 'auto',
    backgroundColor: 'transparent',
  },
  outerCont: {
    width: '100%',
    backgroundColor: 'transparent',
    paddingHorizontal: constants.paddingHorizontal,
    marginBottom: constants.paddingHorizontal,
  },
  touchableCont: {
    backgroundColor: 'transparent',
  },
  cont: {
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 8,
    backgroundColor: 'transparent',
    borderRadius: 6,
    // borderWidth: 0.5,
  },
  flightNumCont: {
    backgroundColor: 'transparent',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // borderWidth: 0.5,
  },
  flightNum: {
    fontSize: 18,
  },
})
