import React, { useContext } from 'react'
import { TouchableHighlight, TouchableOpacity } from 'react-native'
import { ThemeContext } from '../context'
import constants from '../constants'

const Touchable = ({ buttonType, children, ...rest }) => {
  const { theme, font } = useContext(ThemeContext)

  if (buttonType === 'opacity') {
    return <TouchableOpacity {...rest}>{children}</TouchableOpacity>
  } else if (buttonType === 'highlight') {
    return (
      <TouchableHighlight
        activeOpacity={1}
        underlayColor={constants[theme].pickerUnderlayColor}
        {...rest}
      >
        {children}
      </TouchableHighlight>
    )
  }
}

export default Touchable
