import React, { useState, useEffect, useContext } from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Animated,
  Easing,
  Image,
  Switch,
} from 'react-native'
import { TextInputMask } from 'react-native-masked-text'
import { MaterialIndicator } from 'react-native-indicators'
import Icon from './Icon'
import Text from './Text'
import Touchable from './Touchable'
import View from './View'
import { ThemeContext } from '../context'
import constants from '../constants'

const Input = ({
  type,
  onPress,
  text,
  maskType,
  maskOptions,
  value,
  outerContStyle,
  prefixContStyle,
  prefixIcon,
  prefixText,
  inputRef,
  allowClear,
  onClear,
  withService,
  serviceLogo,
  withSwitch,
  switchValue,
  withSearch,
  searchLoading,
  onSearch,
  onSwitchValueChange,
  ...rest
}) => {
  const { theme, font } = useContext(ThemeContext)
  const [focused, setFocused] = useState(false)

  const handleInputFocus = () => {
    setFocused(true)
  }

  const handleInputBlur = () => {
    setFocused(false)
  }

  return (
    <View style={[styles.outerCont, outerContStyle]}>
      <View
        style={[
          styles.cont,
          {
            // borderColor:
            //   type === 'picker'
            //     ? constants[theme].primary
            //     : constants[theme].inputBorderBottomColorInactive,
            borderColor: focused
              ? constants[theme].buttonBorderColor
              : constants[theme].inputBorderBottomColorInactive,
          },
        ]}
      >
        {(() => {
          if (type === 'picker') {
            return (
              <>
                <Touchable
                  buttonType="opacity"
                  onPress={onPress}
                  style={{ flexDirection: 'row' }}
                >
                  <View style={styles.textCont}>
                    <Text
                      style={[styles.inputText, { fontFamily: font }]}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {text}
                    </Text>
                  </View>
                  <View style={styles.rightCont}>
                    <RightIcon
                      name="map-location-filled"
                      disabled={true}
                      color={constants.primary}
                      size={23}
                    />
                  </View>
                </Touchable>
              </>
            )
          } else {
            return (
              <React.Fragment>
                <View style={[styles.textCont]}>
                  <View
                    style={[
                      styles.prefixCont,
                      { marginRight: 5 },
                      prefixContStyle,
                    ]}
                  >
                    {prefixIcon && (
                      <Icon
                        name={prefixIcon}
                        size={20}
                        color={constants[theme].primary}
                      />
                    )}
                    {prefixText !== '' && (
                      <Text
                        style={[
                          styles.prefixText,
                          {
                            color: constants[theme].inputTextColor,
                            fontFamily: font,
                          },
                        ]}
                      >
                        {prefixText}
                      </Text>
                    )}
                  </View>
                  {(() => {
                    if (type === 'masked') {
                      return (
                        <TextInputMask
                          type={maskType}
                          options={maskOptions}
                          ref={inputRef}
                          value={value}
                          autoCapitalize="none"
                          placeholderTextColor={
                            constants[theme].inputPlaceholderColor
                          }
                          secureTextEntry={false}
                          keyboardType="numeric"
                          returnKeyType="done"
                          underlineColorAndroid="transparent"
                          allowFontScaling={false}
                          ellipsizeMode="head"
                          style={[
                            styles.inputText,
                            {
                              color: constants[theme].inputTextColor,
                              fontFamily: font,
                            },
                          ]}
                          onFocus={handleInputFocus}
                          onBlur={handleInputBlur}
                          keyboardAppearance={
                            constants[theme].inputKeyboardAppearance
                          }
                          {...rest}
                        />
                      )
                    } else {
                      return (
                        <TextInput
                          ref={inputRef}
                          value={value}
                          autoCapitalize="none"
                          placeholderTextColor={
                            constants[theme].inputPlaceholderColor
                          }
                          secureTextEntry={false}
                          keyboardType="numeric"
                          returnKeyType="done"
                          underlineColorAndroid="transparent"
                          allowFontScaling={false}
                          ellipsizeMode="head"
                          style={[
                            styles.inputText,
                            {
                              color: constants[theme].inputTextColor,
                              fontFamily: font,
                            },
                          ]}
                          onFocus={handleInputFocus}
                          onBlur={handleInputBlur}
                          keyboardAppearance={
                            constants[theme].inputKeyboardAppearance
                          }
                          {...rest}
                        />
                      )
                    }
                  })()}
                </View>

                <View style={styles.rightCont}>
                  {(() => {
                    if (allowClear && value) {
                      return (
                        <RightIcon
                          name="circle-with-cross"
                          onPress={() => {
                            onClear()
                            handleInputBlur('clear')
                          }}
                          color={constants[theme].inputClearIconColor}
                          size={20}
                        />
                      )
                    } else if (withService) {
                      return <Logo imageSrc={serviceLogo} />
                    } else if (withSearch) {
                      if (searchLoading) {
                        return (
                          <MaterialIndicator
                            size={20}
                            color={constants[theme].iconColor}
                            animationDuration={1000}
                          />
                        )
                      } else {
                        return (
                          <SearchIcon
                            name="magnifying-glass"
                            onPress={() => {
                              onSearch()
                            }}
                            color={constants[theme].iconColor}
                            size={25}
                          />
                        )
                      }
                    } else if (withSwitch) {
                      return (
                        <View style={styles.switchCont}>
                          <Switch
                            style={styles.switch}
                            value={switchValue}
                            onValueChange={onSwitchValueChange}
                            trackColor={{
                              true: constants.primary,
                            }}
                          />
                        </View>
                      )
                    }
                  })()}
                </View>
              </React.Fragment>
            )
          }
        })()}
      </View>
    </View>
  )
}

export default Input

class RightIcon extends React.Component {
  state = {
    scaleAnim: new Animated.Value(0),
  }

  componentDidMount() {
    this.handleScaleAnim(1)
  }

  handleScaleAnim = scaleTo => {
    let { scaleAnim } = this.state
    Animated.timing(scaleAnim, {
      toValue: scaleTo,
      duration: 400,
      easing: Easing.bezier(0.94, -0.41, 0.36, 1.51),
    }).start()
  }

  render() {
    const { scaleAnim } = this.state
    const { onPress, name, disabled = false, color, size } = this.props
    return (
      <TouchableOpacity onPress={onPress} disabled={disabled}>
        <Animated.View
          style={[
            styles.clearCont,
            {
              transform: [{ scale: scaleAnim }],
            },
          ]}
        >
          <Icon name={name} size={size} color={color} />
        </Animated.View>
      </TouchableOpacity>
    )
  }
}

class Logo extends React.Component {
  state = {
    scaleAnim: new Animated.Value(0),
  }

  componentDidMount() {
    this.handleScaleAnim(1)
  }

  handleScaleAnim = scaleTo => {
    let { scaleAnim } = this.state
    Animated.timing(scaleAnim, {
      toValue: scaleTo,
      duration: 400,
      easing: Easing.bezier(0.94, -0.41, 0.36, 1.51),
    }).start()
  }

  render() {
    const { scaleAnim } = this.state
    const { imageSrc } = this.props
    return (
      <Animated.View
        style={[
          styles.serviceLogoCont,
          {
            transform: [{ scale: scaleAnim }],
          },
        ]}
      >
        <Image source={imageSrc} style={styles.serviceLogo} />
      </Animated.View>
    )
  }
}

class SearchIcon extends React.Component {
  state = {
    scaleAnim: new Animated.Value(0),
  }

  componentDidMount() {
    this.handleScaleAnim(1)
  }

  handleScaleAnim = scaleTo => {
    let { scaleAnim } = this.state
    Animated.timing(scaleAnim, {
      toValue: scaleTo,
      duration: 400,
      easing: Easing.bezier(0.94, -0.41, 0.36, 1.51),
    }).start()
  }

  render() {
    const { scaleAnim } = this.state
    const { onPress, name, disabled = false, color, size } = this.props
    return (
      <TouchableOpacity onPress={onPress} disabled={disabled}>
        <Animated.View
          style={[
            styles.clearCont,
            {
              transform: [{ scale: scaleAnim }],
            },
          ]}
        >
          <Icon name={name} size={size} color={color} />
        </Animated.View>
      </TouchableOpacity>
    )
  }
}

const imageWidth = constants.contWidth * 0.16
const imageHeight = imageWidth / 2

const styles = StyleSheet.create({
  outerCont: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
    marginVertical: 5,
  },
  cont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    // borderBottomWidth: constants.inputBorderBottomWidth,
    // borderBottomEndRadius: 1,
    // borderBottomStartRadius: 1,
    borderRadius: 6,
    paddingVertical: 5,
    borderWidth: 1,
    paddingHorizontal: 8,
  },
  textCont: {
    // borderWidth: 0.5,
    width: '90%',
    flexDirection: 'row',
    marginVertical: 4,
  },
  prefixCont: {
    // borderWidth: 0.5,
  },
  prefixText: {
    fontWeight: constants.inputTextFontWeight,
    fontSize: constants.inputTextFontSize,
  },
  inputText: {
    flex: 1,
    fontWeight: constants.inputTextFontWeight,
    fontSize: constants.inputTextFontSize,
    textAlign: 'left',
  },
  rightCont: {
    // borderWidth: 0.5,
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  clearCont: {
    // borderWidth: 0.5,
    marginLeft: 'auto',
    paddingHorizontal: 5,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  serviceLogoCont: {
    marginLeft: 'auto',
    width: imageWidth,
    alignItems: 'flex-end',
  },
  serviceLogo: {
    height: imageHeight - 3,
    width: imageWidth,
    resizeMode: 'contain',
    borderColor: 'red',
  },
  switchCont: {},
  switch: {
    marginRight: -4,
    transform: [{ scaleX: 0.9 }, { scaleY: 0.9 }],
  },
})
