import React, { useContext } from 'react'
import { StyleSheet, SafeAreaView } from 'react-native'
import { BottomTabBar } from 'react-navigation-tabs'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { ThemeContext } from '../context'
import View from './View'
import Touchable from './Touchable'
import constants from '../constants'

const ThemedBottomTabBar = props => {
  const { navigation } = props
  const { theme } = useContext(ThemeContext)
  return (
    <React.Fragment>
      {/* <SafeAreaView>
        <Touchable
          buttonType="opacity"
          activeOpacity={0.8}
          onPress={() => {
            console.log('lkjflkj')
          }}
        >
          <View
            style={[
              styles.buttonCont,
              {
                backgroundColor: constants[theme].floatButtonBackgroundColor,
                ...constants[theme].shadowPrimary,
              },
            ]}
          >
            <MaterialCommunityIcons
              name="email-plus"
              size={30}
              color={constants[theme].floatButtonIconColor}
            />
          </View>
        </Touchable>
      </SafeAreaView> */}
      <BottomTabBar
        {...props}
        activeTintColor={constants[theme].activeTintColor}
        inactiveTintColor={constants[theme].inactiveTintColor}
        style={{
          backgroundColor: constants[theme].backgroundColor,
          borderTopColor: constants[theme].borderColor,
          paddingTop: 3,
        }}
        onTabLongPress={nav => {
          console.log(nav.route.key)
        }}
      />
    </React.Fragment>
  )
}

export default ThemedBottomTabBar

const BUTTON_HEIGHT = 60

const styles = StyleSheet.create({
  buttonCont: {
    position: 'absolute',
    right: constants.paddingHorizontal + 10,
    bottom: constants.paddingHorizontal + 10,
    justifyContent: 'center',
    alignItems: 'center',
    height: BUTTON_HEIGHT,
    width: BUTTON_HEIGHT,
    borderRadius: BUTTON_HEIGHT / 2,
  },
})
