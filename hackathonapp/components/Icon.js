import React, { useContext } from 'react'
import { ThemeContext } from '../context'
import { Entypo } from '@expo/vector-icons'
import constants from '../constants'

const Icon = ({ fomily, ...rest }) => {
  const { theme } = useContext(ThemeContext)
  return <Entypo color={constants[theme].iconColor} size={26} {...rest} />
}

export default Icon
