import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { withNavigation } from 'react-navigation'
import { useSelector, useDispatch } from 'react-redux'
import { searchFlight, searchFlightReset } from '../actions'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import View from './View'
import HapticGenerator from './HapticGenerator'
import Input from './Input'
import Message from './Message'
import constants from '../constants'
import Icon from './Icon'
import Label from './Label'

const FlightSearch = ({ navigation }) => {
  const [flightNum, setFlightNum] = useState('AAL1113')
  const [flightNumMsg, setFlightNumMsg] = useState('')
  const [errorMsg, setErrorMsg] = useState('')

  const dispatch = useDispatch()
  const { flight } = useSelector(({ flight }) => ({ flight }))
  const { isSearchingFlight, searchFlightMessage, searchFlightData } = flight

  useEffect(() => {
    if (searchFlightMessage === 'searchFlightSuccess') {
      navigation.navigate('Flights', { data: searchFlightData })
    } else if (searchFlightMessage !== '') {
      setErrorMsg(searchFlightMessage)
      setTimeout(() => {
        setErrorMsg('')
      }, 2000)
    }
  }, [searchFlightMessage])

  const clearErrors = () => {
    dispatch(searchFlightReset())
    setFlightNumMsg('')
  }

  const handleSearch = () => {
    dispatch(searchFlightReset())
    if (flightNum !== '') {
      dispatch(searchFlight({ flight_id: flightNum }))
    } else {
      setFlightNumMsg(i18n.t('flightNumberRequired'))
    }
  }

  return (
    <View style={[styles.cont]}>
      <View style={[styles.logoCont]}>
        <View style={[styles.logo]}>
          <Icon name="aircraft-take-off" size={100} />
        </View>
      </View>
      <Label text="titleFlightSearch" />
      <HapticGenerator error={flightNumMsg !== ''}>
        <Input
          value={flightNum}
          onChangeText={value => {
            setFlightNum(value.toUpperCase())
            clearErrors()
          }}
          placeholder={i18n.t('flightNumber')}
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          keyboardType="default"
          returnKeyType="search"
          withSearch={true}
          searchLoading={isSearchingFlight}
          onSearch={() => {
            handleSearch()
            // navigation.navigate('Flights')
          }}
          onSubmitEditing={() => {
            handleSearch()
          }}
        />
        <Message text={flightNumMsg} />
      </HapticGenerator>

      <Message type="message" text={errorMsg && i18n.t(errorMsg)} />
    </View>
  )
}

export default withNavigation(FlightSearch)

const styles = StyleSheet.create({
  cont: {
    // borderWidth: 0.5,
    width: '100%',
  },
  logoCont: {
    paddingHorizontal: constants.paddingHorizontal,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    // height: 80,
    // width: 160,
    // borderWidth: 0.4,
    borderRadius: 10,
    marginVertical: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
