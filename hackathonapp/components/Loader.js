import React, { useContext } from 'react'
import { StyleSheet } from 'react-native'
import {
  DotIndicator,
  SkypeIndicator,
  BallIndicator,
} from 'react-native-indicators'
import { ThemeContext } from '../context'
import View from './View'
import constants from '../constants'

const Loader = ({ type, indicatorType, size, color, contStyle }) => {
  const { theme } = useContext(ThemeContext)
  return (
    <React.Fragment>
      {(() => {
        if (type === 'service') {
          return <View></View>
        } else {
          if (indicatorType === 'skype') {
            return (
              <View style={[styles.cont, contStyle]}>
                <SkypeIndicator
                  size={size || 50}
                  color={color || constants[theme].loaderColor}
                  count={5}
                />
              </View>
            )
          } else if (indicatorType === 'ball') {
            return (
              <View style={[styles.cont, contStyle]}>
                <BallIndicator
                  size={size}
                  color={color || constants[theme].loaderColor}
                  count={12}
                />
              </View>
            )
          } else {
            return (
              <View style={[styles.cont, contStyle]}>
                <DotIndicator
                  size={size}
                  color={color || constants[theme].loaderColor}
                  animationDuration={1000}
                  count={3}
                />
              </View>
            )
          }
        }
      })()}
    </React.Fragment>
  )
}

export default Loader

const styles = StyleSheet.create({
  cont: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
