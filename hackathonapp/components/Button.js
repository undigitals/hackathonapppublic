import React, { useContext } from 'react'
import { StyleSheet } from 'react-native'
import { ThemeContext } from '../context'
import { LinearGradient } from 'expo-linear-gradient'
import { DotIndicator } from 'react-native-indicators'
import View from './View'
import Text from './Text'
import Touchable from './Touchable'
import i18n from 'i18n-js'
import constants from '../constants'

const Button = ({
  type,
  text,
  loading,
  contStyle,
  textStyle,
  translate = true,
  ...rest
}) => {
  const { theme, font } = useContext(ThemeContext)
  return (
    <React.Fragment>
      {(() => {
        if (type === 'plain') {
          return (
            <View
              style={[
                {
                  width: '100%',
                  alignItems: 'center',
                  justifyContent: 'center',
                },
                contStyle,
              ]}
            >
              <Touchable buttonType="opacity" {...rest}>
                <Text style={[textStyle]}>
                  {translate ? i18n.t(text) : text}
                </Text>
              </Touchable>
            </View>
          )
        } else {
          return (
            <View style={[styles.cont, contStyle]}>
              <Touchable buttonType="opacity" disabled={loading} {...rest}>
                <LinearGradient
                  colors={constants[theme].buttonGradient}
                  style={[
                    styles.buttonCont,
                    { borderColor: constants[theme].buttonBorderColor },
                  ]}
                >
                  {loading ? (
                    <DotIndicator
                      size={10}
                      color={constants[theme].buttonTextColor}
                      animationDuration={1000}
                      count={3}
                    />
                  ) : (
                    <React.Fragment>
                      <Text
                        style={[
                          styles.buttonText,
                          {
                            color: constants[theme].buttonTextColor,
                            fontFamily: font,
                          },
                        ]}
                        numberOfLines={1}
                        ellipsizeMode="tail"
                      >
                        {i18n.t(text) || ' '}
                      </Text>
                    </React.Fragment>
                  )}
                </LinearGradient>
              </Touchable>
            </View>
          )
        }
      })()}
    </React.Fragment>
  )
}

export default Button

const styles = StyleSheet.create({
  cont: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
    justifyContent: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  buttonCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: constants.borderRadius,
    borderWidth: 0.5,
    width: '100%',
    height: 50,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '700',
  },
})
