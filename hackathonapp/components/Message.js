import React, { useContext } from 'react'
import { StyleSheet } from 'react-native'
import i18n from 'i18n-js'
import View from './View'
import Text from './Text'
import { ThemeContext } from '../context'
import constants from '../constants'

const Message = ({ type, text, translate = false, contStyle, textStyle }) => {
  let color = constants.danger
  const { theme } = useContext(ThemeContext)

  return (
    <React.Fragment>
      {(() => {
        if (type === 'message') {
          return (
            <View style={[styles.messageCont, contStyle]}>
              <Text
                style={[
                  styles.messageText,
                  {
                    color: constants[theme].messageTextColor,
                    opacity: text ? 1 : 0,
                  },
                  textStyle,
                ]}
              >
                {(() => {
                  if (text) {
                    if (translate) {
                      return i18n.t(text)
                    } else {
                      return text
                    }
                  } else {
                    return '.'
                  }
                })()}
              </Text>
            </View>
          )
        } else {
          return (
            <View style={[styles.inputMessageCont, contStyle]}>
              <Text
                style={[
                  styles.inputMessageText,
                  { color: color, opacity: text ? 1 : 0 },
                  textStyle,
                ]}
              >
                {(() => {
                  if (text) {
                    if (translate) {
                      return i18n.t(text)
                    } else {
                      return text
                    }
                  } else {
                    return '.'
                  }
                })()}
              </Text>
            </View>
          )
        }
      })()}
    </React.Fragment>
  )
}

export default Message

const styles = StyleSheet.create({
  messageCont: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
    marginVertical: 15,
    // borderWidth: 0.5,
  },
  messageText: {
    fontSize: 12,
    textAlign: 'center',
  },
  inputMessageCont: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
    // borderWidth: 0.5,
  },
  inputMessageText: {
    fontSize: 12,
  },
})
