import React from 'react'
import { StyleSheet } from 'react-native'
import Text from './Text'
import Icon from './Icon'
import View from './View'
import constants from '../constants'
import { Entypo } from '@expo/vector-icons'

class RecordingStatus extends React.Component {
  state = {
    tick: false,
  }

  componentDidUpdate(prevProps) {
    if (prevProps.tick !== this.props.tick) {
      if (tick) {
        this.handleTick()
      } else {
        this.setState({ tick: false })
      }
    }
  }

  handleTick = () => {
    setInterval(() => {
      this.setState({ tick: !this.state.tick })
    }, 1000)
  }
  render() {
    return (
      <View style={styles.cont}>
        {this.state.tick ? (
          <Entypo name="controller-record" size={15} color={constants.danger} />
        ) : null}
      </View>
    )
  }
}

export default RecordingStatus

const styles = StyleSheet.create({
  cont: {
    width: 18,
    height: 18,
    borderWidth: 0.5,
    borderColor: '#fff',
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
  },
})
