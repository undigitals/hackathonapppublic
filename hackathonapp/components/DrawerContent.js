import React, { useContext } from 'react'
import { StyleSheet, ScrollView, SafeAreaView } from 'react-native'
import { withNavigation } from 'react-navigation'
import { ThemeContext } from '../context'
import View from './View'
import Button from './Button'
import constants from '../constants'

const DrawerContent = props => {
  const { theme } = useContext(ThemeContext)
  return (
    <SafeAreaView
      style={[
        styles.cont,
        { backgroundColor: constants[theme].backgroundColor },
      ]}
    >
      <ScrollView>
        <View style={[styles.content]}>
          <Button
            type="plain"
            text="Notifs"
            onPress={() => {
              props.navigation.navigate('Home')
              props.navigation.navigate('Notifs')
            }}
          />

          <Button
            type="plain"
            text="Help"
            onPress={() => {
              props.navigation.navigate('Home')
              props.navigation.navigate('Help')
            }}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default withNavigation(DrawerContent)

const styles = StyleSheet.create({
  cont: {
    flex: 1,
    // borderWidth: 2,
  },
  content: {
    // borderWidth: 1,
  },
})
