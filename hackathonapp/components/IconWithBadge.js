import React, { useContext, useEffect, useState } from 'react'
import { StyleSheet, Animated, Easing } from 'react-native'
import { ThemeContext } from '../context'
import View from './View'
import Icon from './Icon'
import Text from './Text'
import constants from '../constants'

const IconWithBadge = ({ name, badgeCount, size, color }) => {
  const { theme } = useContext(ThemeContext)
  let scaleAnim = new Animated.Value(1)

  const handleBadgeCountChange = scaleTo => {
    Animated.timing(scaleAnim, {
      toValue: scaleTo,
      duration: 400,
      easing: Easing.bezier(0.94, -0.41, 0.36, 1.51),
    }).start()
  }

  let badgeContWidth = 18
  if (badgeCount > 10 && badgeCount <= 99) {
    badgeContWidth = 18
  } else if (badgeCount >= 100) {
    badgeContWidth = 24
  }
  return (
    <View
      style={[
        styles.cont,
        {
          marginRight: badgeCount > 0 ? 5 : 0,
          alignItems: badgeCount > 0 ? null : 'flex-end',
        },
      ]}
    >
      <Icon name={name} color={constants[theme].badgeIconColor} />
      {badgeCount > 0 && (
        <Animated.View
          style={[
            styles.badgeCont,
            {
              width: badgeContWidth,
              backgroundColor: constants[theme].backgroundColor,
              transform: [{ scale: scaleAnim }],
            },
          ]}
        >
          <View
            style={[
              styles.badgeContInner,
              { backgroundColor: constants[theme].badgeColor },
            ]}
          >
            <Text
              style={[
                styles.badgeText,
                { color: constants[theme].badgeTextColor },
              ]}
            >
              {badgeCount > 99 ? '99+' : badgeCount}
            </Text>
          </View>
        </Animated.View>
      )}
    </View>
  )
}

// class IconWithBadge extends React.Component {
//   state = {
//     scaleAnim: new Animated.Value(0),
//   }

//   componentDidMount() {
//     setTimeout(() => {
//       this.handleBadgeCountChange(1)
//     }, 300)
//   }

//   handleBadgeCountChange = scaleTo => {
//     let { scaleAnim } = this.state
//     Animated.timing(scaleAnim, {
//       toValue: scaleTo,
//       duration: 400,
//       easing: Easing.bezier(0.94, -0.41, 0.36, 1.51),
//     }).start()
//   }

//   componentDidUpdate(prevProps) {
//     const { badgeCount } = this.props
//     if (prevProps.badgeCount !== badgeCount) {
//       this.handleBadgeCountChange(0)
//       setTimeout(() => {
//         this.handleBadgeCountChange(1)
//       }, 500)
//     }
//   }

//   render() {
//     const { scaleAnim } = this.state
//     const { name, badgeCount, size, color } = this.props
//     let badgeContWidth = 18

//     if (badgeCount > 10 && badgeCount <= 99) {
//       badgeContWidth = 18
//     } else if (badgeCount >= 100) {
//       badgeContWidth = 24
//     }

//     return (
//       <ThemeContext.Consumer>
//         {({ theme }) => (
//           <View
//             style={[
//               styles.cont,
//               {
//                 marginRight: badgeCount > 0 ? 5 : 0,
//                 alignItems: badgeCount > 0 ? null : 'flex-end',
//               },
//             ]}
//           >
//             <Icon name={name} color={constants[theme].badgeIconColor} />
//             {badgeCount > 0 && (
//               <Animated.View
//                 style={[
//                   styles.badgeCont,
//                   {
//                     width: badgeContWidth,
//                     backgroundColor: constants[theme].backgroundColor,
//                     transform: [{ scale: scaleAnim }],
//                   },
//                 ]}
//               >
//                 <View
//                   style={[
//                     styles.badgeContInner,
//                     { backgroundColor: constants[theme].badgeColor },
//                   ]}
//                 >
//                   <Text
//                     style={[
//                       styles.badgeText,
//                       { color: constants[theme].badgeTextColor },
//                     ]}
//                   >
//                     {badgeCount > 99 ? '99+' : badgeCount}
//                   </Text>
//                 </View>
//               </Animated.View>
//             )}
//           </View>
//         )}
//       </ThemeContext.Consumer>
//     )
//   }
// }

export default IconWithBadge

const styles = StyleSheet.create({
  cont: {
    width: 27,
    height: 27,
  },
  badgeCont: {
    position: 'absolute',
    right: -6,
    top: -3,
    width: 21,
    height: 21,
    borderRadius: 10.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgeContInner: {
    width: 18,
    height: 18,
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgeText: {
    fontFamily: null,
    fontSize: 11,
    padding: 0,
    margin: 0,
    textAlign: 'center',
  },
})
