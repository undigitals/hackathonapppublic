import React, { useContext } from 'react'
import { StyleSheet } from 'react-native'
import { FlatList } from 'react-navigation'
import { ThemeContext } from '../context'
import constants from '../constants'

const FlatListCont = ({ data, ...rest }) => {
  const { theme } = useContext(ThemeContext)
  return (
    <FlatList
      data={data}
      style={styles.cont}
      contentContainerStyle={[
        styles.contentCont,
        { backgroundColor: constants[theme].backgroundColor },
      ]}
      {...rest}
    />
  )
}

export default FlatListCont

const styles = StyleSheet.create({
  cont: {
    width: '100%',
  },
  contentCont: {},
})
