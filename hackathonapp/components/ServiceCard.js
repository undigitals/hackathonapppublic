import React, { useContext } from 'react'
import { StyleSheet, TouchableOpacity, Image } from 'react-native'
import i18n from 'i18n-js'
import View from './View'
import Text from './Text'
import { ThemeContext } from '../context'
import { Ionicons } from '@expo/vector-icons'
import constants from '../constants'

const ServiceCard = ({
  type,
  onPress,
  onLongPress,
  serviceName,
  serviceLogoUri,
  savedName,
  savedAccount,
}) => {
  const { theme } = useContext(ThemeContext)

  switch (type) {
    case 'regularService':
      return (
        <TouchableOpacity
          onPress={onPress}
          style={{
            marginVertical: constants.regularServiceCardMarginVertical,
          }}
        >
          <View
            style={[
              styles.regularServiceCardCont,
              {
                borderColor: constants[theme].borderColor,
                // ...constants[theme].shadowPrimary,
              },
            ]}
          >
            <Image
              source={serviceLogoUri}
              style={[styles.regularServiceCardLogo]}
            />
            <Text
              style={styles.regularServiceCardName}
              numberOfLines={1}
              ellipsizeMode="tail"
            >
              {serviceName}
            </Text>
          </View>
        </TouchableOpacity>
      )
    default:
      return null
  }
}

export default ServiceCard

const styles = StyleSheet.create({
  regularServiceCardCont: {
    height: constants.regularServiceCardHeight,
    width: constants.regularServiceCardWidth,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: constants.regularServiceCardBorderRadius,
    borderWidth: constants.regularServiceCardBorderWidth,
    padding: constants.regularServiceCardPadding,
  },
  regularServiceCardLogo: {
    width: constants.regularServiceCardLogoWidth,
    height: constants.regularServiceCardLogoHeight,
    resizeMode: 'contain',
    marginBottom: 30,
  },
  regularServiceCardName: {
    fontFamily: constants.fontRegular,
    textAlign: 'center',
    fontSize: 12,
  },
})
