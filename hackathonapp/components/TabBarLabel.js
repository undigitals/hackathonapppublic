import React, { useContext } from 'react'
import i18n from 'i18n-js'
import { ThemeContext } from '../context'
import Text from './Text'
import constants from '../constants'

const TabBarLabel = ({ text, color }) => {
  const { theme, font } = useContext(ThemeContext)
  return (
    <Text
      style={{
        fontFamily: font,
        textAlign: 'center',
        fontSize: 12,
        color: color,
      }}
    >
      {i18n.t(text)}
    </Text>
  )
}

export default TabBarLabel
