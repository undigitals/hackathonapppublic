import React, { useContext } from 'react'
import { RefreshControl } from 'react-native'
import { ThemeContext } from '../context'
import constants from '../constants'

const ThemedRefreshControl = ({ style, ...rest }) => {
  const { theme } = useContext(ThemeContext)
  return (
    <RefreshControl
      {...rest}
      tintColor={constants[theme].refreshControlTintColor}
    />
  )
}

export default ThemedRefreshControl
