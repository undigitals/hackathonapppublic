import React, { useContext } from 'react'
import { StyleSheet, Platform } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Video } from 'expo-av'
import i18n from 'i18n-js'
import View from './View'
import Text from './Text'
import Touchable from './Touchable'
import Icon from './Icon'
import { ThemeContext } from '../context'
import constants from '../constants'
import moment from 'moment'

const Row = ({ left, right, type }) => {
  let fontWeight = null
  let fontSize = 14
  let opacity = 0.8
  switch (type) {
    case 'semi-bold':
      fontWeight = '700'
      fontSize = 16
      opacity = 1
      break
    case 'bold':
      fontWeight = '800'
      fontSize = 18
      opacity = 1
      break
    default:
      break
  }
  return (
    <View style={styles.rowCont}>
      <Text
        style={[
          styles.left,
          { fontWeight: fontWeight, fontSize: fontSize, opacity: opacity },
        ]}
        numberOfLines={3}
        ellipsizeMode="tail"
      >
        {left}
      </Text>
      {right && (
        <Text
          style={[
            styles.right,
            { fontWeight: fontWeight, fontSize: fontSize, opacity: opacity },
          ]}
          numberOfLines={3}
          ellipsizeMode="tail"
        >
          {right}
        </Text>
      )}
    </View>
  )
}

export default Row

const styles = StyleSheet.create({
  rowCont: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: '100%',
    marginVertical: 3,
    // borderWidth: 0.5,
  },
  left: {
    textAlign: 'left',
    width: '50%',
    fontWeight: Platform.OS === 'ios' ? '500' : '200',
    // borderWidth: 0.5,
  },
  right: {
    textAlign: 'right',
    width: '50%',
    fontWeight: Platform.OS === 'ios' ? '500' : '200',
    // borderWidth: 0.5,
  },
})
