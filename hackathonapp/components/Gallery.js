import React from 'react'
import { StyleSheet } from 'react-native'
import { withNavigation } from 'react-navigation'
import View from './View'
import constants from '../constants'

const Gallery = ({ navigation }) => {
  return <View style={styles.cont}></View>
}

export default withNavigation(Gallery)

const styles = StyleSheet.create({
  cont: {
    borderWidth: 0.5,
  },
})
