import React from 'react'
import { StyleSheet } from 'react-native'
import i18n from 'i18n-js'
import Text from './Text'
import View from './View'
import constants from '../constants'

const Label = ({ type, text, translate = true, contStyle, textStyle }) => {
  return (
    <React.Fragment>
      {(() => {
        if (type === 'input') {
          return (
            <View style={[styles.inputCont, contStyle]}>
              <Text style={[styles.input, textStyle]}>
                {translate ? i18n.t(text) : text}
              </Text>
            </View>
          )
        }
        if (type === 'listFooter') {
          return (
            <View style={[styles.listFooterCont, contStyle]}>
              <Text style={[styles.listFooter, textStyle]}>
                {translate ? i18n.t(text) : text}
              </Text>
            </View>
          )
        } else {
          return (
            <View style={[styles.titleCont, contStyle]}>
              <Text style={[styles.title, textStyle]}>
                {translate ? i18n.t(text) : text}
              </Text>
            </View>
          )
        }
      })()}
    </React.Fragment>
  )
}

export default Label

const styles = StyleSheet.create({
  inputCont: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
    marginTop: 10,
  },
  input: {
    fontSize: 14,
  },
  listFooterCont: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
    marginVertical: 10,
  },
  listFooter: {
    fontSize: 12,
    textAlign: 'center',
  },
  titleCont: {
    width: '100%',
    paddingHorizontal: constants.paddingHorizontal,
    marginVertical: 15,
    borderWidth: 0.1,
  },
  title: {
    fontSize: 18,
    fontWeight: '700',
  },
})
