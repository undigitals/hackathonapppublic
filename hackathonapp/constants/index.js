import { Platform } from 'react-native'
import { Dimensions } from 'react-native'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const os = Platform.OS
const x_padding = 15
const contWidth = width - x_padding * 2

const primary = '#02A5FF'
// const primary = '#000'
const primaryRgba = 'rgba(23, 120, 242, .2)'
const success = '#3ac163'
const danger = '#FF3B30'
const warning = '#CD9909'
const info = '#17a2b8'

const light = {
  backgroundColor: '#fff',
  galleryBackgroundColor: '#fff',
  galleryBackgroundGradient: ['rgba(0,0,0,0.2)', '#fff'],
  videoMsgVideoBackground: ['rgba(0,0,0,0.2)', 'rgba(0,0,0,0.05)'],
  fontColor: '#000',
  headerTintColor: '#000',
  activeTintColor: '#000',
  inactiveTintColor: 'rgba(0,0,0,.3)',
  borderColor: 'rgba(0,0,0,0.2)',
  headerBorderBottomColor: 'rgba(0,0,0,0)',
  statusBarStyle: 'dark-content',
  modalContBackground: 'rgba(255,255,255,.95)',
  spinnerColor: primary,
  refreshControlTintColor: '#000',
  headerPressColorAndroid: primary,
  loaderColor: primary,
  pickerUnderlayColor: 'rgba(0,0,0,.03)',
  iconColor: '#000',
  buttonGradient: [primary, primary],
  buttonTextColor: '#fff',
  buttonBorderColor: primary,
  inputBorderBottomColorInactive: 'rgba(0,0,0,.1)',
  inputBorderBottomColor: primary,
  inputTextColor: primary,
  inputPlaceholderColor: 'rgba(0,0,0,.07)',
  inputClearIconColor: 'rgba(0,0,0,.2)',
  inputSearchIconColor: '#000',
  inputKeyboardAppearance: 'light',
  badgeColor: '#FC0000',
  badgeTextColor: '#fff',
  badgeIconColor: '#000',
  messageTextColor: 'rgba(0,0,0,.5)',
  listItemBorder: {
    borderWidth: 0.3,
    borderColor: 'rgba(0,0,0,.08)',
  },
  listItemBorderBottom: {
    borderBottomWidth: 0.3,
    borderBottomColor: 'rgba(0,0,0,.08)',
  },
  selected: primary,
  selectedRgb: primaryRgba,
  // IntroSlider
  sliderIconColor: primary,
  sliderTextColor: '#000',
  sliderButtonBackgroundColor: 'rgba(0, 0, 0, .15)',
  sliderButtonIconColor: '#000',
  sliderDotColor: 'rgba(0, 0, 0, .15)',
  sliderActiveDotColor: 'rgba(0, 0, 0, .9)',
  // BottomTabBar
  floatButtonBackgroundColor: primary,
  floatButtonIconColor: '#fff',
  shadowPrimary: {
    shadowColor: 'rgba(0,0,0,.3)',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  scrollViewIndicatorStyle: 'dark',
  galleryScrollViewIndicatorStyle: 'white',
  settingAvatarBackground: primary,
  settingAvatarBorderColor: primary,
  settingAvatarTextColor: '#fff',
  tabButtonColor: 'rgba(0,0,0,.5)',
  tabButtonColorInverted: 'rgba(255,255,255,.5)',
}

const dark = {
  backgroundColor: '#000',
  galleryBackgroundColor: 'rgba(0,0,0,1)',
  galleryBackgroundGradient: ['rgba(0,0,0,1)', 'rgba(0,0,0,1)'],
  videoMsgVideoBackground: ['rgba(255,255,255,0.2)', 'rgba(0,0,0,0.05)'],
  fontColor: '#fff',
  headerTintColor: '#fff',
  activeTintColor: '#fff',
  inactiveTintColor: '#888',
  borderColor: 'rgba(255,255,255,.5)',
  headerBorderBottomColor: 'rgba(255,255,255,0)',
  statusBarStyle: 'light-content',
  modalContBackground: 'rgba(0,0,0,.95)',
  spinnerColor: '#fff',
  refreshControlTintColor: '#fff',
  headerPressColorAndroid: '#ccc',
  loaderColor: '#fff',
  pickerUnderlayColor: 'rgba(255,255,255,.08)',
  iconColor: '#fff',
  buttonGradient: ['#000', '#000'],
  buttonTextColor: '#fff',
  buttonBorderColor: '#fff',
  inputBorderBottomColorInactive: 'rgba(255,255,255,.3)',
  inputBorderBottomColor: 'rgba(255,255,255,.7)',
  inputTextColor: 'rgba(255,255,255,.9)',
  inputPlaceholderColor: 'rgba(255,255,255,.1)',
  inputClearIconColor: 'rgba(255,255,255,.3)',
  inputSearchIconColor: '#fff',
  inputKeyboardAppearance: 'dark',
  badgeColor: '#FC0000',
  badgeTextColor: '#fff',
  badgeIconColor: '#fff',
  messageTextColor: 'rgba(255,255,255,.5)',
  listItemBorder: {
    borderWidth: 0.3,
    borderColor: 'rgba(255,255,255,.25)',
  },
  listItemBorderBottom: {
    borderBottomWidth: 0.3,
    borderBottomColor: 'rgba(255,255,255,.25)',
  },
  selected: '#fff',
  selectedRgb: '#fff',
  // IntroSlider
  sliderIconColor: '#fff',
  sliderTextColor: '#fff',
  sliderButtonBackgroundColor: 'rgba(255, 255, 255, .15)',
  sliderButtonIconColor: '#fff',
  sliderDotColor: 'rgba(255, 255, 255, .15)',
  sliderActiveDotColor: 'rgba(255, 255, 255, .9)',
  // BottomTabBar
  floatButtonBackgroundColor: primary,
  floatButtonIconColor: '#fff',
  shadowPrimary: {
    shadowColor: '#fff',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  scrollViewIndicatorStyle: 'white',
  galleryScrollViewIndicatorStyle: 'white',
  settingAvatarBackground: 'transparent',
  settingAvatarBorderColor: 'rgba(255,255,255,.5)',
  settingAvatarTextColor: '#fff',
  tabButtonColor: 'rgba(255,255,255,.5)',
  tabButtonColorInverted: 'rgba(0,0,0,.5)',
}

export default {
  light: light,
  dark: dark,

  paddingHorizontal: x_padding,
  borderRadius: 8,
  inputTextFontSize: 20,
  inputTextFontWeight: '700',
  inputBorderBottomWidth: 1,
  // regularService
  pictureCardHeight: width / 3,
  pictureCardWidth: width / 3,
  pictureCardLogoWidth: ((width - 30) / 2 - 3) / 1.5,
  pictureCardLogoHeight: ((width - 30) / 2 - 3) / 1.5,
  pictureCardBackgroundColor: '#fff',
  pictureCardBorderRadius: 6,
  pictureCardBorderWidth: 0.3,
  pictureCardBorderColor: 'rgba(0,0,0,.1)',
  pictureCardPadding: 10,
  pictureCardMarginVertical: 6 / 2,

  // regularService
  regularServiceCardHeight: (width - 30) / 2 - 3,
  regularServiceCardWidth: (width - 30) / 2 - 3,
  regularServiceCardLogoWidth: ((width - 30) / 2 - 3) / 2.5,
  regularServiceCardLogoHeight: ((width - 30) / 2 - 3) / 2.5,
  regularServiceCardBackgroundColor: '#fff',
  regularServiceCardBorderRadius: 6,
  regularServiceCardBorderWidth: 0.3,
  regularServiceCardBorderColor: 'rgba(0,0,0,.1)',
  regularServiceCardPadding: 10,
  regularServiceCardMarginVertical: 6 / 2,

  primary,
  primaryRgba,
  success,
  danger,
  warning,
  info,
  width,
  height,
  os,
  x_padding,
  contWidth,
  fontWeightAndroid: '200',
  fontRegular: null,
}
