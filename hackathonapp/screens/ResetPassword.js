import React from 'react'
import {} from 'react-native'
import { connect } from 'react-redux'
import { getVerif, getVerifReset } from '../actions'
import i18n from 'i18n-js'
import {
  Button,
  Container,
  Label,
  Input,
  HapticGenerator,
  Message,
  View,
} from '../components'

class ResetPassword extends React.Component {
  state = {
    email: '',
    emailMsg: '',
    password: '',
    passwordMsg: '',
  }

  componentDidUpdate() {
    const { navigation, getVerifMessage } = this.props
    if (getVerifMessage === 'smsSent') {
      navigation.navigate('SendVerif')
    }
  }

  componentWillUnmount() {
    const { getVerifReset } = this.props
  }

  render() {
    const {
      navigation,
      getVerifReset,
      isGettingVerif,
      getVerifMessage,
    } = this.props
    const { email, emailMsg, password, passwordMsg } = this.state
    return (
      <Container>
        <HapticGenerator error={emailMsg !== ''}>
          <Label type="input" text="inputEmail" />
          <Input
            value={email}
            inputRef={ref => (this.emailField = ref)}
            onChangeText={value => {
              this.setState({ email: value, emailMsg: '' })
            }}
            placeholder="user@doman.com"
            maxLength={40}
            prefixContStyle={{ marginRight: 0 }}
            allowClear
            keyboardType="email-address"
            onClear={() => {
              this.setState({ email: '', emailMsg: '' })
            }}
          />
          <Message text={emailMsg} />
        </HapticGenerator>

        {/* <HapticGenerator error={passwordMsg !== ''}>
          <Label type="input" text="inputPassword" />
          <Input
            value={password}
            inputRef={ref => (this.emailField = ref)}
            onChangeText={value => {
              this.setState({ password: value, passwordMsg: '' })
              getVerifReset()
            }}
            placeholder="****"
            maxLength={40}
            prefixContStyle={{ marginRight: 0 }}
            allowClear
            keyboardType="default"
            secureTextEntry={true}
            onClear={() => {
              this.setState({ password: '', passwordMsg: '' })
              getVerifReset()
            }}
          />
          <Message text={passwordMsg} />
        </HapticGenerator> */}

        <Message type="message" text={getVerifMessage} />

        {/* <Button
          type="plain"
          text="forgotPassword"
          onPress={() => {
            navigation.navigate('ResetPassword')
          }}
        /> */}

        <View style={{ marginVertical: 20, width: '100%' }}>
          <Button text="confirm" loading={isGettingVerif} onPress={() => {}} />
        </View>

        <Button
          type="plain"
          text="rememberPassword"
          onPress={() => {
            navigation.navigate('Login')
          }}
        />
      </Container>
    )
  }

  handleGetVerif = () => {
    const { getVerif, lang } = this.props
    const { email } = this.state
    const regex = /[0-9]{8}/g

    if (regex.test(email)) {
      getVerif({
        lang_code: lang,
        phone: `${email}`,
      })
    } else {
      this.setState({ emailMsg: i18n.t('invalidPhoneNumber') })
    }
  }
}

const mapStateToProps = ({ auth, locale }) => ({
  isGettingVerif: auth.isGettingVerif,
  getVerifMessage: auth.getVerifMessage,
  lang: locale.lang,
})

export default connect(mapStateToProps, { getVerif, getVerifReset })(
  ResetPassword
)
