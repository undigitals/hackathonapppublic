import React from 'react'
import {} from 'react-native'
import { connect } from 'react-redux'
import {
  updatePhone,
  getUpdatePhoneVerifReset,
  sendUpdatePhoneVerifReset,
} from '../actions'
import i18n from 'i18n-js'
import {
  Button,
  Container,
  Label,
  Input,
  HapticGenerator,
  Message,
} from '../components'

class ChangePhone extends React.Component {
  state = {
    account: '',
    accountMsg: '',
    verifCode: '',
    verifMsg: '',
  }

  componentDidUpdate() {
    const {
      navigation,
      sendUpdateVerifMessage,
      getUpdateVerifMessage,
    } = this.props

    if (sendUpdateVerifMessage === 'updatePhoneSuccess') {
      setTimeout(() => {
        navigation.goBack(null)
      }, 1000)
    }
  }

  componentWillUnmount() {
    const { getUpdatePhoneVerifReset } = this.props
    getUpdatePhoneVerifReset()
  }

  render() {
    const {
      navigation,
      getUpdatePhoneVerifReset,
      sendUpdatePhoneVerifReset,
      isGettingUpdateVerif,
      isSendingUpdateVerif,
      getUpdateVerifMessage,
      sendUpdateVerifMessage,
    } = this.props
    const { account, verifCode, accountMsg, verifMsg } = this.state
    return (
      <Container>
        <HapticGenerator error={accountMsg !== ''}>
          <Label type="input" text="inputNewPhoneNumber" />
          <Input
            value={account}
            onChangeText={value => {
              this.setState({
                account: value,
                accountMsg: '',
                verifCode: '',
                verifMsg: '',
              })
              getUpdatePhoneVerifReset()
            }}
            placeholder="00000000"
            prefixText="010"
            maxLength={8}
            prefixContStyle={{ marginRight: 0 }}
            allowClear
            onClear={() => {
              this.setState({
                account: '',
                accountMsg: '',
                verifCode: '',
                verifMsg: '',
              })
              getUpdatePhoneVerifReset()
            }}
          />
          <Message text={accountMsg} />
        </HapticGenerator>

        <Message type="message" text={getUpdateVerifMessage} />

        {(() => {
          if (getUpdateVerifMessage === 'updatePhoneVerificationCodeSent') {
            return (
              <React.Fragment>
                <HapticGenerator error={verifMsg !== ''}>
                  <Label type="input" text="inputVerifCode" />
                  <Input
                    value={verifCode}
                    onChangeText={value => {
                      this.setState({ verifCode: value, verifMsg: '' })
                      sendUpdatePhoneVerifReset()
                    }}
                    placeholder="00000"
                    maxLength={5}
                    prefixContStyle={{ marginRight: 0 }}
                    allowClear
                    onClear={() => {
                      this.setState({ verifCode: '', verifMsg: '' })
                      sendUpdatePhoneVerifReset()
                    }}
                    autoFocus={true}
                  />
                  <Message text={verifMsg} />
                </HapticGenerator>
                <Message type="message" text={sendUpdateVerifMessage} />
              </React.Fragment>
            )
          }
        })()}

        <Button
          text={
            getUpdateVerifMessage === 'updatePhoneVerificationCodeSent'
              ? 'verifyNewPhoneSmsCode'
              : 'getNewPhoneSmsCode'
          }
          onPress={() => {
            if (getUpdateVerifMessage === 'updatePhoneVerificationCodeSent') {
              this.handleVerifySmsCode()
            } else {
              getUpdatePhoneVerifReset()
              this.handleGetSmsCode()
            }
          }}
          loading={
            getUpdateVerifMessage === 'updatePhoneVerificationCodeSent'
              ? isSendingUpdateVerif
              : isGettingUpdateVerif
          }
        />
      </Container>
    )
  }

  handleGetSmsCode = () => {
    const { account } = this.state
    const { updatePhone, lang } = this.props
    const regex = /[0-9]{8}/g

    if (regex.test(account)) {
      let data = {
        action: 'verify',
        user_new_phone_number: account,
        lang_code: lang,
      }
      updatePhone(data)
    } else {
      this.setState({ accountMsg: i18n.t('invalidPhoneNumber') })
    }
  }

  handleVerifySmsCode = () => {
    const { verifCode, account } = this.state
    const { updatePhone } = this.props
    const regex = /[0-9]{5}/g

    if (regex.test(verifCode)) {
      let data = {
        action: 'update',
        user_new_phone_number: account,
        sms_code: verifCode,
      }
      updatePhone(data)
    } else {
      this.setState({ verifMsg: i18n.t('invalidVerifCode') })
    }
  }
}

const mapStateToProps = ({ user, locale }) => ({
  isGettingUpdateVerif: user.isGettingUpdateVerif,
  isSendingUpdateVerif: user.isSendingUpdateVerif,
  getUpdateVerifMessage: user.getUpdateVerifMessage,
  sendUpdateVerifMessage: user.sendUpdateVerifMessage,
  lang: locale.lang,
})

export default connect(mapStateToProps, {
  updatePhone,
  getUpdatePhoneVerifReset,
  sendUpdatePhoneVerifReset,
})(ChangePhone)
