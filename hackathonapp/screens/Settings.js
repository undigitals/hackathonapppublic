import React, { useEffect } from 'react'
import { Alert, Share } from 'react-native'
import { NavigationEvents } from 'react-navigation'
import { useSelector, useDispatch } from 'react-redux'
import { removeUser, resetRemoveUser, refreshUser } from '../actions'
import {
  Container,
  HomeHeaderLeft,
  HomeHeaderRight,
  ListItem,
  SettingsHeader,
} from '../components'
import i18n from 'i18n-js'
import { settingOptions, openBrowser, terms, appStore } from '../utils'

const Settings = ({ navigation }) => {
  const dispatch = useDispatch()
  const { user } = useSelector(({ user }) => ({ user }))
  const { userTokenExpired, isRefreshingUser01 } = user

  useEffect(() => {
    if (userTokenExpired === true) {
      navigation.navigate('AuthLoading')
      dispatch(resetRemoveUser())
    }
  }, [userTokenExpired])

  const logout = () => {
    Alert.alert(
      i18n.t('logoutAlertTitle'),
      i18n.t('logoutAlertContent'),
      [
        {
          text: i18n.t('cancel'),
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: i18n.t('yes'),
          onPress: () => {
            dispatch(removeUser())
          },
        },
      ],
      { cancelable: true }
    )
  }

  return (
    <Container
      onRefresh={() => {
        dispatch(refreshUser({ method: 'refresh' }))
      }}
      refreshing={isRefreshingUser01}
    >
      <NavigationEvents
        onWillFocus={payload => {}}
        onDidFocus={payload => {
          dispatch(refreshUser({ method: null }))
        }}
        onWillBlur={payload => {}}
        onDidBlur={payload => {}}
      />
      {user.data && <SettingsHeader />}
      {(() => {
        return settingOptions.map(item => {
          return (
            <ListItem
              key={item.id}
              type="setting"
              onPress={() => {
                if (item.to === 'Terms') {
                  openBrowser(terms)
                } else if (item.to === 'Share') {
                  handleShare()
                } else if (item.to === 'Logout') {
                  logout()
                } else {
                  navigation.navigate(item.to)
                }
              }}
              data={item}
            />
          )
        })
      })()}
    </Container>
  )
}

Settings.navigationOptions = ({}) => {
  return {
    headerRight: <HomeHeaderRight />,
    headerLeft: <HomeHeaderLeft />,
  }
}

const handleShare = () => {
  Share.share(
    {
      title: `${i18n.t(`shareTitle`)}`,
      message: `${i18n.t(`shareMessage`, {
        appStoreLink: `${appStore}`,
      })}`,
    },
    {
      dialogTitle: `${i18n.t(`shareTitle`)}`,
    }
  )
}

export default Settings
