import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import {
  Container,
  HomeHeaderLeft,
  HomeHeaderRight,
  StopwatchExample,
} from '../components'

const Explore = () => {
  return <Container>{/* <StopwatchExample /> */}</Container>
}

export default Explore

Explore.navigationOptions = ({}) => {
  return {
    headerRight: <HomeHeaderRight />,
    headerLeft: <HomeHeaderLeft />,
  }
}
