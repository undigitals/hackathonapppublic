import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container, IntroSlider } from '../components'
import constants from '../constants'

const Intro = ({ navigation }) => {
  useEffect(() => {
    // navigation.navigate('Login')
  }, [])
  return <IntroSlider />
}

export default Intro
