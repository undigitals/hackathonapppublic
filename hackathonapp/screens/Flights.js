import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { searchFlightReset, newMsgUpdate, newMsgReset } from '../actions'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container, ListItemFlight, Label } from '../components'
import constants from '../constants'

const Flights = ({ navigation }) => {
  const dispatch = useDispatch()

  useEffect(() => {
    return () => {
      dispatch(searchFlightReset())
      dispatch(newMsgReset())
    }
  }, [])

  const { data } = navigation.state.params
  // const data = {
  //   ident: 'AAL40',

  //   origin: 'KSNA',
  //   originName: 'John Wayne',
  //   originCity: 'Santa Ana, CA',
  //   filed_departuretime: 1575303300,

  //   destination: 'KORD',
  //   destinationName: "Chicago O'Hare Intl",
  //   destinationCity: 'Chicago, IL',
  //   estimatedarrivaltime: 1575317040,

  //   actualarrivaltime: 0,
  //   actualdeparturetime: 0,
  //   aircrafttype: 'B738',
  //   diverted: '',
  //   filed_airspeed_kts: 393,
  //   filed_airspeed_mach: '',
  //   filed_altitude: 0,
  //   filed_ete: '03:49:00',
  //   filed_time: 1575092748,
  //   route: '',
  // }

  return (
    <Container>
      <Label text="titleFlights" />
      <ListItemFlight
        type="flightInfo"
        onPress={() => {
          dispatch(newMsgUpdate({ flightInfo: data }))
          navigation.navigate('VideoReecord')
        }}
        data={data}
      />
    </Container>
  )
}

export default Flights
