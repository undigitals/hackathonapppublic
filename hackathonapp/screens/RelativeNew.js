import React, { useState, useEffect } from 'react'
import {} from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { addRelative, addRelativeReset } from '../actions'
import i18n from 'i18n-js'
import {
  Button,
  Container,
  Label,
  Input,
  HapticGenerator,
  Message,
  View,
} from '../components'
import { regex, feedbackGenerator } from '../utils'

const RelativeNew = ({ navigation }) => {
  const [name, setName] = useState('')
  const [nameMsg, setNameMsg] = useState('')
  const [phoneNumber, setPhoneNumber] = useState('')
  const [phoneNumberMsg, setPhoneNumberMsg] = useState('')
  const [email, setEmail] = useState('')
  const [emailMsg, setEmailMsg] = useState('')
  const [errorMsg, setErrorMsg] = useState('')

  const dispatch = useDispatch()
  const { relative } = useSelector(({ relative }) => ({ relative }))
  const { isAddingRelative, addRelativeMessage } = relative

  useEffect(() => {
    if (addRelativeMessage === 'addRelativeSuccess') {
      feedbackGenerator('success')
      setErrorMsg(addRelativeMessage)
      setTimeout(() => {
        navigation.goBack(null)
      }, 2000)
    } else if (addRelativeMessage !== '') {
      setErrorMsg(addRelativeMessage)
      setTimeout(() => {
        setErrorMsg('')
      }, 2000)
    }
    return () => {
      dispatch(addRelativeReset())
    }
  }, [addRelativeMessage])

  const clearErrors = () => {
    dispatch(addRelativeReset())
    setNameMsg('')
    setPhoneNumberMsg('')
    setEmailMsg('')
  }

  const handleAddRelative = () => {
    dispatch(addRelativeReset())
    if (name === '' && phoneNumber === '' && email === '') {
      setNameMsg(i18n.t('relativeNameRequired'))
      setPhoneNumberMsg(i18n.t('relativePhoneNumberRequired'))
      setEmailMsg(i18n.t('emailRequired'))
    } else if (name === '') {
      setNameMsg(i18n.t('relativeNameRequired'))
    } else if (phoneNumber === '') {
      setPhoneNumberMsg(i18n.t('relativePhoneNumberRequired'))
    } else if (email === '') {
      setEmailMsg(i18n.t('emailRequired'))
    } else if (regex.email.test(email) === false) {
      setEmailMsg(i18n.t('invalidEmail'))
    } else {
      dispatch(
        addRelative({
          name: name,
          email,
          phone_number: phoneNumber,
        })
      )
    }
  }

  return (
    <Container>
      <HapticGenerator error={nameMsg !== ''}>
        <Label type="input" text="inputRelativeName" />
        <Input
          value={name}
          inputRef={ref => (this.firstNameField = ref)}
          onChangeText={value => {
            setName(value)
            clearErrors()
          }}
          placeholder={i18n.t('relativeNamePlaceholder')}
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="default"
          onClear={() => {
            setName('')
            clearErrors()
          }}
          autoCompleteType="name"
        />
        <Message text={nameMsg} />
      </HapticGenerator>

      <HapticGenerator error={phoneNumberMsg !== ''}>
        <Label type="input" text="inputRelativePhoneNumber" />
        <Input
          value={phoneNumber}
          inputRef={ref => (this.phoneNumberField = ref)}
          onChangeText={value => {
            setPhoneNumber(value)
            clearErrors()
          }}
          placeholder={i18n.t('relativePhoneNumberPlaceholder')}
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="phone-pad"
          onClear={() => {
            setPhoneNumber('')
            clearErrors()
          }}
        />
        <Message text={phoneNumberMsg} />
      </HapticGenerator>

      <HapticGenerator error={emailMsg !== ''}>
        <Label type="input" text="inputEmail" />
        <Input
          value={email}
          inputRef={ref => (this.emailField = ref)}
          onChangeText={value => {
            setEmail(value)
            clearErrors()
          }}
          placeholder="user@doman.com"
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="email-address"
          onClear={() => {
            setEmail('')
            clearErrors()
          }}
          autoCompleteType="email"
        />
        <Message text={emailMsg} />
      </HapticGenerator>

      <Message type="message" text={errorMsg && i18n.t(errorMsg)} />

      <View style={{ marginVertical: 20, width: '100%' }}>
        <Button
          text="addRelative"
          loading={isAddingRelative}
          onPress={() => {
            handleAddRelative()
          }}
        />
      </View>
    </Container>
  )
}

export default RelativeNew
