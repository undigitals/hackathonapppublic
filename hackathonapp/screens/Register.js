import React, { useState, useEffect } from 'react'
import {} from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { register, registerReset } from '../actions'
import i18n from 'i18n-js'
import {
  Button,
  Container,
  Label,
  Input,
  HapticGenerator,
  Message,
  View,
} from '../components'
import { regex } from '../utils'

const Register = ({ navigation }) => {
  const [firstName, setFirstName] = useState('')
  const [firstNameMsg, setFirstNameMsg] = useState('')
  const [lastName, setLastName] = useState('')
  const [lastNameMsg, setLastNameMsg] = useState('')
  const [email, setEmail] = useState('')
  const [emailMsg, setEmailMsg] = useState('')
  const [password, setPassword] = useState('')
  const [passwordMsg, setPasswordMsg] = useState('')
  const [passwordConfirm, setPasswordConfirm] = useState('')
  const [passwordConfirmMsg, setPasswordConfirmMsg] = useState('')
  const [errorMsg, setErrorMsg] = useState('')

  const dispatch = useDispatch()
  const { auth } = useSelector(({ auth }) => ({ auth }))
  const { isRegistering, registerMessage } = auth

  useEffect(() => {
    if (registerMessage === 'registerSuccess') {
      navigation.navigate('App')
    } else if (registerMessage !== '') {
      setErrorMsg(registerMessage)
      setTimeout(() => {
        setErrorMsg('')
      }, 2000)
    }
    return () => {
      dispatch(registerReset())
    }
  }, [registerMessage])

  const clearErrors = () => {
    dispatch(registerReset())
    setFirstNameMsg('')
    setLastNameMsg('')
    setEmailMsg('')
    setPasswordMsg('')
    setPasswordConfirmMsg('')
  }

  const handleRegister = () => {
    dispatch(registerReset())
    if (
      firstName === '' &&
      lastName === '' &&
      email === '' &&
      password === '' &&
      passwordConfirm === ''
    ) {
      setFirstNameMsg(i18n.t('firstNameRequired'))
      setLastNameMsg(i18n.t('lastNameRequired'))
      setEmailMsg(i18n.t('emailRequired'))
      setPasswordMsg(i18n.t('passwordRequired'))
      setPasswordConfirmMsg(i18n.t('passwordConfirmRequired'))
    } else if (firstName === '') {
      setFirstNameMsg(i18n.t('firstNameRequired'))
    } else if (lastName === '') {
      setLastNameMsg(i18n.t('lastNameRequired'))
    } else if (email === '') {
      setEmailMsg(i18n.t('emailRequired'))
    } else if (password === '') {
      setPasswordMsg(i18n.t('passwordRequired'))
    } else if (passwordConfirm === '') {
      setPasswordConfirmMsg(i18n.t('passwordConfirmRequired'))
    } else if (regex.email.test(email) === false) {
      setEmailMsg(i18n.t('invalidEmail'))
    } else if (password !== passwordConfirm) {
      setPasswordMsg(i18n.t('passwordDontMatch'))
      setPasswordConfirmMsg(i18n.t('passwordDontMatch'))
    } else {
      dispatch(
        register({
          email,
          password,
          first_name: firstName,
          last_name: lastName,
        })
      )
    }
  }

  return (
    <Container>
      <HapticGenerator error={firstNameMsg !== ''}>
        <Label type="input" text="inputFirstName" />
        <Input
          value={firstName}
          inputRef={ref => (this.firstNameField = ref)}
          onChangeText={value => {
            setFirstName(value)
            clearErrors()
          }}
          placeholder={i18n.t('firstNamePlaceholder')}
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="default"
          onClear={() => {
            setFirstName('')
            clearErrors()
          }}
          autoCompleteType="name"
        />
        <Message text={firstNameMsg} />
      </HapticGenerator>

      <HapticGenerator error={lastNameMsg !== ''}>
        <Label type="input" text="inputLastName" />
        <Input
          value={lastName}
          inputRef={ref => (this.lastNameField = ref)}
          onChangeText={value => {
            setLastName(value)
            clearErrors()
          }}
          placeholder={i18n.t('lastNamePlaceholder')}
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="default"
          onClear={() => {
            setLastName('')
            clearErrors()
          }}
          autoCompleteType="name"
        />
        <Message text={lastNameMsg} />
      </HapticGenerator>

      <HapticGenerator error={emailMsg !== ''}>
        <Label type="input" text="inputEmail" />
        <Input
          value={email}
          inputRef={ref => (this.emailField = ref)}
          onChangeText={value => {
            setEmail(value)
            clearErrors()
          }}
          placeholder="user@doman.com"
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="email-address"
          onClear={() => {
            setEmail('')
            clearErrors()
          }}
          autoCompleteType="email"
        />
        <Message text={emailMsg} />
      </HapticGenerator>

      <HapticGenerator error={passwordMsg !== ''}>
        <Label type="input" text="inputPassword" />
        <Input
          value={password}
          inputRef={ref => (this.emailField = ref)}
          onChangeText={value => {
            setPassword(value)
            clearErrors()
          }}
          placeholder="****"
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="default"
          secureTextEntry={true}
          onClear={() => {
            setPassword('')
            clearErrors()
          }}
          autoCompleteType="password"
        />
        <Message text={passwordMsg} />
      </HapticGenerator>

      <HapticGenerator error={passwordConfirmMsg !== ''}>
        <Label type="input" text="inputPasswordConfirm" />
        <Input
          value={passwordConfirm}
          inputRef={ref => (this.emailField = ref)}
          onChangeText={value => {
            setPasswordConfirm(value)
            clearErrors()
          }}
          placeholder="****"
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="default"
          secureTextEntry={true}
          onClear={() => {
            setPasswordConfirm('')
            clearErrors()
          }}
          autoCompleteType="password"
        />
        <Message text={passwordConfirmMsg} />
      </HapticGenerator>

      <Message type="message" text={errorMsg && i18n.t(errorMsg)} />

      <View style={{ marginVertical: 20, width: '100%' }}>
        <Button
          text="register"
          loading={isRegistering}
          onPress={() => {
            handleRegister()
          }}
        />
      </View>

      <Button
        type="plain"
        text="orLogin"
        onPress={() => {
          navigation.navigate('Login')
        }}
      />
    </Container>
  )
}

export default Register
