import { AntDesign, Entypo } from '@expo/vector-icons'
import * as FileSystem from 'expo-file-system'
import { LinearGradient } from 'expo-linear-gradient'
import * as MediaLibrary from 'expo-media-library'
import * as Permissions from 'expo-permissions'
import React from 'react'
import { StatusBar, StyleSheet, TouchableOpacity } from 'react-native'
import isIPhoneX from 'react-native-is-iphonex'
import { Header } from 'react-navigation-stack'
import { Container, View } from '../components'
import constants from '../constants'
import { feedbackGenerator } from '../utils'
import { ThemeContext } from '../context'
import Photo from './Photo'

const PHOTOS_DIR = FileSystem.documentDirectory + 'photos'
const VIDEOS_DIR = FileSystem.documentDirectory + 'videos'

class Gallery extends React.Component {
  state = {
    faces: {},
    images: {},
    photos: [],
    videos: [],
    selected: [],
  }

  componentDidMount = async () => {
    StatusBar.setHidden(false, 'slide')
    StatusBar.setBarStyle('light-content', 'fade')
    const photos = await FileSystem.readDirectoryAsync(PHOTOS_DIR)
    const videos = await FileSystem.readDirectoryAsync(VIDEOS_DIR)
    this.setState({ photos, videos })

    console.log(photos, videos)
  }

  componentWillUnmount() {
    const { theme } = this.context
    StatusBar.setHidden(true, 'slide')
    StatusBar.setBarStyle(constants[theme].statusBarStyle, 'fade')
  }

  toggleSelection = (uri, isSelected) => {
    let selected = this.state.selected
    if (isSelected) {
      selected.push(uri)
    } else {
      selected = selected.filter(item => item !== uri)
    }
    this.setState({ selected })
  }

  saveToGallery = async () => {
    const photos = this.state.selected

    if (photos.length > 0) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)

      if (status !== 'granted') {
        throw new Error('Denied CAMERA_ROLL permissions!')
      }

      const promises = photos.map(photoUri => {
        return MediaLibrary.createAssetAsync(photoUri)
      })

      await Promise.all(promises)
      alert("Successfully saved photos to user's gallery!")
    } else {
      alert('No photos to save!')
    }
  }

  renderPhoto = fileName => (
    <Photo
      key={fileName}
      uri={`${PHOTOS_DIR}/${fileName}`}
      onSelectionToggle={this.toggleSelection}
    />
  )

  render() {
    const { theme } = this.context
    return (
      <React.Fragment>
        <View style={[styles.headerCont]}>
          <View style={[styles.topBarControls]}>
            <TouchableOpacity
              style={[
                styles.toggleButton,
                { marginLeft: constants.paddingHorizontal },
              ]}
              onPress={() => {
                feedbackGenerator('selection')
                this.props.navigation.goBack(null)
              }}
            >
              <AntDesign
                style={[styles.topBarButtonIcon]}
                name="close"
                size={32}
                color="white"
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.toggleButton,
                { marginRight: constants.paddingHorizontal },
              ]}
              onPress={() => {
                feedbackGenerator('selection')
                // this.props.navigation.goBack(null)
              }}
            >
              <Entypo
                style={[styles.topBarButtonIcon]}
                name="forward"
                size={32}
                color="white"
              />
            </TouchableOpacity>
          </View>
        </View>
        <LinearGradient
          colors={constants[theme].galleryBackgroundGradient}
          style={{ flex: 1 }}
        >
          <Container
            outerContStyle={{
              backgroundColor: 'transparent',
            }}
            contStyle={[styles.photosCont]}
            scrollIndicatorStyle={
              constants[theme].galleryScrollViewIndicatorStyle
            }
          >
            {this.state.videos.map(item => {
              console.log(item)
              return (
                <Photo
                  type="video"
                  key={item}
                  uri={`${VIDEOS_DIR}/${item}`}
                  onSelectionToggle={this.toggleSelection}
                />
              )
            })}
            {this.state.photos.map(item => {
              console.log(item)
              return (
                <Photo
                  type="photo"
                  key={item}
                  uri={`${PHOTOS_DIR}/${item}`}
                  onSelectionToggle={this.toggleSelection}
                />
              )
            })}
          </Container>
        </LinearGradient>
      </React.Fragment>
    )
  }
}

Gallery.contextType = ThemeContext

export default Gallery

const styles = StyleSheet.create({
  headerCont: {
    // borderWidth: 0.5,
    position: 'absolute',
    top: isIPhoneX ? 30 : 0,
    left: 0,
    width: '100%',
    height: Header.HEIGHT,
    zIndex: 999,
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
  },
  topBarControls: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    height: Header.HEIGHT,
    backgroundColor: 'transparent',
    // borderWidth: 0.5,
    borderColor: '#fff',
  },
  toggleButton: {
    // flex: 0.25,
    width: 40,
    height: 40,
    // marginHorizontal: 2,
    // marginBottom: 10,
    // marginTop: 20,
    // padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 0.5,
    borderColor: '#fff',
    // marginBottom: 3,
  },
  topBarButtonIcon: {
    marginBottom: -3,
  },
  photosCont: {
    backgroundColor: 'transparent',
    paddingTop: Header.HEIGHT,
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    // borderWidth: 2,
    borderColor: 'red',
  },
})
