import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, ScrollView } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container, View, TabButton, ListItem } from '../components'
import constants from '../constants'

const SharingSections = ({ navigation }) => {
  const { service } = navigation.state.params
  const { sections } = service
  const [selected, setSelected] = useState(sections[0])

  useEffect(() => {
    navigation.setParams({ headerTitle: service.name })
  }, [])

  return (
    <Container onRefresh={() => {}} refreshing={false}>
      <View style={[styles.cont]}>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            paddingHorizontal: constants.paddingHorizontal,
            height: 90,
            marginBottom: 10,
            // width: '100%',
          }}
        >
          {sections.map(section => {
            return (
              <TabButton
                key={section.id}
                focused={selected.id === section.id}
                section={section}
                onPress={() => {
                  setSelected(section)
                }}
              />
            )
          })}
        </ScrollView>
      </View>
      {(() => {
        if (selected && selected.products) {
          return selected.products.map(item => {
            let data = {
              imageSrc: { uri: item.image.url },
              textPrimary: item.name,
              textSecondary: item.details,
              textRight: `KRW ${item.price}`,
            }

            return (
              <ListItem
                key={item.id}
                type="order"
                onPress={() => {
                  navigation.navigate('Product', { product: item })
                }}
                data={data}
              />
            )
          })
        }
      })()}
    </Container>
  )
}

export default SharingSections

const styles = StyleSheet.create({
  cont: {
    // borderWidth: 0.5,
    alignItems: 'flex-start',
    height: 100,
    width: '100%',
    marginBottom: 20,
  },
})
