import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { createMsg, createMsgReset } from '../actions'
import { Container, Button } from '../components'
import * as FileSystem from 'expo-file-system'

const UploadMsg = ({ navigation }) => {
  const dispatch = useDispatch()
  const { flight } = useSelector(({ flight }) => ({ flight }))
  const { isCreatingMsg, createMsgMessage, createMsgData } = flight

  const getFileSize = async () => {
    let size = await FileSystem.getInfoAsync(
      'file:///var/mobile/Containers/Data/Application/140D1624-371E-4F3A-861C-F040FD559CCC/Library/Caches/ExponentExperienceData/%2540jobir%252Fgoodfarewellapp/Camera/494CE355-9681-4D50-B412-1FC4262F6072.mov',
      { size: true }
    )
    console.log(size)
  }

  const handleCreateMsg = () => {
    getFileSize()
    const data = new FormData()
    data.append('flight_id', 'UZB333')
    data.append('flight_time', '2019-11-09 22:00:00')
    data.append('text', 'Something')
    data.append('from', 'Tashkent')
    data.append('to', 'Dubai')
    data.append('file', {
      name: 'mobile-video-upload',
      type: 'video/mp4',
      uri:
        'file:///var/mobile/Containers/Data/Application/140D1624-371E-4F3A-861C-F040FD559CCC/Library/Caches/ExponentExperienceData/%2540jobir%252Fgoodfarewellapp/Camera/494CE355-9681-4D50-B412-1FC4262F6072.mov',
    })
    dispatch(createMsg(data))
    // dispatch(
    //   createMsg({
    //     flight_id: 'UZB333',
    //     flight_time: '2019-11-09 22:00:00',
    //     text: 'Something',
    //     from: 'Tashkent',
    //     to: 'Dubai',
    //     file:
    //       'file:///var/mobile/Containers/Data/Application/140D1624-371E-4F3A-861C-F040FD559CCC/Library/Caches/ExponentExperienceData/%2540jobir%252Fgoodfarewellapp/Camera/0BF3779C-ADA0-4604-8D3B-8796BDC1B173.mov',
    //   })
    // )
  }

  return (
    <Container>
      <Button
        text="upload"
        loading={isCreatingMsg}
        onPress={() => {
          handleCreateMsg()
        }}
      />
    </Container>
  )
}

export default UploadMsg
