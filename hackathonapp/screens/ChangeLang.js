import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { changeLang, updateLang, updateLangReset } from '../actions'
import i18n from 'i18n-js'
import { Container, ListItem } from '../components'
import { locales, feedbackGenerator } from '../utils'

const ChangeLang = ({ navigation }) => {
  const dispatch = useDispatch()
  const { locale, user } = useSelector(({ locale, user }) => ({ locale, user }))
  const { updateLangMessage } = user

  useEffect(() => {
    // if (updateLangMessage === 'updateLangSuccess') {
    //   navigation.navigate('AuthLoading')
    // }
    return () => {
      dispatch(updateLangReset())
    }
  }, [updateLangMessage])

  return (
    <Container>
      {locales.map(item => {
        let data = {
          textPrimary: i18n.t(item.lang),
          textSecondary: item.name,
        }
        return (
          <ListItem
            key={item.id}
            type="lang"
            data={data}
            onPress={() => {
              if (locale.lang === item.lang) {
              } else {
                feedbackGenerator('selection')
                dispatch(
                  updateLang({
                    user_lang: item.lang === 'ko' ? 'kr' : item.lang,
                  })
                )
                dispatch(changeLang(item.lang))
                setTimeout(() => {
                  navigation.navigate('AuthLoading')
                }, 100)
              }
            }}
            loading={user.isUpdatingLang}
            selected={locale.lang === item.lang}
          />
        )
      })}
    </Container>
  )
}

export default ChangeLang
