import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container } from '../components'
import constants from '../constants'
import { WebView } from 'react-native-webview'

const ChatBot = ({ navigation }) => {
  const { link } = navigation.state.params
  return (
    <WebView
      source={{
        uri: link,
      }}
      style={{
        marginBottom: 30,
      }}
    />
  )
}

export default ChatBot
