import React from 'react'
import { StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { Video } from 'expo-av'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Button, View, Container } from '../components'
import constants from '../constants'

class VideoPreview extends React.Component {
  render() {
    const { navigation } = this.props
    const { uri } = navigation.state.params
    return (
      <Container>
        <Video
          source={uri}
          rate={1.0}
          volume={1.0}
          isMuted={false}
          resizeMode="cover"
          shouldPlay
          isLooping
          style={{ width: 300, height: 300 }}
        />
      </Container>
    )
  }
}

const mapStateToProps = ({}) => ({})

export default connect(mapStateToProps, {})(VideoPreview)
