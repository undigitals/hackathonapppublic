import React, { useState, useEffect } from 'react'
import * as Constants from 'expo-constants'
import * as GoogleSignIn from 'expo-google-sign-in'
import * as Facebook from 'expo-facebook'
import md5 from 'md5'
import uuid from 'uuid'
import { Platform } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import {
  login,
  loginReset,
  signInWithGoogleReset,
  signInWithGoogle,
  signInWithFacebook,
  signInWithFacebookReset,
} from '../actions'
import i18n from 'i18n-js'
import {
  Button,
  Container,
  Label,
  Input,
  HapticGenerator,
  Message,
  View,
  Alert,
} from '../components'
import { regex } from '../utils'

const isInClient = Constants.appOwnership === 'expo'
if (isInClient) {
  GoogleSignIn.allowInClient()
}
const clientIdForUseInTheExpoClient =
  '603386649315-vp4revvrcgrcjme51ebuhbkbspl048l9.apps.googleusercontent.com'
const yourClientIdForUseInStandalone = Platform.select({
  android:
    '701018637764-shojvq76p609pi7s8sbi7h3eponfh1oe.apps.googleusercontent.com',
  ios:
    '701018637764-98cnjl3kt2ppug0bvejirf4vf15g889n.apps.googleusercontent.com',
})
const clientId = isInClient
  ? clientIdForUseInTheExpoClient
  : yourClientIdForUseInStandalone

const Login = ({ navigation }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [emailMsg, setEmailMsg] = useState('')
  const [passwordMsg, setPasswordMsg] = useState('')
  const [errorMsg, setErrorMsg] = useState('')

  const dispatch = useDispatch()
  const { auth } = useSelector(({ auth }) => ({ auth }))
  const {
    isLoggingIn,
    loginMessage,
    isSigningInWithGoogle,
    signInWithGoogleMessage,
    isSigningInWithFacebook,
    signInWithFacebookMessage,
  } = auth

  useEffect(() => {
    if (loginMessage === 'loginSuccess') {
      navigation.navigate('App')
    } else if (loginMessage !== '') {
      setErrorMsg(loginMessage)
      setTimeout(() => {
        setErrorMsg('')
      }, 2000)
    }
    return () => {
      dispatch(loginReset())
    }
  }, [loginMessage])

  const clearErrors = () => {
    dispatch(loginReset())
    setEmailMsg('')
    setPasswordMsg('')
  }

  const handlelogin = () => {
    dispatch(loginReset())
    if (regex.email.test(email) && password) {
      dispatch(login({ email, password }))
    } else if (email === '' && password === '') {
      setEmailMsg(i18n.t('emailRequired'))
      setPasswordMsg(i18n.t('passwordRequired'))
    } else if (regex.email.test(email) === false) {
      setEmailMsg(i18n.t('emailRequired'))
    } else if (password === '') {
      setPasswordMsg(i18n.t('passwordRequired'))
    }
  }

  useEffect(() => {
    const handleGoogleSignInInitAsync = async () => {
      try {
        await GoogleSignIn.initAsync({
          clientId,
        })
      } catch ({ message }) {
        alert('GoogleSignIn.initAsync(): ' + message)
      }
    }
    handleGoogleSignInInitAsync()
  }, [])

  useEffect(() => {
    if (signInWithGoogleMessage === 'signInWithGoogleSuccess') {
      navigation.navigate('App')
    } else if (signInWithGoogleMessage !== '') {
      setErrorMsg(signInWithGoogleMessage)
      setTimeout(() => {
        setErrorMsg('')
      }, 2000)
    }
    return () => {
      dispatch(signInWithGoogleReset())
    }
  }, [signInWithGoogleMessage])

  const googleSignInAsync = async () => {
    try {
      await GoogleSignIn.askForPlayServicesAsync()
      const { type, user } = await GoogleSignIn.signInAsync()
      if (type === 'success') {
        syncUserWithStateAsync()
      }
    } catch ({ message }) {
      alert('login: Error:' + message)
    }
  }

  const syncUserWithStateAsync = async () => {
    const data = await GoogleSignIn.signInSilentlyAsync()
    console.log({ data })
    if (data) {
      const photoURL = await GoogleSignIn.getPhotoAsync(256)
      const user = await GoogleSignIn.getCurrentUserAsync()

      if (user) {
        const s1 = (md5(uuid.v4()) + md5(uuid.v4())).slice(0, 50)
        const s2 = md5(uuid.v4()).slice(0, 18)
        const something = `${s1}${md5(user.email)}${s2}`
        console.log(something)
        dispatch(
          signInWithGoogle({
            email: user.email,
            email_token: something,
          })
        )
      }
    } else {
      alert('No google data')
    }
  }

  const loginWithFBAsync = async () => {
    try {
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions,
      } = await Facebook.logInWithReadPermissionsAsync('1253597608157986', {
        permissions: ['public_profile', 'email'],
      })
      if (type === 'success') {
        const res = await fetch(
          `https://graph.facebook.com/me?access_token=${token}&fields=id,email`
        )
        let { email } = await res.json()
        if (email) {
          const s1 = (md5(uuid.v4()) + md5(uuid.v4())).slice(0, 50)
          const s2 = md5(uuid.v4()).slice(0, 18)
          const something = `${s1}${md5(email)}${s2}`
          console.log(something)
          dispatch(
            signInWithFacebook({
              email: email,
              email_token: something,
            })
          )
        } else {
          alert(`Couldn't get facebook email`)
        }
      } else {
        console.log(`cancelled`)
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`)
    }
  }

  useEffect(() => {
    if (signInWithFacebookMessage === 'signInWithFacebookSuccess') {
      navigation.navigate('App')
    } else if (signInWithFacebookMessage !== '') {
      setErrorMsg(signInWithFacebookMessage)
      setTimeout(() => {
        setErrorMsg('')
      }, 2000)
    }
    return () => {
      dispatch(signInWithFacebookReset())
    }
  }, [signInWithFacebookMessage])

  return (
    <Container>
      <HapticGenerator error={emailMsg !== ''}>
        <Label type="input" text="inputEmail" />
        <Input
          value={email}
          onChangeText={value => {
            setEmail(value)
            clearErrors()
          }}
          placeholder="user@doman.com"
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="email-address"
          onClear={() => {
            setEmail('')
            clearErrors()
          }}
          autoCompleteType="email"
        />
        <Message text={emailMsg} />
      </HapticGenerator>

      <HapticGenerator error={passwordMsg !== ''}>
        <Label type="input" text="inputPassword" />
        <Input
          value={password}
          onChangeText={value => {
            setPassword(value)
            clearErrors()
          }}
          placeholder="****"
          maxLength={40}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="default"
          secureTextEntry={true}
          onClear={() => {
            setPassword('')
            clearErrors()
          }}
          onSubmitEditing={() => {
            handlelogin()
          }}
          autoCompleteType="password"
        />
        <Message text={passwordMsg} />
      </HapticGenerator>

      <Message type="message" text={errorMsg && i18n.t(errorMsg)} />

      <Button
        type="plain"
        text="forgotPassword"
        onPress={() => {
          navigation.navigate('ResetPassword')
        }}
      />

      <View style={{ marginVertical: 20, width: '100%' }}>
        <Button
          text="login"
          loading={isLoggingIn}
          onPress={() => {
            handlelogin()
          }}
        />
        <Button
          text="loginWithGoogle"
          loading={isSigningInWithGoogle}
          onPress={() => {
            googleSignInAsync()
          }}
        />
        <Button
          text="loginWithFacebook"
          loading={isSigningInWithFacebook}
          onPress={() => {
            loginWithFBAsync()
          }}
        />
      </View>

      <Button
        type="plain"
        text="orRegister"
        onPress={() => {
          navigation.navigate('Register')
        }}
      />
    </Container>
  )
}

export default Login
