import React from 'react'
import { Container, ListItem } from '../components'
import { displayOptions } from '../utils'

const Display = ({ navigation }) => {
  return (
    <Container>
      {displayOptions.map(item => {
        return (
          <ListItem
            key={item.id}
            type="setting"
            onPress={() => {
              navigation.navigate(item.to)
            }}
            data={item}
          />
        )
      })}
    </Container>
  )
}

export default Display
