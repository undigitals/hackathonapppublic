import React from 'react'
import { StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { changeLang } from '../actions'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Button, View, Container } from '../components'
import { locales } from '../utils'
import constants from '../constants'

class SelectLang extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('headerTitle', i18n.t('SelectLang')),
    }
  }

  render() {
    const { navigation, changeLang } = this.props
    return (
      <Container contStyle={{ alignItems: 'center', justifyContent: 'center' }}>
        {locales.map(item => {
          return (
            <Button
              key={item.id}
              type="plain"
              text={item.name}
              onPress={() => {
                changeLang(item.lang)
                navigation.setParams({ headerTitle: i18n.t('SelectLang') })
                navigation.navigate('Login')
              }}
              contStyle={{ paddingVertical: 10 }}
              textStyle={{ fontWeight: '600' }}
            />
          )
        })}
      </Container>
    )
  }
}

const mapStateToProps = ({}) => ({})

export default connect(mapStateToProps, { changeLang })(SelectLang)
