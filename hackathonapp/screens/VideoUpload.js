import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, Alert } from 'react-native'
import * as FileSystem from 'expo-file-system'
import { useSelector, useDispatch } from 'react-redux'
import { createMsg, createMsgReset } from '../actions'
import {
  Container,
  Button,
  HapticGenerator,
  Label,
  Input,
  Message,
} from '../components'
import { feedbackGenerator } from '../utils'
import i18n from 'i18n-js'

const VIDEOS_DIR = FileSystem.documentDirectory + 'videos'

const VideoUpload = ({ navigation }) => {
  const [videos, setVideos] = useState([])
  const [message, setMessage] = useState('')
  const [errorMsg, setErrorMsg] = useState('')

  const dispatch = useDispatch()
  const { flight } = useSelector(({ flight }) => ({ flight }))
  const { isCreatingMsg, createMsgMessage, createMsgData, newMsg } = flight
  const { newVideo } = navigation.state.params

  useEffect(() => {
    const loadVideos = async () => {
      const savedVideos = await FileSystem.readDirectoryAsync(VIDEOS_DIR)
      setVideos(savedVideos)
    }
    loadVideos()
  }, [])

  useEffect(() => {
    if (createMsgMessage === 'createMsgSuccess') {
      feedbackGenerator('success')
      setErrorMsg(createMsgMessage)
      setTimeout(() => {
        navigation.navigate('Home')
        navigation.navigate('VideoMessages')
      }, 2000)
    } else if (createMsgMessage !== '') {
      setErrorMsg(createMsgMessage)
      setTimeout(() => {
        setErrorMsg('')
      }, 2000)
    }
    return () => {
      dispatch(createMsgReset())
    }
  }, [createMsgMessage])

  const getFileSize = async () => {
    let size = await FileSystem.getInfoAsync(`${VIDEOS_DIR}/${newVideo}`, {
      size: true,
    })
    console.log(`size: ${size} MB`)
  }

  const handleCreateMsg = () => {
    if (newVideo) {
      getFileSize()
      const {
        ident,
        filed_time,
        originCity,
        destinationCity,
      } = newMsg.flightInfo
      const data = new FormData()

      data.append('text', message || 'something')
      data.append('flight_id', ident)
      data.append('flight_time', filed_time)
      data.append('from', originCity)
      data.append('to', destinationCity)
      data.append('file', {
        name: `new-video`,
        type: 'video/mp4',
        uri: `${VIDEOS_DIR}/${newVideo}`,
      })

      dispatch(createMsg(data))
    } else {
      console.log('no newVideo')
    }
  }

  const cofirmCreateMsg = () => {
    Alert.alert(
      i18n.t('confirmCreateMsg'),
      '',
      [
        {
          text: i18n.t('cancel'),
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: i18n.t('confirm'),
          onPress: () => {
            handleCreateMsg()
          },
        },
      ],
      { cancelable: true }
    )
  }

  return (
    <Container>
      <Label text="titleVideoUpload" />

      <HapticGenerator error={false}>
        <Label type="input" text="inputMessage" />
        <Input
          value={message}
          onChangeText={value => {
            setMessage(value)
          }}
          placeholder="Something"
          maxLength={100}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          onClear={() => {
            setMessage('')
          }}
          keyboardType="default"
        />
        <Message text="" />
      </HapticGenerator>

      <Message type="message" text={errorMsg && i18n.t(errorMsg)} />

      <Button
        text="upload"
        loading={isCreatingMsg}
        onPress={() => {
          cofirmCreateMsg()
        }}
      />
    </Container>
  )
}

export default VideoUpload
