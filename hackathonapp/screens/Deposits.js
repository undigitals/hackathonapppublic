import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, Alert } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { getDeposits, getDepositsReset } from '../actions'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import {
  Container,
  FlatListCont,
  RefreshControl,
  ListItem,
  HomeHeaderRight,
} from '../components'
import { feedbackGenerator } from '../utils'
import constants from '../constants'

const Deposits = () => {
  const dispatch = useDispatch()
  const { deposit } = useSelector(({ deposit }) => ({ deposit }))
  const { isGettingDeposits, getDepositsMessage, deposits } = deposit

  useEffect(() => {
    dispatch(
      getDeposits({
        per_page: 30,
        page: 1,
      })
    )
  }, [])

  return (
    <Container type="view">
      {(() => {
        if (isGettingDeposits) {
          return null
        } else if (deposits.length !== 0) {
          return (
            <FlatListCont
              data={deposits}
              keyExtractor={item => item.id + ''}
              renderItem={({ item }) => {
                let data = {
                  imageSrc: require('../assets/images/paypal.png'),
                  textPrimary: i18n.t(item.payment_type),
                  textSecondary: `${i18n.t(item.status)} | ${item.created_at}`,
                  textRight: `${i18n.t('usd')} ${item.usd_amount}`,
                }
                return (
                  <ListItem
                    type="order"
                    onPress={() => {
                      console.log(item)
                    }}
                    data={data}
                  />
                )
              }}
              refreshControl={
                <RefreshControl onRefresh={() => {}} refreshing={false} />
              }
              onEndThreshold={0.1}
              onEndReached={() => {}}
            />
          )
        }
      })()}
    </Container>
  )
}

Deposits.navigationOptions = ({}) => {
  return {
    headerRight: <HomeHeaderRight to="deposit" />,
  }
}

export default Deposits
