import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container, View } from '../components'
import constants from '../constants'
import { GiftedChat, MessageImage } from 'react-native-gifted-chat'
import { Dialogflow_V2 } from 'react-native-dialogflow-text'
import { envs } from '../envs'

const BOT_USER = {
  _id: 2,
  name: 'Anton Bot',
  avatar: 'https://image.flaticon.com/icons/png/512/1587/1587565.png',
}

class ChatBotOptimized extends React.Component {
  state = {
    messages: [
      {
        _id: 1,
        text: `Hello, I'm Anton. I can help you with almost anything! Ask me`,
        image: '',
        location: {},
        createdAt: new Date(),
        user: BOT_USER,
      },
    ],
  }

  componentDidMount() {
    const { navigation } = this.props
    const { type, name } = navigation.state.params
    navigation.setParams({ headerTitle: name })

    if (type === 'academic') {
      Dialogflow_V2.setConfiguration(
        envs[0].dialogflowConfig.client_email,
        envs[0].dialogflowConfig.private_key,
        Dialogflow_V2.LANG_ENGLISH_US,
        envs[0].dialogflowConfig.project_id
      )
    } else if (type === 'foreigner') {
      Dialogflow_V2.setConfiguration(
        envs[1].dialogflowConfig.client_email,
        envs[1].dialogflowConfig.private_key,
        Dialogflow_V2.LANG_ENGLISH_US,
        envs[1].dialogflowConfig.project_id
      )
    } else if (type === 'food') {
      Dialogflow_V2.setConfiguration(
        envs[2].dialogflowConfig.client_email,
        envs[2].dialogflowConfig.private_key,
        Dialogflow_V2.LANG_ENGLISH_US,
        envs[2].dialogflowConfig.project_id
      )
    }
  }

  handleGoogleResponse(result) {
    let text = result.queryResult.fulfillmentMessages[0].text.text[0]

    let media = null
    if (result.queryResult.fulfillmentMessages[1])
      media = result.queryResult.fulfillmentMessages[1].payload.media

    console.log(media)
    this.sendBotResponse(text, media)
  }

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))

    let message = messages[0].text
    Dialogflow_V2.requestQuery(
      message,
      result => this.handleGoogleResponse(result),
      error => console.log(error)
    )
  }

  sendBotResponse(text, media) {
    let msg = {
      _id: this.state.messages.length + 1,
      text,
      image: media && media[0].image && media[0].image,
      createdAt: new Date(),
      user: BOT_USER,
    }

    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, [msg]),
    }))
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1,
          }}
        />
      </View>
    )
  }
}

export default ChatBotOptimized
