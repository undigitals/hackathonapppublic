import React from 'react'
import {} from 'react-native'
import { connect } from 'react-redux'
import { sendVerif, sendVerifReset } from '../actions'
import i18n from 'i18n-js'
import {
  Button,
  Container,
  Label,
  Input,
  HapticGenerator,
  Message,
} from '../components'

class SendVerif extends React.Component {
  state = {
    verif: '',
    verifMsg: '',
  }

  componentDidUpdate() {
    const { navigation, sendVerifMessage } = this.props
    if (sendVerifMessage === 'verifSuccess') {
      navigation.navigate('App')
    }
  }

  componentWillUnmount() {
    const { sendVerifReset } = this.props
    sendVerifReset()
  }

  render() {
    const { sendVerifReset, isSendingVerif, sendVerifMessage } = this.props
    const { verif, verifMsg } = this.state
    return (
      <Container>
        <HapticGenerator error={verifMsg !== ''}>
          <Label type="input" text="inputVerifCode" />
          <Input
            value={verif}
            inputRef={ref => (this.verifField = ref)}
            onChangeText={value => {
              this.setState({ verif: value, verifMsg: '' })
              sendVerifReset()
            }}
            placeholder="00000"
            maxLength={5}
            prefixContStyle={{ marginRight: 0 }}
            allowClear
            onClear={() => {
              this.setState({ verif: '', verifMsg: '' })
              sendVerifReset()
            }}
            autoFocus={true}
          />
          <Message text={verifMsg} />
        </HapticGenerator>

        <Message type="message" text={sendVerifMessage} />

        <Button
          text="continue"
          loading={isSendingVerif}
          onPress={() => {
            this.handleSendVerif()
          }}
        />
      </Container>
    )
  }

  handleSendVerif = () => {
    const { sendVerif, userId } = this.props
    const { verif } = this.state
    const regex = /[0-9]{5}/g

    if (regex.test(verif)) {
      sendVerif({
        user_id: userId,
        sms_code: `${verif}`,
      })
    } else {
      this.setState({ verifMsg: i18n.t('invalidVerifCode') })
    }
  }
}

const mapStateToProps = ({ auth, locale }) => ({
  isSendingVerif: auth.isSendingVerif,
  sendVerifMessage: auth.sendVerifMessage,
  userId: auth.userId,
  lang: locale.lang,
})

export default connect(mapStateToProps, { sendVerif, sendVerifReset })(
  SendVerif
)
