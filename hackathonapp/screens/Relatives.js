import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, Alert } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import {
  getRelatives,
  getMsgsReset,
  deleteRelative,
  deleteMsgReset,
} from '../actions'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import {
  Container,
  FlatListCont,
  RefreshControl,
  ListItem,
  HomeHeaderLeft,
  HomeHeaderRight,
} from '../components'
import { feedbackGenerator } from '../utils'
import constants from '../constants'

const Relatives = () => {
  const dispatch = useDispatch()
  const { relative } = useSelector(({ relative }) => ({ relative }))
  const {
    isGettingRelatives,
    getMsgsMessage,
    relatives,
    isDeletingMsg,
    deleteMsgMessage,
  } = relative

  useEffect(() => {
    dispatch(getRelatives())
  }, [])

  const handleDeleteRelative = ({ id }) => {
    dispatch(
      deleteRelative({
        relative_id: id,
      })
    )
  }

  const confirmDeleteRelative = data => {
    Alert.alert(
      i18n.t('confirmDeleteRelative'),
      '',
      [
        {
          text: i18n.t('cancel'),
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: i18n.t('delete'),
          onPress: () => {
            handleDeleteRelative(data)
          },
        },
      ],
      { cancelable: true }
    )
  }

  return (
    <Container type="view">
      {(() => {
        if (isGettingRelatives) {
          return null
        } else if (relatives.length !== 0) {
          return (
            <FlatListCont
              data={relatives}
              keyExtractor={item => item.id + ''}
              renderItem={({ item }) => {
                let data = {
                  imageSrc: null,
                  name: item.name,
                  phoneNumber: item.phone_number,
                  email: item.email,
                }
                return (
                  <ListItem
                    type="contact"
                    onPress={() => {
                      console.log(item)
                    }}
                    onLongPress={() => {
                      feedbackGenerator('selection')
                      confirmDeleteRelative(item)
                    }}
                    data={data}
                  />
                )
              }}
              refreshControl={
                <RefreshControl
                  onRefresh={() => {}}
                  refreshing={isGettingRelatives}
                />
              }
              onEndThreshold={0.1}
              onEndReached={() => {}}
            />
          )
        }
      })()}
    </Container>
  )
}

export default Relatives

Relatives.navigationOptions = ({}) => {
  return {
    headerRight: <HomeHeaderRight />,
    headerLeft: <HomeHeaderLeft to="newRelative" />,
  }
}
