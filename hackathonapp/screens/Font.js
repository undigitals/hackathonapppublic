import React, { useContext, useEffect, useState } from 'react'
import { ThemeContext } from '../context'
import { Container, ListItem } from '../components'
import Constants from 'expo-constants'
import i18n from 'i18n-js'
import { feedbackGenerator } from '../utils'

const Font = () => {
  const { font, changeFont } = useContext(ThemeContext)
  const [loading, setLoading] = useState(true)
  const [fonts, setFonts] = useState([])

  useEffect(() => {
    setTimeout(() => {
      sortFonts()
      setLoading(false)
    }, 100)
  })

  const sortFonts = () => {
    let systemFonts = Constants.systemFonts
    setFonts(systemFonts)
  }

  return (
    <Container onRefresh={() => {}} refreshing={loading}>
      {fonts.length > 0 && (
        <ListItem
          type="plain"
          onPress={() => {
            changeFont('default')
            feedbackGenerator('selection')
          }}
          text={i18n.t('defaultFont')}
          selected={font === null}
          textPrimaryStyle={{ fontFamily: null }}
        />
      )}
      {fonts.length > 0 &&
        fonts.map((item, index) => {
          return (
            <ListItem
              key={index}
              type="plain"
              onPress={() => {
                changeFont(item)
                feedbackGenerator('selection')
              }}
              text={item}
              selected={font === item}
              textPrimaryStyle={{ fontFamily: item }}
            />
          )
        })}
    </Container>
  )
}

export default Font
