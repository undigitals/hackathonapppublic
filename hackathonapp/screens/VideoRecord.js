import React from 'react'
import { StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import * as Permissions from 'expo-permissions'
import { Camera } from 'expo-camera'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Button, View, Container, Touchable, Text } from '../components'
import constants from '../constants'

class VideoRecord extends React.Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    this.setState({ hasCameraPermission: status === 'granted' })
  }

  render() {
    const { navigation } = this.props
    const { hasCameraPermission, type } = this.state

    if (hasCameraPermission === null) {
      return (
        <Button
          type="plain"
          text={`hasCameraPermission=${hasCameraPermission}`}
        />
      )
    } else if (hasCameraPermission === false) {
      return <Button type="plain" text="No access to camera" />
    } else {
      return (
        <View style={[styles.cameraOuterCont]}>
          <Camera style={{ flex: 1 }} type={type}>
            <View style={[styles.cameraCont]}>
              <Touchable
                buttonType="opacity"
                style={{
                  flex: 0.1,
                  alignSelf: 'flex-end',
                  alignItems: 'center',
                }}
                onPress={() => {
                  this.setState({
                    type:
                      this.state.type === Camera.Constants.Type.back
                        ? Camera.Constants.Type.front
                        : Camera.Constants.Type.back,
                  })
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    marginBottom: 10,
                    color: 'white',
                  }}
                >
                  {' '}
                  Flip{' '}
                </Text>
              </Touchable>
            </View>
          </Camera>
        </View>
      )
    }
  }
}

const mapStateToProps = ({}) => ({})

export default connect(mapStateToProps, {})(VideoRecord)

const styles = StyleSheet.create({
  cameraOuterCont: {
    flex: 1,
    borderWidth: 1,
  },
  cameraCont: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
})
