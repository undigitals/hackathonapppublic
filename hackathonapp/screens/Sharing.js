import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, View } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import {
  Container,
  HomeHeaderLeft,
  HomeHeaderRight,
  ServiceCard,
  Text,
} from '../components'
import uuid from 'uuid'
import { servicesTemp } from '../utils'
import constants from '../constants'

import { useLazyQuery } from '@apollo/react-hooks'
import { gql } from 'apollo-boost'

const GET_ALL_SERVICES = gql`
  query {
    services {
      id
      name
      desc
      image {
        url
        family
        name
      }
      sections {
        name
        id
        image {
          url
          family
          name
        }
        products {
          name
          id
          details
          price
          image {
            url
            family
            name
          }
          seller {
            name
            email
          }
        }
      }
    }
  }
`

const Sharing = ({ navigation }) => {
  const [services, setServices] = useState([])
  const [getServices, { loading, data }] = useLazyQuery(GET_ALL_SERVICES)

  const fetchServices = () => {
    getServices()
  }

  useEffect(() => {
    fetchServices()
  }, [])

  useEffect(() => {
    if (data && data.services) {
      setServices(data.services)
    }
  }, [data])

  return (
    <Container onRefresh={() => {}} refreshing={loading}>
      <View key={uuid.v4()} style={styles.sectionCont}>
        <View style={styles.servicesCont}>
          {(() => {
            {
              /* if (loading) {
              return <Text>Loading...</Text>
            } */
            }

            if (services.length > 0) {
              return services.map(service => {
                return (
                  <ServiceCard
                    type="regularService"
                    key={service.id}
                    serviceName={service.name}
                    serviceLogoUri={{
                      uri: `${service.image.url}`,
                    }}
                    onPress={() => {
                      navigation.navigate('SharingSections', { service })
                    }}
                  />
                )
              })
            }
          })()}
        </View>
      </View>
    </Container>
  )
}

export default Sharing

Sharing.navigationOptions = ({}) => {
  return {
    headerRight: <HomeHeaderRight />,
    headerLeft: <HomeHeaderLeft />,
  }
}

const styles = StyleSheet.create({
  sectionCont: {
    width: '100%',
  },
  servicesCont: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: constants.paddingHorizontal,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
})
