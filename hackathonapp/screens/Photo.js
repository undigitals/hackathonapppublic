import React from 'react'
import { Image, StyleSheet, TouchableOpacity } from 'react-native'
import { View, Text } from '../components'
import * as FaceDetector from 'expo-face-detector'
import { Video } from 'expo-av'
import { Ionicons } from '@expo/vector-icons'
import { ThemeContext } from '../context'
import constants from '../constants'

const pictureSize = constants.pictureCardWidth

class Photo extends React.Component {
  state = {
    selected: false,
    faces: [],
    image: null,
  }
  _mounted = false

  componentDidMount() {
    this._mounted = true
  }

  componentWillUnmount() {
    this._mounted = false
  }

  toggleSelection = () => {
    this.setState({ selected: !this.state.selected }, () =>
      this.props.onSelectionToggle(this.props.uri, this.state.selected)
    )
  }

  detectFace = () =>
    FaceDetector.detectFacesAsync(this.props.uri, {
      detectLandmarks: FaceDetector.Constants.Landmarks.none,
      runClassifications: FaceDetector.Constants.Classifications.all,
    })
      .then(this.facesDetected)
      .catch(this.handleFaceDetectionError)

  facesDetected = ({ image, faces }) => {
    this.setState({
      faces,
      image,
    })
  }

  getImageDimensions = ({ width, height }) => {
    if (width > height) {
      const scaledHeight = (pictureSize * height) / width
      return {
        width: pictureSize,
        height: scaledHeight,

        scaleX: pictureSize / width,
        scaleY: scaledHeight / height,

        offsetX: 0,
        offsetY: (pictureSize - scaledHeight) / 2,
      }
    } else {
      const scaledWidth = (pictureSize * width) / height
      return {
        width: scaledWidth,
        height: pictureSize,

        scaleX: scaledWidth / width,
        scaleY: pictureSize / height,

        offsetX: (pictureSize - scaledWidth) / 2,
        offsetY: 0,
      }
    }
  }

  handleFaceDetectionError = error => console.warn(error)

  renderFaces = () =>
    this.state.image &&
    this.state.faces &&
    this.state.faces.map(this.renderFace)

  renderFace = (face, index) => {
    const { image } = this.state
    const { scaleX, scaleY, offsetX, offsetY } = this.getImageDimensions(image)
    const layout = {
      top: offsetY + face.bounds.origin.y * scaleY,
      left: offsetX + face.bounds.origin.x * scaleX,
      width: face.bounds.size.width * scaleX,
      height: face.bounds.size.height * scaleY,
    }

    return (
      <View
        key={index}
        style={[styles.face, layout]}
        transform={[
          { perspective: 600 },
          { rotateZ: `${(face.rollAngle || 0).toFixed(0)}deg` },
          { rotateY: `${(face.yawAngle || 0).toFixed(0)}deg` },
        ]}
      >
        <Text style={styles.faceText}>
          😁 {(face.smilingProbability * 100).toFixed(0)}%
        </Text>
      </View>
    )
  }

  render() {
    const { uri, type } = this.props
    const { theme } = this.context
    if (type === 'photo') {
      return (
        <TouchableOpacity
          style={styles.pictureCont}
          onLongPress={this.detectFace}
          onPress={this.toggleSelection}
          activeOpacity={1}
        >
          <View style={styles.pictureWrapper}>
            <Image
              style={[
                styles.picture,
                {
                  borderColor: constants[theme].galleryBackgroundColor,
                  resizeMode: 'cover',
                },
              ]}
              source={{ uri }}
            />
            {this.state.selected && (
              <Ionicons name="md-checkmark-circle" size={30} color="#4630EB" />
            )}
            <View style={styles.facesContainer}>{this.renderFaces()}</View>
          </View>
        </TouchableOpacity>
      )
    } else if (type === 'video') {
      return (
        <TouchableOpacity
          style={styles.pictureCont}
          onLongPress={this.detectFace}
          onPress={this.toggleSelection}
          activeOpacity={1}
        >
          <View style={styles.pictureWrapper}>
            <Video
              source={{ uri }}
              rate={1.0}
              volume={1.0}
              isMuted={false}
              resizeMode="cover"
              // shouldPlay
              // isLooping
              style={[
                styles.picture,
                { borderColor: constants[theme].galleryBackgroundColor },
              ]}
            />
            {this.state.selected && (
              <Ionicons name="md-checkmark-circle" size={30} color="#4630EB" />
            )}
            <View style={styles.facesContainer}>{this.renderFaces()}</View>
          </View>
        </TouchableOpacity>
      )
    }
  }
}

Photo.contextType = ThemeContext

export default Photo

const styles = StyleSheet.create({
  pictureCont: {
    height: constants.pictureCardHeight,
    width: constants.pictureCardWidth,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    // borderWidth: 0.5,
  },
  picture: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
    borderWidth: 0.3,
    borderColor: 'transparent',
    // margin: 0.2,
    // margin: StyleSheet.hairlineWidth,
  },
  pictureWrapper: {
    width: pictureSize,
    height: pictureSize,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    // margin: 5,
    // borderWidth: 0.5,
  },
  facesContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
    backgroundColor: 'transparent',
    // borderWidth: 0.5,
  },
  face: {
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#FFD700',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  faceText: {
    color: '#FFD700',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 2,
    fontSize: 10,
    backgroundColor: 'transparent',
  },
})
