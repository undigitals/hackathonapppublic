import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, ScrollView, Image, Linking } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container, View, Text, Icon, Button } from '../components'
import {
  getBookmarkStatus,
  saveToBookmark,
  removeFromBookmarks,
} from '../utils'
import constants from '../constants'

const ProductBookmarked = ({ navigation }) => {
  const { product } = navigation.state.params
  console.log(product)

  useEffect(() => {
    navigation.setParams({ headerTitle: product.name })
  }, [])

  const { name, details, price, seller, image } = product

  const handleContact = item => {
    Linking.openURL(`mailto:${item.seller.email}?subject=${item.name}&body=Hi`)
  }

  handleBookmark = item => {
    saveToBookmark(item)
  }

  return (
    <Container onRefresh={() => {}} refreshing={false}>
      <View style={[styles.imageCont]}>
        <Image
          source={{ uri: image.url }}
          style={{ width: constants.width, height: 200, resizeMode: 'cover' }}
        />
      </View>
      <View style={styles.detailsCont}>
        <Text style={styles.productName}>{name}</Text>
        <Text style={styles.productDetail}>{details}</Text>
      </View>

      <View style={styles.priceCont}>
        <Text style={[styles.productPrice]}>
          Price: {price === 'free' ? 'Free' : `KRW ${price}`}
        </Text>
      </View>

      <View style={styles.contactCont}>
        <View style={styles.sellerCont}>
          <Icon name="user" style={styles.icon} />
          <Text style={styles.sellerName}>{seller.name}</Text>
        </View>
        <View style={styles.emailCont}>
          <Icon name="mail" style={styles.icon} />
          <Text style={styles.sellerEmail}>{seller.email}</Text>
        </View>
      </View>

      <View style={{ marginVertical: 20, width: '100%' }}>
        <Button
          text="contact"
          onPress={() => {
            handleContact(product)
          }}
        />
        <Button
          text="bookmark"
          onPress={() => {
            handleBookmark(product)
          }}
        />
      </View>
    </Container>
  )
}

export default ProductBookmarked

const styles = StyleSheet.create({
  imageCont: {
    // borderWidth: 0.5,
    width: '100%',
    height: 200,
    marginBottom: 20,
  },
  detailsCont: {
    // borderWidth: 0.5,
    paddingHorizontal: constants.paddingHorizontal,
    width: '100%',
  },
  productName: {
    fontSize: 22,
    fontWeight: '700',
  },
  productDetail: {
    fontSize: 16,
  },
  priceCont: {
    paddingHorizontal: constants.paddingHorizontal,
    width: '100%',
    marginBottom: 10,
    marginTop: 20,
  },
  productPrice: {
    fontSize: 20,
  },
  contactCont: {
    paddingHorizontal: constants.paddingHorizontal,
    width: '100%',
    marginVertical: 10,
  },
  icon: {
    marginRight: constants.paddingHorizontal,
  },
  emailCont: {
    flexDirection: 'row',
  },
  sellerCont: {
    flexDirection: 'row',
  },
  sellerName: {
    fontSize: 16,
  },
  sellerEmail: {
    fontSize: 16,
  },
})
