import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container } from '../components'
import constants from '../constants'

const Maintenance = () => {
  return <Container></Container>
}

export default Maintenance

Maintenance.navigationOptions = ({ screenProps }) => {
  let currentTheme = constants[screenProps.theme]
  return {
    title: i18n.t('Maintenance'),
    headerTintColor: currentTheme.headerTintColor,
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      borderBottomWidth: 0,
      backgroundColor: currentTheme.backgroundColor,
      borderBottomColor: currentTheme.headerBorderBottomColor,
    },
  }
}
