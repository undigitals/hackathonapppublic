import React from 'react'
import { Camera } from 'expo-camera'
import Constants from 'expo-constants'
import { Header } from 'react-navigation-stack'
import { NavigationEvents } from 'react-navigation'
import * as Permissions from 'expo-permissions'
import * as FileSystem from 'expo-file-system'
import * as Device from 'expo-device'
import { BarCodeScanner } from 'expo-barcode-scanner'
import {
  Alert,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Slider,
  Platform,
  StatusBar,
} from 'react-native'
import { Timer } from '../components/stopwatch-timer'
import isIPhoneX from 'react-native-is-iphonex'

import {
  Ionicons,
  MaterialIcons,
  Foundation,
  MaterialCommunityIcons,
  Octicons,
  AntDesign,
  Entypo,
} from '@expo/vector-icons'
import i18n from 'i18n-js'
import { ThemeContext } from '../context'
import {
  feedbackGenerator,
  flashModeOrder,
  flashIcons,
  wbOrder,
  wbIcons,
} from '../utils'
import constants from '../constants'

const landmarkSize = 2
const actionButtonSize = 100

export default class VideoExample extends React.Component {
  state = {
    flash: 'off',
    zoom: 0,
    autoFocus: 'on',
    type: 'back',
    whiteBalance: 'auto',
    ratio: '16:9',
    ratios: [],
    barcodeScanning: false,
    faceDetecting: false,
    faces: [],
    newPhotos: false,
    newVideos: false,
    newVideo: null,
    permissionsGranted: false,
    pictureSize: undefined,
    pictureSizes: [],
    pictureSizeId: 0,
    showGallery: false,
    showMoreOptions: false,
    actionType: 'video',
    isRecording: false,

    // timer
    timerStart: false,
    stopwatchStart: false,
    totalDuration: 10000,
    timerReset: false,
    stopwatchReset: false,
  }

  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    this.setState({ permissionsGranted: status === 'granted' })
  }

  componentDidMount() {
    feedbackGenerator('selection')
    // this.handleStatusBar(true)
    FileSystem.makeDirectoryAsync(
      FileSystem.documentDirectory + 'photos'
    ).catch(e => {
      console.log('photos directory exists')
    })

    FileSystem.makeDirectoryAsync(
      FileSystem.documentDirectory + 'videos'
    ).catch(e => {
      console.log('videos directory exists')
    })
  }

  componentWillUnmount() {
    this.handleStatusBar(false)
  }

  toggleTimer = () => {
    this.setState({ timerStart: !this.state.timerStart, timerReset: false })
  }

  resetTimer = () => {
    this.setState({ timerStart: false, timerReset: true })
  }

  toggleStopwatch = () => {
    this.setState({
      stopwatchStart: !this.state.stopwatchStart,
      stopwatchReset: false,
    })
  }

  resetStopwatch = () => {
    this.setState({ stopwatchStart: false, stopwatchReset: true })
  }

  getFormattedTime = time => {
    this.currentTime = time
  }

  handleStatusBar = hidden => {
    StatusBar.setHidden(hidden, 'slide')
  }

  getRatios = async () => {
    const ratios = await this.camera.getSupportedRatios()
    return ratios
  }

  toggleView = () => {
    feedbackGenerator('selection')
    this.setState({
      showGallery: !this.state.showGallery,
      newPhotos: false,
      newVideos: false,
    })
  }

  toggleMoreOptions = () => {
    feedbackGenerator('selection')
    this.setState({ showMoreOptions: !this.state.showMoreOptions })
  }

  toggleFacing = () => {
    feedbackGenerator('selection')
    this.setState({ type: this.state.type === 'back' ? 'front' : 'back' })
  }

  toggleFlash = () => {
    feedbackGenerator('selection')
    this.setState({ flash: flashModeOrder[this.state.flash] })
  }

  setRatio = ratio => this.setState({ ratio })

  toggleWB = () => {
    feedbackGenerator('selection')
    this.setState({ whiteBalance: wbOrder[this.state.whiteBalance] })
  }

  toggleFocus = () => {
    feedbackGenerator('selection')
    this.setState({ autoFocus: this.state.autoFocus === 'on' ? 'off' : 'on' })
  }

  zoomOut = () =>
    this.setState({
      zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1,
    })

  zoomIn = () =>
    this.setState({
      zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1,
    })

  setFocusDepth = depth => this.setState({ depth })

  toggleBarcodeScanning = () => {
    feedbackGenerator('selection')
    this.setState({ barcodeScanning: !this.state.barcodeScanning })
  }

  toggleFaceDetection = () => {
    feedbackGenerator('selection')
    this.setState({ faceDetecting: !this.state.faceDetecting })
  }

  takePicture = () => {
    feedbackGenerator('selection')
    if (this.camera) {
      this.camera.takePictureAsync({ onPictureSaved: this.onPictureSaved })
    }
  }

  recordVideo = () => {
    this.toggleTimer()
    if (this.camera) {
      const recordOptions = {
        // quality: Camera.Constants.VideoQuality['<value>'], // 2160p, 1080p, 720p, 480p
        // maxDuration: 60 * 3, // 3 min
        maxFileSize: 1000000 * 100, // 100 MB
        mute: false,
      }
      this.camera.recordAsync(recordOptions).then(video => {
        this.onVideoSaved(video)
      })
    }
  }

  stopVideo = () => {
    if (this.state.isRecording) {
      this.setState({ isRecording: false })
      this.resetTimer()
    }
    if (this.camera) {
      this.camera.stopRecording()
      feedbackGenerator('selection')
    }
  }

  handleMountError = ({ message }) => console.error(message)

  onPictureSaved = async photo => {
    console.log(photo)
    await FileSystem.moveAsync({
      from: photo.uri,
      to: `${FileSystem.documentDirectory}photos/${Date.now()}.jpg`,
    }).catch(e => {
      console.log('moveAsync photos/', e)
    })
    this.setState({ newPhotos: true })
  }

  onVideoSaved = async video => {
    console.log(video)
    feedbackGenerator('selection')
    if (this.state.isRecording) {
      this.resetTimer()
      this.setState({ isRecording: false })
    }
    const videoName = Date.now()
    await FileSystem.moveAsync({
      from: video.uri,
      to: `${FileSystem.documentDirectory}videos/${videoName}.mov`,
    }).catch(e => {
      console.log('moveAsync videos/', e)
    })

    this.setState({ newVideos: true, newVideo: `${videoName}.mov` })
  }

  onBarCodeScanned = code => {
    feedbackGenerator('selection')
    this.setState(
      { barcodeScanning: !this.state.barcodeScanning },
      Alert.alert(`Barcode found: ${code.data}`)
    )
  }

  onFacesDetected = ({ faces }) => this.setState({ faces })
  onFaceDetectionError = state => console.warn('Faces detection error:', state)

  collectPictureSizes = async () => {
    if (this.camera) {
      const pictureSizes = await this.camera.getAvailablePictureSizesAsync(
        this.state.ratio
      )
      let pictureSizeId = 0
      if (Platform.OS === 'ios') {
        pictureSizeId = pictureSizes.indexOf('High')
      } else {
        // returned array is sorted in ascending order - default size is the largest one
        pictureSizeId = pictureSizes.length - 1
      }
      this.setState({
        pictureSizes,
        pictureSizeId,
        pictureSize: pictureSizes[pictureSizeId],
      })
    }
  }

  previousPictureSize = () => this.changePictureSize(1)
  nextPictureSize = () => this.changePictureSize(-1)

  changePictureSize = direction => {
    let newId = this.state.pictureSizeId + direction
    const length = this.state.pictureSizes.length
    if (newId >= length) {
      newId = 0
    } else if (newId < 0) {
      newId = length - 1
    }
    this.setState({
      pictureSize: this.state.pictureSizes[newId],
      pictureSizeId: newId,
    })
  }

  handleChangeActionType = type => {
    feedbackGenerator('selection')
    this.setState({ actionType: type })
  }

  renderGallery() {
    // return <Gallery onPress={this.toggleView.bind(this)} />
    return null
  }

  renderFace({ bounds, faceID, rollAngle, yawAngle }) {
    return (
      <View
        key={faceID}
        transform={[
          { perspective: 600 },
          { rotateZ: `${rollAngle.toFixed(0)}deg` },
          { rotateY: `${yawAngle.toFixed(0)}deg` },
        ]}
        style={[
          styles.face,
          {
            ...bounds.size,
            left: bounds.origin.x,
            top: bounds.origin.y,
          },
        ]}
      >
        <Text style={styles.faceText}>ID: {faceID}</Text>
        <Text style={styles.faceText}>rollAngle: {rollAngle.toFixed(0)}</Text>
        <Text style={styles.faceText}>yawAngle: {yawAngle.toFixed(0)}</Text>
      </View>
    )
  }

  renderLandmarksOfFace(face) {
    const renderLandmark = position =>
      position && (
        <View
          style={[
            styles.landmark,
            {
              left: position.x - landmarkSize / 2,
              top: position.y - landmarkSize / 2,
            },
          ]}
        />
      )
    return (
      <View key={`landmarks-${face.faceID}`}>
        {renderLandmark(face.leftEyePosition)}
        {renderLandmark(face.rightEyePosition)}
        {renderLandmark(face.leftEarPosition)}
        {renderLandmark(face.rightEarPosition)}
        {renderLandmark(face.leftCheekPosition)}
        {renderLandmark(face.rightCheekPosition)}
        {renderLandmark(face.leftMouthPosition)}
        {renderLandmark(face.mouthPosition)}
        {renderLandmark(face.rightMouthPosition)}
        {renderLandmark(face.noseBasePosition)}
        {renderLandmark(face.bottomMouthPosition)}
      </View>
    )
  }

  renderFaces = () => (
    <View style={styles.facesContainer} pointerEvents="none">
      {this.state.faces.map(this.renderFace)}
    </View>
  )

  renderLandmarks = () => (
    <View style={styles.facesContainer} pointerEvents="none">
      {this.state.faces.map(this.renderLandmarksOfFace)}
    </View>
  )

  renderNoPermissions = () => (
    <ThemeContext.Consumer>
      {({ font }) => {
        return (
          <View style={styles.noPermissions}>
            <Text
              style={{ color: 'white', fontFamily: font, textAlign: 'center' }}
            >
              {i18n.t('noCameraPermission')}
            </Text>
          </View>
        )
      }}
    </ThemeContext.Consumer>
  )

  renderTopBar = () => (
    <ThemeContext.Consumer>
      {({ font }) => {
        return (
          <React.Fragment>
            <View style={styles.topBar}>
              <View style={[styles.topBarControls]}>
                <TouchableOpacity
                  style={styles.toggleButton}
                  onPress={() => {
                    feedbackGenerator('selection')
                    this.props.navigation.goBack(null)
                  }}
                >
                  <AntDesign
                    style={{
                      marginLeft: constants.paddingHorizontal,
                      marginRight: 'auto',
                    }}
                    name="close"
                    size={32}
                    color="white"
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.toggleButton}
                  onPress={this.toggleFacing}
                >
                  <Ionicons name="ios-reverse-camera" size={32} color="white" />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.toggleButton}
                  onPress={this.toggleFlash}
                >
                  <MaterialIcons
                    name={flashIcons[this.state.flash]}
                    size={32}
                    color="white"
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.toggleButton}
                  onPress={this.toggleWB}
                  disabled={this.state.actionType === 'video' ? true : false}
                >
                  <MaterialIcons
                    name={wbIcons[this.state.whiteBalance]}
                    size={32}
                    color={
                      this.state.actionType === 'video' ? '#6b6b6b' : 'white'
                    }
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.toggleButton}
                  onPress={this.toggleFocus}
                >
                  <Text
                    style={[
                      styles.autoFocusLabel,
                      {
                        color:
                          this.state.autoFocus === 'on' ? 'white' : '#6b6b6b',
                      },
                    ]}
                  >
                    AF
                  </Text>
                </TouchableOpacity>
              </View>
              {this.state.actionType === 'video' ? (
                <View style={[styles.timerCont]}>
                  <Timer
                    totalDuration={this.state.totalDuration}
                    // msecs
                    start={this.state.timerStart}
                    reset={this.state.timerReset}
                    options={{
                      container: {
                        marginTop: 10,
                        // borderWidth: 0.5,
                        borderColor: '#fff',
                      },
                      text: {
                        fontSize: 22,
                        color: '#fff',
                        fontFamily: font,
                      },
                    }}
                    handleFinish={() => {
                      // Alert.alert('finished')
                      this.stopVideo()
                    }}
                    getTime={this.getFormattedTime}
                  />
                </View>
              ) : null}
            </View>
          </React.Fragment>
        )
      }}
    </ThemeContext.Consumer>
  )

  renderBottomBar = () => (
    <ThemeContext.Consumer>
      {({ font }) => {
        return (
          <View style={styles.bottomBar}>
            <View style={[styles.actionButtonCont]}>
              <View style={{}}>
                <TouchableOpacity
                  onPress={() => {
                    if (this.state.actionType === 'photo') {
                      this.takePicture()
                    } else if (this.state.actionType === 'video') {
                      if (this.state.isRecording) {
                        feedbackGenerator('selection')
                        this.setState({ isRecording: false })
                        this.stopVideo()
                      } else {
                        feedbackGenerator('selection')
                        this.setState({ isRecording: true })
                        this.recordVideo()
                      }
                    }
                  }}
                  style={[styles.actionButton]}
                >
                  {(() => {
                    if (this.state.actionType === 'photo') {
                      return (
                        <Ionicons
                          name="ios-radio-button-on"
                          size={actionButtonSize}
                          color="white"
                        />
                      )
                    } else if (this.state.actionType === 'video') {
                      return (
                        <React.Fragment>
                          {this.state.isRecording ? (
                            <Entypo
                              name="controller-stop"
                              size={actionButtonSize}
                              color={constants.danger}
                            />
                          ) : (
                            <Foundation
                              name="record"
                              size={actionButtonSize}
                              color="#fff"
                            />
                          )}
                        </React.Fragment>
                      )
                    }
                  })()}
                </TouchableOpacity>
              </View>
            </View>
            <View style={[styles.actionButtonsCont]}>
              <TouchableOpacity
                style={[
                  styles.bottomButton,
                  // { opacity: 0 }
                ]}
                // disabled={true}
                onPress={() => {
                  feedbackGenerator('selection')
                  this.props.navigation.navigate('Gallery')
                }}
              >
                <View>
                  <Entypo name="images" size={30} color="white" />
                  {(this.state.newPhotos || this.state.newVideos) && (
                    <View style={styles.newPhotosDot} />
                  )}
                </View>
              </TouchableOpacity>
              <View style={[styles.actionTypesCont]}>
                <TouchableOpacity
                  style={styles.actionTypeButton}
                  onPress={() => {
                    this.handleChangeActionType('video')
                  }}
                >
                  <Text
                    style={[
                      styles.actionTypeButtonText,
                      {
                        fontFamily: font,
                        color:
                          this.state.actionType === 'video'
                            ? '#fff'
                            : '#6b6b6b',
                      },
                    ]}
                  >
                    {i18n.t('video').toUpperCase()}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.actionTypeButton}
                  onPress={() => {
                    this.handleChangeActionType('photo')
                  }}
                >
                  <Text
                    style={[
                      styles.actionTypeButtonText,
                      {
                        fontFamily: font,
                        color:
                          this.state.actionType === 'video'
                            ? '#6b6b6b'
                            : '#fff',
                      },
                    ]}
                  >
                    {i18n.t('photo').toUpperCase()}
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={[styles.bottomButton, {}]}
                onPress={() => {
                  feedbackGenerator('selection')
                  this.props.navigation.navigate('VideoUpload', {
                    newVideo: this.state.newVideo,
                  })
                }}
                // disabled={!this.state.newVideos}
              >
                <Entypo
                  name="upload"
                  size={32}
                  color={
                    this.state.newVideos
                      ? constants.success
                      : 'rgba(255,255,255,.2)'
                  }
                />
              </TouchableOpacity>
            </View>
          </View>
        )
      }}
    </ThemeContext.Consumer>
  )

  renderMoreOptions = () => (
    <View style={styles.options}>
      <View style={styles.detectors}>
        <TouchableOpacity onPress={this.toggleFaceDetection}>
          <MaterialIcons
            name="tag-faces"
            size={32}
            color={this.state.faceDetecting ? 'white' : '#858585'}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.toggleBarcodeScanning}>
          <MaterialCommunityIcons
            name="barcode-scan"
            size={32}
            color={this.state.barcodeScanning ? 'white' : '#858585'}
          />
        </TouchableOpacity>
      </View>

      <View style={styles.pictureSizeContainer}>
        <Text style={styles.pictureQualityLabel}>Picture quality</Text>
        <View style={styles.pictureSizeChooser}>
          <TouchableOpacity
            onPress={this.previousPictureSize}
            style={{ padding: 6 }}
          >
            <Ionicons name="md-arrow-dropleft" size={14} color="white" />
          </TouchableOpacity>
          <View style={styles.pictureSizeLabel}>
            <Text style={{ color: 'white' }}>{this.state.pictureSize}</Text>
          </View>
          <TouchableOpacity
            onPress={this.nextPictureSize}
            style={{ padding: 6 }}
          >
            <Ionicons name="md-arrow-dropright" size={14} color="white" />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )

  renderCamera = () => (
    <View style={{ flex: 1 }}>
      {Device.isDevice === true ? (
        <Camera
          ref={ref => {
            this.camera = ref
          }}
          style={styles.camera}
          onCameraReady={this.collectPictureSizes}
          type={this.state.type}
          flashMode={this.state.flash}
          autoFocus={this.state.autoFocus}
          zoom={this.state.zoom}
          whiteBalance={this.state.whiteBalance}
          ratio={this.state.ratio}
          pictureSize={this.state.pictureSize}
          onMountError={this.handleMountError}
          onFacesDetected={
            this.state.faceDetecting ? this.onFacesDetected : undefined
          }
          onFaceDetectionError={this.onFaceDetectionError}
          barCodeScannerSettings={{
            barCodeTypes: [
              BarCodeScanner.Constants.BarCodeType.qr,
              BarCodeScanner.Constants.BarCodeType.pdf417,
            ],
          }}
          onBarCodeScanned={
            this.state.barcodeScanning ? this.onBarCodeScanned : undefined
          }
        >
          {this.renderTopBar()}
          {this.renderBottomBar()}
        </Camera>
      ) : (
        <View style={styles.camera}>
          {this.renderTopBar()}
          {this.renderBottomBar()}
        </View>
      )}
      {this.state.faceDetecting && this.renderFaces()}
      {this.state.faceDetecting && this.renderLandmarks()}
      {this.state.showMoreOptions && this.renderMoreOptions()}
    </View>
  )

  render() {
    const cameraScreenContent = this.state.permissionsGranted
      ? this.renderCamera()
      : this.renderNoPermissions()
    const content = this.state.showGallery
      ? this.renderGallery()
      : cameraScreenContent
    return (
      <View style={styles.container}>
        <NavigationEvents
          onWillFocus={payload => {}}
          onDidFocus={payload => {
            this.handleStatusBar(true)
          }}
          onWillBlur={payload => {
            this.handleStatusBar(false)
          }}
          onDidBlur={payload => {}}
        />
        {cameraScreenContent}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  camera: {
    flex: 1,
    justifyContent: 'space-between',
  },
  topBar: {
    flex: 0.2,
    backgroundColor: 'transparent',
    // paddingTop: Constants.statusBarHeight / 2,
    marginTop: isIPhoneX ? 30 : 0,
    // borderWidth: 0.5,
    borderColor: '#fff',
  },
  topBarControls: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    height: Header.HEIGHT,
    // borderWidth: 0.5,
    borderColor: '#fff',
  },
  bottomBar: {
    width: '100%',
    paddingBottom: isIPhoneX ? 25 : 5,
    backgroundColor: 'transparent',
    // alignSelf: 'flex-end',
    // justifyContent: 'space-between',
    // flex: 0.22,
    flexDirection: 'column',
    // borderWidth: 0.5,
    borderColor: '#fff',
  },
  noPermissions: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: constants.paddingHorizontal,
  },
  gallery: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  toggleButton: {
    flex: 0.25,
    height: 40,
    // marginHorizontal: 2,
    // marginBottom: 10,
    // marginTop: 20,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 0.5,
    borderColor: '#fff',
  },
  timerCont: {
    // borderWidth: 0.5,
    borderColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  autoFocusLabel: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  actionButtonCont: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  actionButton: {
    // alignSelf: 'center',
    // borderWidth: 0.5,
    borderColor: '#fff',
  },
  actionButtonsCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderWidth: 0.5,
    borderColor: '#fff',
  },
  bottomButton: {
    flex: 0.4,
    height: 58,
    justifyContent: 'center',
    alignItems: 'center',
    // borderWidth: 0.5,
    borderColor: '#fff',
  },
  actionTypesCont: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  actionTypeButton: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  actionTypeButtonText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: '700',
  },
  newPhotosDot: {
    position: 'absolute',
    top: 0,
    right: -5,
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: constants.success,
  },
  options: {
    position: 'absolute',
    bottom: 80,
    left: 30,
    width: 200,
    height: 160,
    backgroundColor: '#000000BA',
    borderRadius: 4,
    padding: 10,
  },
  detectors: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  pictureQualityLabel: {
    fontSize: 10,
    marginVertical: 3,
    color: 'white',
  },
  pictureSizeContainer: {
    flex: 0.5,
    alignItems: 'center',
    paddingTop: 10,
  },
  pictureSizeChooser: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  pictureSizeLabel: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  facesContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
  },
  face: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 6,
    position: 'absolute',
    borderColor: '#FFD700',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    backgroundColor: 'transparent',
  },
  landmark: {
    width: landmarkSize,
    height: landmarkSize,
    position: 'absolute',
    backgroundColor: 'red',
  },
  faceText: {
    color: '#FFD700',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
    backgroundColor: 'transparent',
  },
  row: {
    flexDirection: 'row',
  },
})
