import React, { useEffect } from 'react'
import { AsyncStorage } from 'react-native'
import * as Localization from 'expo-localization'
import { useSelector, useDispatch } from 'react-redux'
import { changeLang, getSavedUser } from '../actions'
import { Container, Loader } from '../components'
import { locales } from '../utils'

const AuthLoading = ({ navigation }) => {
  const { user } = useSelector(({ user }) => ({ user }))
  const { getUserMessage } = user
  const dispatch = useDispatch()

  useEffect(() => {
    getLang()
  }, [])

  useEffect(() => {
    if (getUserMessage === 'hasToken') {
      navigation.navigate('App')
    } else if (getUserMessage === 'hasNoToken') {
      navigation.navigate('Auth')
    }
  }, [getUserMessage])

  const getLang = async () => {
    try {
      const lang = await AsyncStorage.getItem('lang')
      if (lang !== null) {
        dispatch(changeLang(lang))
      } else {
        try {
          const { locale } = await Localization.getLocalizationAsync()
          if (locale) {
            const localeFound = locales.find(
              item => item.lang === locale.slice(0, 2)
            )
            if (localeFound) {
              dispatch(changeLang(localeFound.lang))
            } else {
              dispatch(changeLang('en'))
            }
          } else {
            dispatch(changeLang('en'))
          }
        } catch (error) {
          dispatch(changeLang('en'))
        }
      }
    } catch (error) {
      dispatch(changeLang('en'))
    }
    dispatch(getSavedUser())
  }

  return (
    <Container contStyle={{ alignItems: 'center', justifyContent: 'center' }}>
      <Loader indicatorType="skype" />
    </Container>
  )
}

export default AuthLoading
