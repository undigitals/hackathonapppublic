import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container } from '../components'
import constants from '../constants'

const GuideSections = ({ navigation }) => {
  const { service } = navigation.state.params
  useEffect(() => {
    navigation.setParams({ headerTitle: service.name })
  }, [])

  return <Container onRefresh={() => {}} refreshing={false}></Container>
}

export default GuideSections
