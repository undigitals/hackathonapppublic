import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, ScrollView } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container, View, TabButton, ListItem } from '../components'
import constants from '../constants'

const SharingProduct = ({ navigation }) => {
  const { product } = navigation.state.params

  useEffect(() => {
    navigation.setParams({ headerTitle: product.name })
  }, [])

  console.log(product)

  return <Container></Container>
}

export default SharingProduct

const styles = StyleSheet.create({})
