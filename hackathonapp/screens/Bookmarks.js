import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container, ListItem } from '../components'
import { getBookmarks } from '../utils'
import constants from '../constants'

const Bookmarks = ({ navigation }) => {
  const [bookmarkList, setBookmarkList] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    getBookmarks().then(list => {
      console.log(list)
      setBookmarkList(list)
      setLoading(false)
    })
  }, [])

  return (
    <Container onRefresh={() => {}} refreshing={loading}>
      {(() => {
        if (bookmarkList && bookmarkList.length > 0) {
          return bookmarkList.map(item => {
            let data = {
              imageSrc: { uri: item.image.url },
              textPrimary: item.name,
              textSecondary: item.details,
              textRight: item.price === 'free' ? 'Free' : `KRW ${item.price}`,
            }

            return (
              <ListItem
                key={item.id}
                type="order"
                onPress={() => {
                  navigation.navigate('ProductBookmarked', { product: item })
                }}
                data={data}
              />
            )
          })
        }
      })()}
    </Container>
  )
}

export default Bookmarks
