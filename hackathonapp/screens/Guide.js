import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, View } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import {
  Container,
  HomeHeaderLeft,
  HomeHeaderRight,
  ServiceCard,
} from '../components'
import uuid from 'uuid'
import { services } from '../utils'
import constants from '../constants'

const Guide = ({ navigation }) => {
  return (
    <Container onRefresh={() => {}} refreshing={false}>
      <View key={uuid.v4()} style={styles.sectionCont}>
        <View style={styles.servicesCont}>
          {(() => {
            return services.map(service => {
              return (
                <ServiceCard
                  type="regularService"
                  key={service.id}
                  serviceName={service.name}
                  serviceLogoUri={{
                    uri: `${service.image}`,
                  }}
                  onPress={() => {
                    navigation.navigate('GuideSections', { service })
                  }}
                />
              )
            })
          })()}
        </View>
      </View>
    </Container>
  )
}

export default Guide

Guide.navigationOptions = ({}) => {
  return {
    headerRight: <HomeHeaderRight />,
    headerLeft: <HomeHeaderLeft />,
  }
}

const styles = StyleSheet.create({
  sectionCont: {
    width: '100%',
  },
  servicesCont: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: constants.paddingHorizontal,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
})
