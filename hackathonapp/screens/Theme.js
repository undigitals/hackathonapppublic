import React, { useContext, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Container, ListItem } from '../components'
import { themes, feedbackGenerator } from '../utils'
import Constants from 'expo-constants'

const Theme = () => {
  const { theme, changeTheme } = useContext(ThemeContext)

  return (
    <Container>
      {themes.map(item => {
        return (
          <ListItem
            key={item.id}
            type="plain"
            onPress={() => {
              changeTheme(item.key)
              feedbackGenerator('selection')
            }}
            text={i18n.t(item.name)}
            selected={theme === item.key}
          />
        )
      })}
    </Container>
  )
}

export default Theme
