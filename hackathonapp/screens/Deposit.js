import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { depositMoney, depositReset } from '../actions'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import {
  Container,
  Label,
  HapticGenerator,
  Input,
  Message,
  Button,
} from '../components'
import { openBrowser } from '../utils'
import constants from '../constants'

const Deposit = ({ navigation }) => {
  const [amount, setAmount] = useState('')
  const [amountMsg, setAmountMsg] = useState('')
  const [errorMsg, setErrorMsg] = useState('')

  const dispatch = useDispatch()
  const { deposit } = useSelector(({ deposit }) => ({ deposit }))
  const { isDepositing, depositMessage, payPalLink } = deposit

  useEffect(() => {
    return () => {
      depositReset()
    }
  }, [])

  useEffect(() => {
    if (depositMessage === 'depositSuccess') {
      navigation.goBack(null)
      openBrowser(payPalLink)
    } else if (depositMessage !== '') {
      setErrorMsg(depositMessage)
      setTimeout(() => {
        setErrorMsg('')
      }, 2000)
    }
    return () => {
      dispatch(depositReset())
    }
  }, [depositMessage])

  const handleDeposit = () => {
    if (amount) {
      dispatch(depositMoney({ usd_amount: amount }))
    } else {
      setAmountMsg(i18n.t('depositAmountRequired'))
    }
  }

  const clearErrors = () => {
    setAmountMsg('')
  }

  return (
    <Container>
      <Label text="titleDeposit" />
      <HapticGenerator error={amountMsg !== ''}>
        <Label type="input" text="inputDepositAmount" />
        <Input
          value={amount}
          onChangeText={value => {
            setAmount(value)
            clearErrors()
          }}
          placeholder="000"
          maxLength={6}
          prefixContStyle={{ marginRight: 0 }}
          allowClear
          keyboardType="numeric"
          onClear={() => {
            setAmount('')
            clearErrors()
          }}
        />
        <Message text={amountMsg} />
      </HapticGenerator>

      <Message type="message" text={errorMsg && i18n.t(errorMsg)} />

      <Button
        text="payWithPayPal"
        loading={isDepositing}
        onPress={() => {
          handleDeposit()
        }}
      />
    </Container>
  )
}

export default Deposit
