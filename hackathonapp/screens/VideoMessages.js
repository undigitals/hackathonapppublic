import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, Alert } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { getMsgs, getMsgsReset, deleteMsg, deleteMsgReset } from '../actions'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import {
  Container,
  FlatListCont,
  RefreshControl,
  ListItemFlight,
} from '../components'
import { feedbackGenerator } from '../utils'
import constants from '../constants'

const VideoMessages = () => {
  const dispatch = useDispatch()
  const { flight } = useSelector(({ flight }) => ({ flight }))
  const {
    isGettingMsgs,
    getMsgsMessage,
    messages,
    isDeletingMsg,
    deleteMsgMessage,
  } = flight

  useEffect(() => {
    dispatch(getMsgs())
  }, [])

  const handleDeleteMsg = ({ id }) => {
    dispatch(
      deleteMsg({
        message_id: id,
      })
    )
  }

  const confirmDeleteMsg = data => {
    Alert.alert(
      i18n.t('confirmDeleteMsg'),
      '',
      [
        {
          text: i18n.t('cancel'),
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: i18n.t('delete'),
          onPress: () => {
            handleDeleteMsg(data)
          },
        },
      ],
      { cancelable: true }
    )
  }

  return (
    <Container type="view">
      {(() => {
        if (isGettingMsgs) {
          return null
        } else if (messages.length !== 0) {
          return (
            <FlatListCont
              data={messages}
              keyExtractor={item => item.id + ''}
              renderItem={({ item }) => {
                return (
                  <ListItemFlight
                    type="videoMsg"
                    onPress={() => {
                      console.log(item)
                    }}
                    onLongPress={() => {
                      feedbackGenerator('selection')
                      confirmDeleteMsg(item)
                    }}
                    data={item}
                  />
                )
              }}
              refreshControl={
                <RefreshControl
                  onRefresh={() => {}}
                  refreshing={isGettingMsgs}
                />
              }
              onEndThreshold={0.1}
              onEndReached={() => {}}
            />
          )
        }
      })()}
    </Container>
  )
}

export default VideoMessages
