import React from 'react'
import { StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import { Button, View, Container } from '../components'
import constants from '../constants'

class NewFarewell extends React.Component {
  render() {
    const { navigation } = this.props
    return (
      <Container>
        <Button
          type="plain"
          text="Go to Home"
          onPress={() => {
            navigation.navigate('Home')
          }}
        />
      </Container>
    )
  }
}

const mapStateToProps = ({}) => ({})

export default connect(mapStateToProps, {})(NewFarewell)
