import React from 'react'
import {} from 'react-native'
import { connect } from 'react-redux'
import { getNotifs, getMoreNotifs } from '../actions'
import {
  Container,
  FlatListCont,
  ListItemNotif,
  Button,
  Label,
  RefreshControl,
} from '../components'

class Notifs extends React.Component {
  componentDidMount() {
    const { getNotifs } = this.props
    getNotifs({ method: null })
  }

  render() {
    const {
      navigation,
      getNotifs,
      getMoreNotifs,
      notifsList,
      isGettingNotifs,
      isGettingMoreNotifs,
      isRefreshingNotifs,
      hasNextPage,
    } = this.props
    return (
      <Container
        type={notifsList.length === 0 ? null : 'view'}
        onRefresh={() => {
          if (notifsList.length === 0) {
            getNotifs({ method: 'refresh' })
          }
        }}
        refreshing={notifsList.length === 0 ? isRefreshingNotifs : false}
      >
        {(() => {
          if (isGettingNotifs) {
            {
              /* return [1, 2, 3, 4, 5, 6, 7, 8, 9].map(item => {
              return <Loader key={item} type="service" />
            }) */
            }
          } else if (notifsList.length !== 0) {
            return (
              <FlatListCont
                data={notifsList}
                keyExtractor={item => item.notification_id + ''}
                renderItem={({ item }) => {
                  let data = {
                    imageSrc: item.notification_img_url,
                    textPrimary: item.notification_title,
                    textSecondary: item.notification_content,
                    textDate: item.notification_date,
                  }
                  return (
                    <ListItemNotif
                      type="notif"
                      onPress={() => {
                        navigation.navigate('NotifDetails', { data: data })
                      }}
                      data={data}
                      showFullContent={false}
                    />
                  )
                }}
                refreshControl={
                  <RefreshControl
                    onRefresh={() => {
                      getNotifs({ method: 'refresh' })
                    }}
                    refreshing={isRefreshingNotifs}
                  />
                }
                onEndThreshold={0.1}
                onEndReached={() => {
                  if (!isGettingMoreNotifs && hasNextPage) {
                    getMoreNotifs()
                  }
                }}
                ListFooterComponent={() => {
                  if (!hasNextPage) {
                    return <Label type="listFooter" text="endOfNotifs" />
                  } else {
                    return (
                      <Button
                        type="plain"
                        text="loadingMore"
                        onPress={() => {
                          getMoreNotifs()
                        }}
                      />
                    )
                  }
                }}
              />
            )
          } else {
            return <Label type="plain" text="noNotifsYet" />
          }
        })()}
      </Container>
    )
  }
}

const mapStateToProps = ({ notifs }) => ({
  page: notifs.page,
  perPage: notifs.perPage,
  hasNextPage: notifs.hasNextPage,
  isGettingNotifs: notifs.isGettingNotifs,
  isGettingMoreNotifs: notifs.isGettingMoreNotifs,
  isRefreshingNotifs: notifs.isRefreshingNotifs,
  getOrdersMessage: notifs.getOrdersMessage,
  getMoreOrdersMessage: notifs.getMoreOrdersMessage,
  notifsList: notifs.notifsList,
})

export default connect(mapStateToProps, { getNotifs, getMoreNotifs })(Notifs)
