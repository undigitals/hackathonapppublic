import React, { useState, useEffect, useContext } from 'react'
import { Platform } from 'react-native'
import { Notifications } from 'expo'
import * as Permissions from 'expo-permissions'
import { NavigationEvents } from 'react-navigation'
import { useSelector, useDispatch, connect } from 'react-redux'
import {
  removeUser,
  resetRemoveUser,
  refreshUser,
  refreshUserReset,
  updateNotif,
  getSavedUser,
} from '../actions'
import {
  Container,
  HomeHeaderLeft,
  HomeHeaderRight,
  Button,
} from '../components'
import Responder from '../components/Responder'
import { createLocalNotif } from '../utils'

const Home = ({ navigation }) => {
  const [notification, setNotification] = useState(null)
  const dispatch = useDispatch()
  const { user } = useSelector(({ user }) => ({ user }))
  const { userTokenExpired, isRefreshingUser01 } = user

  useEffect(() => {
    dispatch(getSavedUser())
    handleCreateNotificationChannels()
    // registerForPushNotificationsAsync(dispatch)
    Notifications.addListener(handleNotification)
    // navigation.navigate('VideoReecord')
  }, [])

  useEffect(() => {
    if (userTokenExpired === true) {
      navigation.navigate('AuthLoading')
      dispatch(resetRemoveUser())
    }
  }, [userTokenExpired])

  const handleNotification = notification => {
    setNotification(notification)
    console.log(notification)
    if (notification) {
      const { origin, data } = notification
      if (origin === 'selected') {
        const { notification_type, notification_data } = data
        switch (notification_type) {
          case 'news':
            navigation.navigate('Notifs')
            break
          default:
            break
        }
      } else if (origin === 'received') {
        // createLocalNotif(notification)
      }
    }
  }

  return (
    // <Container type="view"
    //   // onRefresh={() => {
    //   //   dispatch(refreshUser({ method: 'refresh' }))
    //   // }}
    //   // refreshing={isRefreshingUser01}
    // >
    // <NavigationEvents
    //   onWillFocus={payload => {}}
    //   onDidFocus={payload => {
    //     dispatch(refreshUser({ method: null }))
    //   }}
    //   onWillBlur={payload => {}}
    //   onDidBlur={payload => {}}
    // />

    <Responder />
    // </Container>
  )
}

Home.navigationOptions = ({}) => {
  return {
    headerRight: <HomeHeaderRight />,
    headerLeft: <HomeHeaderLeft />,
    headerTitle: null,
  }
}

export default Home

const registerForPushNotificationsAsync = async dispatch => {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  )
  let finalStatus = existingStatus
  if (existingStatus !== 'granted') {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
    finalStatus = status
  }
  let formData = {
    notification_token: null,
    permission_status: finalStatus,
  }
  if (finalStatus !== 'granted') {
    dispatch(updateNotif(formData))
    return
  }
  let expoPushToken = await Notifications.getExpoPushTokenAsync()
  formData = Object.assign({}, formData, {
    notification_token: expoPushToken,
  })
  dispatch(updateNotif(formData))
}

const handleCreateNotificationChannels = () => {
  if (Platform.OS === 'android') {
    Notifications.createChannelAndroidAsync('iamlanded-notifications', {
      name: 'iamlanded notifications',
      sound: true,
      priority: 'max',
      vibrate: true,
    })
  }
}

const handleNotifcationBadges = async () => {
  let badges = await Notifications.getBadgeNumberAsync()
  if (badges !== 0) {
    Notifications.setBadgeNumberAsync(0)
  }
}
