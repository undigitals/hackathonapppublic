import React, { useContext, useState, useEffect } from 'react'
import { View } from 'react-native'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { ThemeContext } from '../context'
import i18n from 'i18n-js'
import {
  Container,
  HomeHeaderLeft,
  HomeHeaderRight,
  ServiceCard,
} from '../components'
import uuid from 'uuid'
import constants from '../constants'
import { botTypes } from '../utils'

const Assistant = ({ navigation }) => {
  return (
    <Container onRefresh={() => {}} refreshing={false}>
      <View key={uuid.v4()} style={styles.sectionCont}>
        <View style={styles.servicesCont}>
          {(() => {
            return botTypes.map(service => {
              return (
                <ServiceCard
                  type="regularService"
                  key={service.id}
                  serviceName={service.name}
                  serviceLogoUri={{
                    uri: `${service.image}`,
                  }}
                  onPress={() => {
                    navigation.navigate('ChatBotOptimized', {
                      type: service.type,
                      name: service.name,
                    })
                  }}
                />
              )
            })
          })()}
        </View>
      </View>
    </Container>
  )
}

export default Assistant

Assistant.navigationOptions = ({}) => {
  return {
    headerRight: <HomeHeaderRight />,
    headerLeft: <HomeHeaderLeft />,
  }
}

const styles = StyleSheet.create({
  sectionCont: {
    width: '100%',
  },
  servicesCont: {
    // borderWidth: 0.5,
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: constants.paddingHorizontal,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
})
