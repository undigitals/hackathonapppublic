import axios from 'axios'
import {
  GET_RELATIVES_REQUEST,
  GET_RELATIVES_SUCCESS,
  GET_RELATIVES_FAILURE,
  GET_RELATIVES_RESET,
  ADD_RELATIVE_REQUEST,
  ADD_RELATIVE_SUCCESS,
  ADD_RELATIVE_FAILURE,
  ADD_RELATIVE_RESET,
  DELETE_RELATIVE_REQUEST,
  DELETE_RELATIVE_SUCCESS,
  DELETE_RELATIVE_FAILURE,
  DELETE_RELATIVE_RESET,
} from './types'
import { getSafelyAsync, deleteSafelySavedAsync } from './secureStoreActions'
import { apiUrl, requestConfig } from '../utils'

const addRelativeFail = msg => {
  return {
    type: ADD_RELATIVE_FAILURE,
    payload: { message: msg },
  }
}

export const addRelativeReset = () => dispatch => {
  dispatch({ type: ADD_RELATIVE_RESET })
}

export const addRelative = formData => dispatch => {
  dispatch({ type: ADD_RELATIVE_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      let data = formData
      data = Object.assign(data, {
        user_token: user,
      })
      console.log(JSON.stringify(data))
      axios
        .post(`${apiUrl}relative/create`, data)
        .then(res => {
          console.log('/relative/create', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(addRelativeFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.message === 'exists_email') {
              dispatch(addRelativeFail('errorEmailExists'))
            } else if (res.data.message === 'exists_phone_number') {
              dispatch(addRelativeFail('errorPhoneNumberExists'))
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            }
          } else {
            if (res.data.message === 'successfully_saved' && res.data.data) {
              dispatch({
                type: ADD_RELATIVE_SUCCESS,
                payload: {
                  message: 'addRelativeSuccess',
                  data: res.data.data,
                },
              })
              dispatch(getRelatives())
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(addRelativeFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}

const deleteRelativeFail = msg => {
  return {
    type: DELETE_RELATIVE_FAILURE,
    payload: { message: msg },
  }
}

export const deleteRelativeReset = () => dispatch => {
  dispatch({ type: DELETE_RELATIVE_RESET })
}

export const deleteRelative = formData => dispatch => {
  dispatch({ type: DELETE_RELATIVE_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      let data = formData
      data = Object.assign(data, {
        user_token: user,
      })
      console.log(JSON.stringify(data))
      axios
        .post(`${apiUrl}relative/delete`, data)
        .then(res => {
          console.log('/relative/delete', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(deleteRelativeFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.message === 'relative_not_found') {
              dispatch(deleteRelativeFail('errorRelativeNotFound'))
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            }
          } else {
            if (res.data.message === 'relative_successfully_deleted') {
              dispatch({
                type: DELETE_RELATIVE_SUCCESS,
                payload: {
                  message: 'deleteRelativeSuccess',
                },
              })
              dispatch(getRelatives())
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(deleteRelativeFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}

const getRelativesFail = msg => {
  return {
    type: GET_RELATIVES_FAILURE,
    payload: { message: msg },
  }
}

export const getRelativesReset = () => dispatch => {
  dispatch({ type: GET_RELATIVES_RESET })
}

export const getRelatives = () => dispatch => {
  dispatch({ type: GET_RELATIVES_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      let data = {}
      data = Object.assign(data, {
        user_token: user,
      })
      console.log(JSON.stringify(data))
      axios
        .post(`${apiUrl}relatives`, data, requestConfig)
        .then(res => {
          console.log('/relatives', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(getRelativesFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            } else {
              dispatch(getRelativesFail('errorServer'))
            }
          } else {
            if (res.data.data) {
              dispatch({
                type: GET_RELATIVES_SUCCESS,
                payload: {
                  message: 'getRelativesSuccess',
                  data: res.data.data,
                },
              })
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(getRelativesFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}
