import { AsyncStorage } from 'react-native'
import { CHANGE_LANG } from './types'

const setLang = async lang => {
  try {
    await AsyncStorage.setItem('lang', lang)
  } catch (error) {
    // console.log('error setLang: ', error)
  }
}

export const changeLang = lang => dispatch => {
  setLang(lang)
  dispatch({ type: CHANGE_LANG, payload: lang })
}
