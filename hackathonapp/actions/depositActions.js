import axios from 'axios'
import {
  GET_DEPOSITS_REQUEST,
  GET_DEPOSITS_SUCCESS,
  GET_DEPOSITS_FAILURE,
  GET_DEPOSITS_RESET,
  DEPOSIT_REQUEST,
  DEPOSIT_SUCCESS,
  DEPOSIT_FAILURE,
  DEPOSIT_RESET,
} from './types'
import { getSafelyAsync, deleteSafelySavedAsync } from './secureStoreActions'
import { apiUrl, requestConfig } from '../utils'

const depositFail = msg => {
  return {
    type: DEPOSIT_FAILURE,
    payload: { message: msg },
  }
}

export const depositReset = () => dispatch => {
  dispatch({ type: DEPOSIT_RESET })
}

export const depositMoney = formData => dispatch => {
  dispatch({ type: DEPOSIT_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      let data = formData
      data = Object.assign(data, {
        user_token: user,
      })
      console.log(JSON.stringify(data))
      axios
        .post(`${apiUrl}user/deposit`, data)
        .then(res => {
          console.log('/user/deposit', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(depositFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.message === 'paypal_error') {
              dispatch(depositFail('errorPayPal'))
            } else if (res.data.message === 'server_side_error') {
              dispatch(depositFail('errorServer'))
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            }
          } else {
            if (res.data.data) {
              dispatch({
                type: DEPOSIT_SUCCESS,
                payload: {
                  message: 'depositSuccess',
                  url: res.data.data.url,
                  deposit: res.data.data.deposit,
                },
              })
              dispatch(
                getDeposits({
                  per_page: 30,
                  page: 1,
                })
              )
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(depositFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}

const getDepositsFail = msg => {
  return {
    type: GET_DEPOSITS_FAILURE,
    payload: { message: msg },
  }
}

export const getDepositsReset = () => dispatch => {
  dispatch({ type: GET_DEPOSITS_RESET })
}

export const getDeposits = formData => dispatch => {
  dispatch({ type: GET_DEPOSITS_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      let data = formData
      data = Object.assign(data, {
        user_token: user,
      })
      console.log(JSON.stringify(data))
      axios
        .post(`${apiUrl}user/get-deposits`, data, requestConfig)
        .then(res => {
          console.log('/user/get-deposits', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(getDepositsFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            } else {
              dispatch(getDepositsFail('errorServer'))
            }
          } else {
            if (res.data.data) {
              dispatch({
                type: GET_DEPOSITS_SUCCESS,
                payload: {
                  message: 'getDepositsSuccess',
                  data: res.data.data,
                },
              })
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(getDepositsFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}
