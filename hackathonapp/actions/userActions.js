import axios from 'axios'
import {
  SET_USER,
  GET_USER,
  GET_USER_RESET,
  REMOVE_USER,
  REMOVE_USER_RESET,
  SET_TEMP_MAINTENANCE,
  REFRESH_USER_REQUEST,
  REFRESH_USER_REQUEST01,
  REFRESH_USER_FAILURE,
  REFRESH_USER_SUCCESS,
  REFRESH_USER_RESET,
  UPDATE_NOTIF_REQUEST,
  UPDATE_NOTIF_FAILURE,
  UPDATE_NOTIF_SUCCESS,
  UPDATE_NOTIF_RESET,
  GET_UPDATE_PHONE_VERIF_REQUEST,
  GET_UPDATE_PHONE_VERIF_SUCCESS,
  GET_UPDATE_PHONE_VERIF_FAILURE,
  GET_UPDATE_PHONE_VERIF_RESET,
  SEND_UPDATE_PHONE_VERIF_REQUEST,
  SEND_UPDATE_PHONE_VERIF_SUCCESS,
  SEND_UPDATE_PHONE_VERIF_FAILURE,
  SEND_UPDATE_PHONE_VERIF_RESET,
  UPDATE_LANG_REQUEST,
  UPDATE_LANG_FAILURE,
  UPDATE_LANG_SUCCESS,
  UPDATE_LANG_RESET,
} from './types'
import { getSafelyAsync, deleteSafelySavedAsync } from './secureStoreActions'
import { apiUrl, requestConfig } from '../utils'

export const getSavedUser = () => dispatch => {
  getSafelyAsync(
    'user',
    user => {
      dispatch({
        type: GET_USER,
        payload: {
          message: 'hasToken',
          data: user,
        },
      })
    },
    () => {
      dispatch({
        type: GET_USER,
        payload: {
          message: 'hasNoToken',
          data: null,
        },
      })
    }
  )
}

export const resetGetSavedUser = () => dispatch => {
  dispatch({ type: GET_USER_RESET })
}

export const setUser = data => dispatch => {
  dispatch({
    type: SET_USER,
    payload: data,
  })
}

export const removeUser = () => dispatch => {
  deleteSafelySavedAsync('user', error => {
    // console.log('could not remove the user: ', error)
  })
  // deleteSafelySavedAsync('cardInfo', () => {
  //   // console.log('error deleting cardInfo')
  // })
  dispatch({ type: REMOVE_USER })
}

export const resetRemoveUser = () => dispatch => {
  dispatch({ type: REMOVE_USER_RESET })
}

export const handleTempMaintenance = status => dispatch => {
  dispatch({ type: SET_TEMP_MAINTENANCE, payload: status ? status : true })
}

const refreshUserFail = msg => {
  return {
    type: REFRESH_USER_FAILURE,
    payload: { message: msg },
  }
}

export const refreshUserReset = () => dispatch => {
  dispatch({ type: LOGIN_RESET })
}

export const refreshUser = ({ method }) => dispatch => {
  if (method === 'refresh') {
    dispatch({ type: REFRESH_USER_REQUEST01 })
  } else {
    dispatch({ type: REFRESH_USER_REQUEST })
  }

  getSafelyAsync(
    'user',
    user => {
      let data = {}
      data = Object.assign(
        {},
        {
          user_token: user,
        }
      )
      axios
        .post(`${apiUrl}user/info`, data, requestConfig)
        .then(res => {
          // console.log('/user/info', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(refreshUserFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            }
          } else if (res.data.error === false) {
            if (res.data.data) {
              dispatch({
                type: REFRESH_USER_SUCCESS,
                payload: {
                  message: 'refreshUserSuccess',
                  data: res.data.data,
                },
              })
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(refreshUserFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}

const updateNotifFail = msg => {
  return {
    type: UPDATE_NOTIF_FAILURE,
    payload: { message: msg },
  }
}

export const updateNotifReset = () => dispatch => {
  dispatch({ type: UPDATE_NOTIF_RESET })
}

export const updateNotif = formData => dispatch => {
  dispatch({ type: UPDATE_NOTIF_REQUEST })
  console.log(formData)
  getSafelyAsync(
    'user',
    user => {
      let data = formData
      data = Object.assign(data, {
        user_token: user,
      })
      axios
        .post(`${apiUrl}user-notification-update`, data, requestConfig)
        .then(res => {
          // console.log('/user-notification-update', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(updateNotifFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            }
          } else if (res.data.error === false) {
            if (
              res.data.message === 'user_notification_data_successfully_updated'
            ) {
              dispatch({
                type: UPDATE_NOTIF_SUCCESS,
                payload: {
                  message: 'updateNotifSuccess',
                },
              })
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(updateNotifFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}

const updateLangFail = msg => {
  return {
    type: UPDATE_LANG_FAILURE,
    payload: { message: msg },
  }
}

export const updateLangReset = () => dispatch => {
  dispatch({ type: UPDATE_LANG_RESET })
}

export const updateLang = formData => dispatch => {
  dispatch({ type: UPDATE_LANG_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      let data = formData
      data = Object.assign(data, {
        user_token: user,
      })
      axios
        .post(`${apiUrl}user-lang-update`, data, requestConfig)
        .then(res => {
          console.log('/user-lang-update', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(updateLangFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            }
          } else if (res.data.error === false) {
            if (res.data.message === 'user_lang_updated_successfully') {
              dispatch({
                type: UPDATE_LANG_SUCCESS,
                payload: {
                  message: 'updateLangSuccess',
                },
              })
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(updateLangFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}

export const updatePhoneFail = (action, message) => {
  if (action === 'verify') {
    return {
      type: GET_UPDATE_PHONE_VERIF_FAILURE,
      payload: {
        message: message,
      },
    }
  } else {
    return {
      type: SEND_UPDATE_PHONE_VERIF_FAILURE,
      payload: {
        message: message,
      },
    }
  }
}

export const updatePhone = formData => dispatch => {
  const { action } = formData
  if (action === 'verify') {
    dispatch({ type: GET_UPDATE_PHONE_VERIF_REQUEST })
  } else {
    dispatch({ type: SEND_UPDATE_PHONE_VERIF_REQUEST })
  }
  getSafelyAsync(
    'user',
    user => {
      let data = formData
      data = Object.assign(data, {
        user_token: user,
        action: action === 'verify' ? 'verfy' : action,
      })
      console.log(data)
      axios
        .post(`${apiUrl}user-phone-update`, data, requestConfig)
        .then(res => {
          console.log('/user-phone-update', res.data)
          if (res.data) {
            if (action === 'verify') {
              if (res.data.error) {
                if (res.data.message === 'contact_service_center') {
                  dispatch(updatePhoneFail(action, 'errorUserStatus'))
                  dispatch(removeUser())
                } else if (res.data.message === 'exist_phone_number') {
                  dispatch(updatePhoneFail(action, 'errorPhoneNumberExist'))
                } else if (res.data.global_maintenance) {
                  dispatch(handleTempMaintenance())
                }
              } else if (res.data.error === false) {
                if (res.data.message === 'sms_sent') {
                  dispatch({
                    type: GET_UPDATE_PHONE_VERIF_SUCCESS,
                    payload: {
                      message: 'updatePhoneVerificationCodeSent',
                    },
                  })
                }
              }
            } else {
              if (res.data.error) {
                if (res.data.message === 'contact_service_center') {
                  dispatch(updatePhoneFail(action, 'errorUserStatus'))
                  dispatch(removeUser())
                } else if (res.data.message === 'invalid_sms_code') {
                  dispatch(updatePhoneFail(action, 'invalidVerifCode'))
                } else if (res.data.global_maintenance) {
                  dispatch(handleTempMaintenance())
                }
              } else if (res.data.error === false) {
                if (res.data.message === 'user_phone_updated_successfully') {
                  dispatch({
                    type: SEND_UPDATE_PHONE_VERIF_SUCCESS,
                    payload: {
                      message: 'updatePhoneSuccess',
                      data: res.data.user,
                    },
                  })
                }
              }
            }
          } else {
            dispatch(updatePhoneFail(action, 'errorFetch'))
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(updatePhoneFail(action, 'errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}

export const getUpdatePhoneVerifReset = () => dispatch => {
  dispatch({ type: GET_UPDATE_PHONE_VERIF_RESET })
}

export const sendUpdatePhoneVerifReset = () => dispatch => {
  dispatch({ type: SEND_UPDATE_PHONE_VERIF_RESET })
}
