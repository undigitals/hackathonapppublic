import axios from 'axios'
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGIN_RESET,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  REGISTER_RESET,
  GOOGLE_SIGN_IN_REQUEST,
  GOOGLE_SIGN_IN_FAILURE,
  GOOGLE_SIGN_IN_SUCCESS,
  GOOGLE_SIGN_IN_RESET,
  FB_SIGN_IN_REQUEST,
  FB_SIGN_IN_FAILURE,
  FB_SIGN_IN_SUCCESS,
  FB_SIGN_IN_RESET,
} from './types'
import { apiUrl, requestConfig, feedbackGenerator } from '../utils'
import { setUser } from './userActions'
import { setSafelyAsync } from './secureStoreActions'

const loginFail = msg => {
  return {
    type: LOGIN_FAILURE,
    payload: { message: msg },
  }
}

export const loginReset = () => dispatch => {
  dispatch({ type: LOGIN_RESET })
}

export const login = data => dispatch => {
  dispatch({ type: LOGIN_REQUEST })
  console.log(JSON.stringify(data))
  axios
    .post(`${apiUrl}login`, data, requestConfig)
    .then(res => {
      console.log('/login', res.data)
      if (res.data.error) {
        if (res.data.message === 'contact_service_center') {
          dispatch(loginFail('errorUserStatus'))
        } else if (res.data.message === 'wrong_email_or_password') {
          dispatch(loginFail('errorWrongEmailPassword'))
          feedbackGenerator('error')
        }
      } else {
        if (
          res.data.message === 'successful_authentication' &&
          res.data.token &&
          res.data.user
        ) {
          setSafelyAsync('user', res.data.token)
          dispatch(setUser(res.data.user))
          dispatch({
            type: LOGIN_SUCCESS,
            payload: {
              message: 'loginSuccess',
            },
          })
        }
      }
    })
    .catch(err => {
      console.log(err.response.data)
      dispatch(loginFail('errorFetch'))
      feedbackGenerator('error')
    })
}

const registerFail = msg => {
  return {
    type: REGISTER_FAILURE,
    payload: { message: msg },
  }
}

export const registerReset = () => dispatch => {
  dispatch({ type: REGISTER_RESET })
}

export const register = data => dispatch => {
  console.log(data)
  dispatch({ type: REGISTER_REQUEST })
  axios
    .post(`${apiUrl}register`, data, requestConfig)
    .then(res => {
      console.log('/register', res.data)
      if (res.data.error) {
        if (res.data.message === 'contact_service_center') {
          dispatch(registerFail('errorUserStatus'))
        } else if (res.data.message === 'exist_email') {
          dispatch(registerFail('errorEmailExist'))
          feedbackGenerator('error')
        }
      } else if (res.data.error === false) {
        if (
          res.data.message === 'successful_registration' &&
          res.data.token &&
          res.data.user
        ) {
          setSafelyAsync('user', res.data.token)
          dispatch(setUser(res.data.user))
          dispatch({
            type: REGISTER_SUCCESS,
            payload: {
              message: 'registerSuccess',
            },
          })
        }
      }
    })
    .catch(err => {
      console.log(err.response.data)
      dispatch(registerFail('errorFetch'))
      feedbackGenerator('error')
    })
}

// signInWithGoogle
const signInWithGoogleFail = msg => {
  return {
    type: GOOGLE_SIGN_IN_FAILURE,
    payload: { message: msg },
  }
}

export const signInWithGoogleReset = () => dispatch => {
  dispatch({ type: GOOGLE_SIGN_IN_RESET })
}

export const signInWithGoogle = data => dispatch => {
  dispatch({ type: GOOGLE_SIGN_IN_REQUEST })
  console.log(JSON.stringify(data))
  axios
    .post(`${apiUrl}login/social`, data, requestConfig)
    .then(res => {
      console.log('/login/social', res.data)
      if (res.data.error) {
        if (res.data.message === 'contact_service_center') {
          dispatch(signInWithGoogleFail('errorUserStatus'))
        } else if (res.data.message === 'fail_authentication') {
          dispatch(signInWithGoogleFail('errorAuthFailed'))
          feedbackGenerator('error')
        }
      } else {
        if (
          res.data.message === 'successful_authentication' &&
          res.data.token &&
          res.data.user
        ) {
          setSafelyAsync('user', res.data.token)
          dispatch(setUser(res.data.user))
          dispatch({
            type: GOOGLE_SIGN_IN_SUCCESS,
            payload: {
              message: 'signInWithGoogleSuccess',
            },
          })
        }
      }
    })
    .catch(err => {
      console.log(err.response.data)
      dispatch(signInWithGoogleFail('errorFetch'))
      feedbackGenerator('error')
    })
}

// signInWithFacebook
const signInWithFacebookFail = msg => {
  return {
    type: FB_SIGN_IN_FAILURE,
    payload: { message: msg },
  }
}

export const signInWithFacebookReset = () => dispatch => {
  dispatch({ type: FB_SIGN_IN_RESET })
}

export const signInWithFacebook = data => dispatch => {
  dispatch({ type: FB_SIGN_IN_REQUEST })
  console.log(JSON.stringify(data))
  axios
    .post(`${apiUrl}login/social`, data, requestConfig)
    .then(res => {
      console.log('/login/social', res.data)
      if (res.data.error) {
        if (res.data.message === 'contact_service_center') {
          dispatch(signInWithFacebookFail('errorUserStatus'))
        } else if (res.data.message === 'fail_authentication') {
          dispatch(signInWithFacebookFail('errorAuthFailed'))
          feedbackGenerator('error')
        }
      } else {
        if (
          res.data.message === 'successful_authentication' &&
          res.data.token &&
          res.data.user
        ) {
          setSafelyAsync('user', res.data.token)
          dispatch(setUser(res.data.user))
          dispatch({
            type: FB_SIGN_IN_SUCCESS,
            payload: {
              message: 'signInWithFacebookSuccess',
            },
          })
        }
      }
    })
    .catch(err => {
      console.log(err.response.data)
      dispatch(signInWithFacebookFail('errorFetch'))
      feedbackGenerator('error')
    })
}
