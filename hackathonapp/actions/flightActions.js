import axios from 'axios'
import {
  SEARCH_FLIGHT_REQUEST,
  SEARCH_FLIGHT_FAILURE,
  SEARCH_FLIGHT_SUCCESS,
  SEARCH_FLIGHT_RESET,
  GET_MSGS_REQUEST,
  GET_MSGS_FAILURE,
  GET_MSGS_SUCCESS,
  GET_MSGS_RESET,
  CREATE_MSG_REQUEST,
  CREATE_MSG_SUCCESS,
  CREATE_MSG_FAILURE,
  CREATE_MSG_RESET,
  DELETE_MSG_REQUEST,
  DELETE_MSG_SUCCESS,
  DELETE_MSG_FAILURE,
  DELETE_MSG_RESET,
  NEW_MSG_UPDATE,
  NEW_MSG_RESET,
} from './types'
import { getSafelyAsync, deleteSafelySavedAsync } from './secureStoreActions'
import { apiUrl, requestConfig, feedbackGenerator } from '../utils'

const searchFlightFail = msg => {
  return {
    type: SEARCH_FLIGHT_FAILURE,
    payload: { message: msg },
  }
}

export const searchFlightReset = () => dispatch => {
  dispatch({ type: SEARCH_FLIGHT_RESET })
}

export const searchFlight = formData => dispatch => {
  dispatch({ type: SEARCH_FLIGHT_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      let data = formData
      data = Object.assign(data, {
        user_token: user,
      })
      console.log(JSON.stringify(data))
      axios
        .post(`${apiUrl}get-flight-info`, data, requestConfig)
        .then(res => {
          console.log('/get-flight-info', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(searchFlightFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            } else if (res.data.message === 'wrong_flight_id') {
              feedbackGenerator('error')
              dispatch(searchFlightFail('errorFlightId'))
            }
          } else {
            if (res.data.data) {
              dispatch({
                type: SEARCH_FLIGHT_SUCCESS,
                payload: {
                  message: 'searchFlightSuccess',
                  data: res.data.data,
                },
              })
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(searchFlightFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}

export const newMsgUpdate = data => {
  return {
    type: NEW_MSG_UPDATE,
    payload: data,
  }
}

export const newMsgReset = () => {
  return {
    type: NEW_MSG_RESET,
  }
}

const createMsgFail = msg => {
  return {
    type: CREATE_MSG_FAILURE,
    payload: { message: msg },
  }
}

export const createMsgReset = () => dispatch => {
  dispatch({ type: CREATE_MSG_RESET })
}

export const createMsg = formData => dispatch => {
  dispatch({ type: CREATE_MSG_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      let data = formData
      data.append('user_token', user)
      console.log(JSON.stringify(data))
      axios
        .post(`${apiUrl}message/create`, data)
        .then(res => {
          console.log('/message/create', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(createMsgFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.message === 'wrong_flight_id') {
              dispatch(createMsgFail('errorFlightId'))
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            }
          } else {
            if (res.data.message === 'successfully_saved' && res.data.data) {
              dispatch({
                type: CREATE_MSG_SUCCESS,
                payload: {
                  message: 'createMsgSuccess',
                  data: res.data.data,
                },
              })
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(createMsgFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}

const deleteMsgFail = msg => {
  return {
    type: DELETE_MSG_FAILURE,
    payload: { message: msg },
  }
}

export const deleteMsgReset = () => dispatch => {
  dispatch({ type: DELETE_MSG_RESET })
}

export const deleteMsg = formData => dispatch => {
  dispatch({ type: DELETE_MSG_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      let data = formData
      data = Object.assign(data, {
        user_token: user,
      })
      console.log(JSON.stringify(data))
      axios
        .post(`${apiUrl}message/delete`, data)
        .then(res => {
          console.log('/message/delete', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(deleteMsgFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.message === 'message_not_found') {
              dispatch(deleteMsgFail('errorMsgNotFound'))
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            }
          } else {
            if (res.data.message === 'message_successfully_deleted') {
              dispatch({
                type: DELETE_MSG_SUCCESS,
                payload: {
                  message: 'deleteMsgSuccess',
                },
              })
              dispatch(getMsgs())
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(createMsgFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}

const getMsgsFail = msg => {
  return {
    type: GET_MSGS_FAILURE,
    payload: { message: msg },
  }
}

export const getMsgsReset = () => dispatch => {
  dispatch({ type: GET_MSGS_RESET })
}

export const getMsgs = () => dispatch => {
  dispatch({ type: GET_MSGS_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      let data = {}
      data = Object.assign(data, {
        user_token: user,
      })
      console.log(JSON.stringify(data))
      axios
        .post(`${apiUrl}messages`, data, requestConfig)
        .then(res => {
          console.log('/messages', res.data)
          if (res.data.error) {
            if (res.data.message === 'contact_service_center') {
              dispatch(getMsgsFail('errorUserStatus'))
              dispatch(removeUser())
            } else if (res.data.global_maintenance) {
              dispatch(handleTempMaintenance())
            } else {
              dispatch(getMsgsFail('errorServer'))
            }
          } else {
            if (res.data.data) {
              dispatch({
                type: GET_MSGS_SUCCESS,
                payload: {
                  message: 'getMsgsSuccess',
                  data: res.data.data,
                },
              })
            }
          }
        })
        .catch(err => {
          console.log(err.response.data)
          dispatch(getMsgsFail('errorFetch'))
        })
    },
    error => {
      console.log(error)
    }
  )
}
