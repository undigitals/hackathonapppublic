import * as SecureStore from 'expo-secure-store'

export const setSafelyAsync = async (key, value) => {
  try {
    await SecureStore.setItemAsync(key, JSON.stringify(value))
  } catch (error) {
    // console.log('setSafelyAsync error:', error)
  }
}

export const getSafelyAsync = async (key, callback, errorCallback) => {
  try {
    const value = await SecureStore.getItemAsync(key)
    if (value !== null) {
      callback(JSON.parse(value))
    } else {
      errorCallback(`${key} is null or !${key}`)
    }
  } catch (error) {
    errorCallback(error)
  }
}

export const deleteSafelySavedAsync = async (key, errorCallback) => {
  try {
    await SecureStore.deleteItemAsync(key)
  } catch (error) {
    // console.log('deleteSafelySavedAsync', error)
    errorCallback(error)
  }
}
