import {
  GET_NOTIFS_REQUEST,
  GET_NOTIFS_FAILURE,
  GET_NOTIFS_SUCCESS,
  GET_NOTIFS_RESET,
  REFRESH_NOTIFS_REQUEST,
  GET_MORE_NOTIFS_REQUEST,
  GET_MORE_NOTIFS_FAILURE,
  GET_MORE_NOTIFS_SUCCESS,
  GET_MORE_NOTIFS_RESET,
  UPDATE_BADGE_COUNT,
} from './types'
import { removeUser } from './userActions'
import { getSafelyAsync, deleteSafelySavedAsync } from './secureStoreActions'
import { apiUrl, fetchOptions } from '../utils'

export const updateBadgeCount = count => {
  return {
    type: UPDATE_BADGE_COUNT,
    payload: count,
  }
}

export const getNotifs = ({ method }) => (dispatch, getState) => {
  if (method === 'refresh') {
    dispatch({ type: REFRESH_NOTIFS_REQUEST })
  } else {
    dispatch({ type: GET_NOTIFS_REQUEST })
  }
  getSafelyAsync(
    'user',
    user => {
      const { page, perPage } = getState().notifs
      fetch(
        `${apiUrl}get-notifications`,
        Object.assign(fetchOptions, {
          body: JSON.stringify(
            Object.assign(
              {},
              {
                user_token: user,
                page: 1,
                per_page: perPage,
              }
            )
          ),
        })
      )
        .then(res => res.json())
        .then(res => {
          console.log('getNotifs:', res)
          if (res.error) {
            if (res.message === 'contact_service_center') {
              dispatch(removeUser())
            } else {
              dispatch({
                type: GET_NOTIFS_FAILURE,
                payload: { message: res.message },
              })
            }
          } else {
            if (res.data) {
              dispatch({
                type: GET_NOTIFS_SUCCESS,
                payload: {
                  message: 'gotNotifs',
                  data: res.data,
                  page: 2,
                  hasNextPage: res.data.length === perPage,
                },
              })
            } else {
            }
          }
        })
        .catch(error => {
          console.log('getNotifs error:', error)
          dispatch({
            type: GET_NOTIFS_FAILURE,
            payload: { message: 'getNotificationsFetchFailed' },
          })
        })
    },
    error => {
      console.log(error)
    }
  )
}

export const getMoreNotifs = () => (dispatch, getState) => {
  dispatch({ type: GET_MORE_NOTIFS_REQUEST })
  getSafelyAsync(
    'user',
    user => {
      const { page, perPage } = getState().notifs
      fetch(
        `${apiUrl}get-notifications`,
        Object.assign(fetchOptions, {
          body: JSON.stringify(
            Object.assign(
              {},
              {
                user_token: user,
                page: page,
                per_page: perPage,
              }
            )
          ),
        })
      )
        .then(res => res.json())
        .then(res => {
          console.log('getMoreNotifs:', res)
          if (res.error) {
            if (res.message === 'contact_service_center') {
              dispatch(removeUser())
            } else {
              dispatch({
                type: GET_MORE_NOTIFS_FAILURE,
                payload: { message: res.message },
              })
            }
          } else {
            if (res.data) {
              dispatch({
                type: GET_MORE_NOTIFS_SUCCESS,
                payload: {
                  message: 'gotMoreNotifs',
                  data: res.data,
                  page: page + 1,
                  hasNextPage: res.data.length === perPage,
                },
              })
            } else {
            }
          }
        })
        .catch(error => {
          console.log('getMoreNotifs error:', error)
          dispatch({
            type: GET_MORE_NOTIFS_FAILURE,
            payload: { message: 'getMoreNotificationsFetchFailed' },
          })
        })
    },
    error => {
      console.log(error)
    }
  )
}
