import React from 'react'
import { View } from 'react-native'
import { useSelector } from 'react-redux'
import i18n from 'i18n-js'
import { en, ko, ru } from '../locale/languages'

const LocaleProvider = ({ children }) => {
  const { locale } = useSelector(({ locale }) => ({ locale }))
  i18n.fallbacks = false
  i18n.translations = { en, ko, ru }
  i18n.locale = locale.lang
  return <View style={{ flexGrow: 1 }}>{children}</View>
}

export default LocaleProvider
