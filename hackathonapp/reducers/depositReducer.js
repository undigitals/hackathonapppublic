import {
  GET_DEPOSITS_REQUEST,
  GET_DEPOSITS_SUCCESS,
  GET_DEPOSITS_FAILURE,
  GET_DEPOSITS_RESET,
  DEPOSIT_REQUEST,
  DEPOSIT_SUCCESS,
  DEPOSIT_FAILURE,
  DEPOSIT_RESET,
} from '../actions/types'

const initialState = {
  // getRelatives
  isGettingDeposts: false,
  getDepositssMessage: '',
  deposits: [],

  // addRelative
  isDepositing: false,
  depositMessage: '',
  depositDetails: null,
  payPalLink: null,
}

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_DEPOSITS_REQUEST:
      return {
        ...state,
        isGettingDeposts: true,
      }
    case GET_DEPOSITS_FAILURE:
      return {
        ...state,
        isGettingDeposts: false,
        getDepositssMessage: action.payload.message,
        deposits: [],
      }
    case GET_DEPOSITS_SUCCESS:
      return {
        ...state,
        isGettingDeposts: false,
        getDepositssMessage: action.payload.message,
        deposits: action.payload.data,
      }
    case GET_DEPOSITS_RESET:
      return {
        ...state,
        isGettingDeposts: false,
        getDepositssMessage: '',
        deposits: [],
      }
    case DEPOSIT_REQUEST:
      return {
        ...state,
        isDepositing: true,
      }
    case DEPOSIT_FAILURE:
      return {
        ...state,
        isDepositing: false,
        depositMessage: action.payload.message,
        depositDetails: null,
        payPalLink: null,
      }
    case DEPOSIT_SUCCESS:
      return {
        ...state,
        isDepositing: false,
        depositMessage: action.payload.message,
        depositDetails: action.payload.deposit,
        payPalLink: action.payload.url,
      }
    case DEPOSIT_RESET:
      return {
        ...state,
        isDepositing: false,
        depositMessage: '',
        depositDetails: null,
        payPalLink: null,
      }
    default:
      return state
  }
}
