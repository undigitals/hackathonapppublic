import {
  GET_RELATIVES_REQUEST,
  GET_RELATIVES_SUCCESS,
  GET_RELATIVES_FAILURE,
  GET_RELATIVES_RESET,
  ADD_RELATIVE_REQUEST,
  ADD_RELATIVE_SUCCESS,
  ADD_RELATIVE_FAILURE,
  ADD_RELATIVE_RESET,
  DELETE_RELATIVE_REQUEST,
  DELETE_RELATIVE_SUCCESS,
  DELETE_RELATIVE_FAILURE,
  DELETE_RELATIVE_RESET,
} from '../actions/types'

const initialState = {
  // getRelatives
  isGettingRelatives: false,
  getRelativesMessage: '',
  relatives: [],

  // addRelative
  isAddingRelative: false,
  addRelativeMessage: '',
  addRelativeData: null,

  // deleteRelative
  isDeletingRelative: false,
  deleteRelativeMessage: '',
}

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_RELATIVES_REQUEST:
      return {
        ...state,
        isGettingRelatives: true,
      }
    case GET_RELATIVES_FAILURE:
      return {
        ...state,
        isGettingRelatives: false,
        getRelativesMessage: action.payload.message,
        relatives: [],
      }
    case GET_RELATIVES_SUCCESS:
      return {
        ...state,
        isGettingRelatives: false,
        getRelativesMessage: action.payload.message,
        relatives: action.payload.data,
      }
    case GET_RELATIVES_RESET:
      return {
        ...state,
        isGettingRelatives: false,
        getRelativesMessage: '',
        relatives: [],
      }
    case ADD_RELATIVE_REQUEST:
      return {
        ...state,
        isAddingRelative: true,
      }
    case ADD_RELATIVE_FAILURE:
      return {
        ...state,
        isAddingRelative: false,
        addRelativeMessage: action.payload.message,
        addRelativeData: null,
      }
    case ADD_RELATIVE_SUCCESS:
      return {
        ...state,
        isAddingRelative: false,
        addRelativeMessage: action.payload.message,
        addRelativeData: action.payload.data,
      }
    case ADD_RELATIVE_RESET:
      return {
        ...state,
        isAddingRelative: false,
        addRelativeMessage: '',
        addRelativeData: null,
      }
    case DELETE_RELATIVE_REQUEST:
      return {
        ...state,
        isDeletingRelative: true,
      }
    case DELETE_RELATIVE_FAILURE:
      return {
        ...state,
        isDeletingRelative: false,
        deleteRelativeMessage: action.payload.message,
      }
    case DELETE_RELATIVE_SUCCESS:
      return {
        ...state,
        isDeletingRelative: false,
        deleteRelativeMessage: action.payload.message,
      }
    case DELETE_RELATIVE_RESET:
      return {
        ...state,
        isDeletingRelative: false,
        deleteRelativeMessage: '',
      }
    default:
      return state
  }
}
