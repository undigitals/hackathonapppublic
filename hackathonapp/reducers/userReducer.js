import {
  SET_USER,
  GET_USER,
  GET_USER_RESET,
  REMOVE_USER,
  REMOVE_USER_RESET,
  SET_TEMP_MAINTENANCE,
  REFRESH_USER_REQUEST,
  REFRESH_USER_REQUEST01,
  REFRESH_USER_FAILURE,
  REFRESH_USER_SUCCESS,
  REFRESH_USER_RESET,
  UPDATE_LANG_REQUEST,
  UPDATE_LANG_FAILURE,
  UPDATE_LANG_SUCCESS,
  UPDATE_LANG_RESET,
  UPDATE_NOTIF_REQUEST,
  UPDATE_NOTIF_FAILURE,
  UPDATE_NOTIF_SUCCESS,
  UPDATE_NOTIF_RESET,
  GET_UPDATE_PHONE_VERIF_REQUEST,
  GET_UPDATE_PHONE_VERIF_SUCCESS,
  GET_UPDATE_PHONE_VERIF_FAILURE,
  GET_UPDATE_PHONE_VERIF_RESET,
  SEND_UPDATE_PHONE_VERIF_REQUEST,
  SEND_UPDATE_PHONE_VERIF_SUCCESS,
  SEND_UPDATE_PHONE_VERIF_FAILURE,
  SEND_UPDATE_PHONE_VERIF_RESET,
} from '../actions/types'

const initialState = {
  data: {},
  savedToken: null,
  getUserMessage: '',
  userTokenExpired: false,
  tempMaintenance: false,

  isRefreshingUser: false,
  isRefreshingUser01: false,
  refreshUserMessage: '',
  // updateLang
  isUpdatingLang: false,
  updateLangMessage: '',
  // updateNotif
  isUpdatingNotif: false,
  updateNotifMessage: '',
  // user-phone-update
  isGettingUpdateVerif: false,
  isSendingUpdateVerif: false,
  getUpdateVerifMessage: '',
  sendUpdateVerifMessage: '',
}

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        data: action.payload,
      }
    case GET_USER:
      return {
        ...state,
        getUserMessage: action.payload.message,
        savedToken: action.payload.data,
      }
    case GET_USER_RESET:
      return {
        ...state,
        getUserMessage: '',
        savedToken: null,
      }
    case REMOVE_USER:
      return {
        ...state,
        userTokenExpired: true,
        data: {},
        savedToken: '',
        getUserMessage: '',
      }
    case REMOVE_USER_RESET:
      return {
        ...state,
        data: {},
        savedToken: '',
        getUserMessage: '',
        userTokenExpired: false,
      }
    case SET_TEMP_MAINTENANCE:
      return {
        ...state,
        tempMaintenance: action.payload,
      }
    case REFRESH_USER_REQUEST:
      return {
        ...state,
        isRefreshingUser: true,
      }
    case REFRESH_USER_REQUEST01:
      return {
        ...state,
        isRefreshingUser01: true,
      }
    case REFRESH_USER_FAILURE:
      return {
        ...state,
        isRefreshingUser: false,
        isRefreshingUser01: false,
        refreshUserMessage: action.payload.message,
      }
    case REFRESH_USER_SUCCESS:
      return {
        ...state,
        isRefreshingUser: false,
        isRefreshingUser01: false,
        refreshUserMessage: action.payload.message,
        data: action.payload.data,
      }
    case REFRESH_USER_RESET:
      return {
        ...state,
        isRefreshingUser: false,
        isRefreshingUser01: false,
        refreshUserMessage: '',
        data: {},
      }
    case UPDATE_LANG_REQUEST:
      return {
        ...state,
        isUpdatingLang: true,
      }
    case UPDATE_LANG_FAILURE:
      return {
        ...state,
        isUpdatingLang: false,
        updateLangMessage: action.payload.message,
      }
    case UPDATE_LANG_SUCCESS:
      return {
        ...state,
        isUpdatingLang: false,
        updateLangMessage: action.payload.message,
      }
    case UPDATE_LANG_RESET:
      return {
        ...state,
        isUpdatingLang: false,
        updateLangMessage: '',
      }
    case UPDATE_NOTIF_REQUEST:
      return {
        ...state,
        isUpdatingNotif: true,
      }
    case UPDATE_NOTIF_FAILURE:
      return {
        ...state,
        isUpdatingNotif: false,
        updateNotifMessage: action.payload.message,
      }
    case UPDATE_NOTIF_SUCCESS:
      return {
        ...state,
        isUpdatingNotif: false,
        updateNotifMessage: action.payload.message,
      }
    case UPDATE_NOTIF_RESET:
      return {
        ...state,
        isUpdatingNotif: false,
        updateNotifMessage: '',
      }
    case GET_UPDATE_PHONE_VERIF_REQUEST:
      return {
        ...state,
        isGettingUpdateVerif: true,
      }
    case GET_UPDATE_PHONE_VERIF_FAILURE:
      return {
        ...state,
        isGettingUpdateVerif: false,
        getUpdateVerifMessage: action.payload.message,
      }
    case GET_UPDATE_PHONE_VERIF_SUCCESS:
      return {
        ...state,
        isGettingUpdateVerif: false,
        getUpdateVerifMessage: action.payload.message,
      }
    case GET_UPDATE_PHONE_VERIF_RESET:
      return {
        ...state,
        getUpdateVerifMessage: '',
        isGettingUpdateVerif: false,
        isSendingUpdateVerif: false,
        sendUpdateVerifMessage: '',
      }
    case SEND_UPDATE_PHONE_VERIF_REQUEST:
      return {
        ...state,
        isSendingUpdateVerif: true,
      }
    case SEND_UPDATE_PHONE_VERIF_FAILURE:
      return {
        ...state,
        isSendingUpdateVerif: false,
        sendUpdateVerifMessage: action.payload.message,
      }
    case SEND_UPDATE_PHONE_VERIF_SUCCESS:
      return {
        ...state,
        isSendingUpdateVerif: false,
        sendUpdateVerifMessage: action.payload.message,
        data: action.payload.data,
      }
    case SEND_UPDATE_PHONE_VERIF_RESET:
      return {
        ...state,
        sendUpdateVerifMessage: '',
        isSendingUpdateVerif: false,
      }
    default:
      return state
  }
}
