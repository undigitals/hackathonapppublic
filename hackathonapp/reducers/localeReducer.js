import { CHANGE_LANG } from '../actions/types'

const initialState = {
  lang: 'en',
}

export default function(state = initialState, action) {
  switch (action.type) {
    case CHANGE_LANG:
      return {
        ...state,
        lang: action.payload,
      }
    default:
      return state
  }
}
