import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGIN_RESET,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  REGISTER_RESET,
  GOOGLE_SIGN_IN_REQUEST,
  GOOGLE_SIGN_IN_FAILURE,
  GOOGLE_SIGN_IN_SUCCESS,
  GOOGLE_SIGN_IN_RESET,
  FB_SIGN_IN_REQUEST,
  FB_SIGN_IN_FAILURE,
  FB_SIGN_IN_SUCCESS,
  FB_SIGN_IN_RESET,
} from '../actions/types'

const initialState = {
  // login
  isLoggingIn: false,
  loginMessage: '',
  // register
  isRegistering: false,
  registerMessage: '',
  // signInWithGoogle
  isSigningInWithGoogle: false,
  signInWithGoogleMessage: '',
  // signInWithFacebook
  isSigningInWithFacebook: false,
  signInWithFacebookMessage: '',
}

export default function(state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        isLoggingIn: true,
      }
    case LOGIN_FAILURE:
      return {
        ...state,
        loginMessage: action.payload.message,
        isLoggingIn: false,
      }
    case LOGIN_SUCCESS:
      return {
        ...state,
        loginMessage: action.payload.message,
        isLoggingIn: false,
      }
    case LOGIN_RESET:
      return {
        ...state,
        loginMessage: '',
        isLoggingIn: false,
      }
    case REGISTER_REQUEST:
      return {
        ...state,
        isRegistering: true,
      }
    case REGISTER_FAILURE:
      return {
        ...state,
        registerMessage: action.payload.message,
        isRegistering: false,
      }
    case REGISTER_SUCCESS:
      return {
        ...state,
        registerMessage: action.payload.message,
        isRegistering: false,
      }
    case REGISTER_RESET:
      return {
        ...state,
        registerMessage: '',
        isRegistering: false,
      }
    case GOOGLE_SIGN_IN_REQUEST:
      return {
        ...state,
        isSigningInWithGoogle: true,
      }
    case GOOGLE_SIGN_IN_FAILURE:
      return {
        ...state,
        signInWithGoogleMessage: action.payload.message,
        isSigningInWithGoogle: false,
      }
    case GOOGLE_SIGN_IN_SUCCESS:
      return {
        ...state,
        signInWithGoogleMessage: action.payload.message,
        isSigningInWithGoogle: false,
      }
    case GOOGLE_SIGN_IN_RESET:
      return {
        ...state,
        signInWithGoogleMessage: '',
        isSigningInWithGoogle: false,
      }
    case FB_SIGN_IN_REQUEST:
      return {
        ...state,
        isSigningInWithFacebook: true,
      }
    case FB_SIGN_IN_FAILURE:
      return {
        ...state,
        isSigningInWithFacebook: false,
        signInWithFacebookMessage: action.payload.message,
      }
    case FB_SIGN_IN_SUCCESS:
      return {
        ...state,
        isSigningInWithFacebook: false,
        signInWithFacebookMessage: action.payload.message,
      }
    case FB_SIGN_IN_RESET:
      return {
        ...state,
        isSigningInWithFacebook: false,
        signInWithFacebookMessage: '',
      }
    default:
      return state
  }
}
