import {
  SEARCH_FLIGHT_REQUEST,
  SEARCH_FLIGHT_FAILURE,
  SEARCH_FLIGHT_SUCCESS,
  SEARCH_FLIGHT_RESET,
  GET_MSGS_REQUEST,
  GET_MSGS_FAILURE,
  GET_MSGS_SUCCESS,
  GET_MSGS_RESET,
  CREATE_MSG_REQUEST,
  CREATE_MSG_FAILURE,
  CREATE_MSG_SUCCESS,
  CREATE_MSG_RESET,
  DELETE_MSG_REQUEST,
  DELETE_MSG_FAILURE,
  DELETE_MSG_SUCCESS,
  DELETE_MSG_RESET,
  NEW_MSG_UPDATE,
  NEW_MSG_RESET,
} from '../actions/types'

const initialState = {
  // newMsg
  newMsg: null,

  // searchFlight
  isSearchingFlight: false,
  searchFlightMessage: '',
  searchFlightData: null,

  // getMsgs
  isGettingMsgs: false,
  getMsgsMessage: '',
  messages: [],

  // createMsg
  isCreatingMsg: false,
  createMsgMessage: '',
  createMsgData: null,

  // deleteMsg
  isDeletingMsg: false,
  deleteMsgMessage: '',
}

export default function(state = initialState, action) {
  switch (action.type) {
    case NEW_MSG_UPDATE:
      return {
        ...state,
        newMsg: action.payload,
      }
    case NEW_MSG_RESET:
      return {
        ...state,
        newMsg: null,
      }
    case SEARCH_FLIGHT_REQUEST:
      return {
        ...state,
        isSearchingFlight: true,
      }
    case SEARCH_FLIGHT_FAILURE:
      return {
        ...state,
        isSearchingFlight: false,
        searchFlightMessage: action.payload.message,
        searchFlightData: null,
      }
    case SEARCH_FLIGHT_SUCCESS:
      return {
        ...state,
        isSearchingFlight: false,
        searchFlightMessage: action.payload.message,
        searchFlightData: action.payload.data,
      }
    case SEARCH_FLIGHT_RESET:
      return {
        ...state,
        isSearchingFlight: false,
        searchFlightMessage: '',
        searchFlightData: null,
      }
    case GET_MSGS_REQUEST:
      return {
        ...state,
        isGettingMsgs: true,
      }
    case GET_MSGS_FAILURE:
      return {
        ...state,
        isGettingMsgs: false,
        getMsgsMessage: action.payload.message,
        messages: [],
      }
    case GET_MSGS_SUCCESS:
      return {
        ...state,
        isGettingMsgs: false,
        getMsgsMessage: action.payload.message,
        messages: action.payload.data,
      }
    case GET_MSGS_RESET:
      return {
        ...state,
        isGettingMsgs: false,
        getMsgsMessage: '',
        messages: [],
      }
    case CREATE_MSG_REQUEST:
      return {
        ...state,
        isCreatingMsg: true,
      }
    case CREATE_MSG_FAILURE:
      return {
        ...state,
        isCreatingMsg: false,
        createMsgMessage: action.payload.message,
        createMsgData: null,
      }
    case CREATE_MSG_SUCCESS:
      return {
        ...state,
        isCreatingMsg: false,
        createMsgMessage: action.payload.message,
        createMsgData: action.payload.data,
      }
    case CREATE_MSG_RESET:
      return {
        ...state,
        isCreatingMsg: false,
        createMsgMessage: '',
        createMsgData: null,
      }
    case DELETE_MSG_REQUEST:
      return {
        ...state,
        isDeletingMsg: true,
      }
    case DELETE_MSG_FAILURE:
      return {
        ...state,
        isDeletingMsg: false,
        deleteMsgMessage: action.payload.message,
      }
    case DELETE_MSG_SUCCESS:
      return {
        ...state,
        isDeletingMsg: false,
        deleteMsgMessage: action.payload.message,
      }
    case DELETE_MSG_RESET:
      return {
        ...state,
        isDeletingMsg: false,
        deleteMsgMessage: '',
      }
    default:
      return state
  }
}
