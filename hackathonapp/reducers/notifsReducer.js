import {
  GET_NOTIFS_REQUEST,
  GET_NOTIFS_FAILURE,
  GET_NOTIFS_SUCCESS,
  GET_NOTIFS_RESET,
  REFRESH_NOTIFS_REQUEST,
  GET_MORE_NOTIFS_REQUEST,
  GET_MORE_NOTIFS_FAILURE,
  GET_MORE_NOTIFS_SUCCESS,
  GET_MORE_NOTIFS_RESET,
  UPDATE_BADGE_COUNT,
} from '../actions/types'

const initialState = {
  page: 1,
  perPage: 20,
  hasNextPage: true,
  isGettingNotifs: false,
  isGettingMoreNotifs: false,
  isRefreshingNotifs: false,
  getNotifsMessage: '',
  getMoreNotifsMessage: '',
  notifsList: [],
  totalBadges: 0,
}

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_NOTIFS_REQUEST:
      return {
        ...state,
        isGettingNotifs: true,
      }
    case GET_NOTIFS_FAILURE:
      return {
        ...state,
        isGettingNotifs: false,
        isRefreshingNotifs: false,
        getNotifsMessage: action.payload.message,
      }
    case GET_NOTIFS_SUCCESS:
      return {
        ...state,
        isGettingNotifs: false,
        isRefreshingNotifs: false,
        getNotifsMessage: action.payload.message,
        notifsList: action.payload.data,
        page: action.payload.page,
        hasNextPage: action.payload.hasNextPage,
      }
    case REFRESH_NOTIFS_REQUEST:
      return {
        ...state,
        isRefreshingNotifs: true,
      }
    case GET_MORE_NOTIFS_REQUEST:
      return {
        ...state,
        isGettingMoreNotifs: true,
      }
    case GET_MORE_NOTIFS_FAILURE:
      return {
        ...state,
        isGettingMoreNotifs: false,
        getMoreNotifsMessage: action.payload.message,
      }
    case GET_MORE_NOTIFS_SUCCESS:
      return {
        ...state,
        isGettingMoreNotifs: false,
        getMoreNotifsMessage: action.payload.message,
        notifsList: [...state.notifsList, ...action.payload.data],
        page: action.payload.page,
        hasNextPage: action.payload.hasNextPage,
      }
    case UPDATE_BADGE_COUNT:
      return {
        ...state,
        totalBadges: action.payload,
      }
    default:
      return state
  }
}
