import { combineReducers } from 'redux'
import localeReducer from './localeReducer'
import userReducer from './userReducer'
import authReducer from './authReducer'
import notifsReducer from './notifsReducer'
import flightReducer from './flightReducer'
import relativeReducer from './relativeReducer'
import depositReducer from './depositReducer'

export default combineReducers({
  locale: localeReducer,
  user: userReducer,
  auth: authReducer,
  notifs: notifsReducer,
  flight: flightReducer,
  relative: relativeReducer,
  deposit: depositReducer,
})
