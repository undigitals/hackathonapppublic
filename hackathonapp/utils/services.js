import uuid from 'uuid'
export const services = [
  {
    id: uuid.v4(),
    name: 'Books',
    description:
      'You can rent, share or give away your unused books or text materials.',
    image: 'https://image.flaticon.com/icons/png/512/167/167756.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
    sections: [
      {
        id: uuid.v4(),
        name: '',
        image: '',
        items: [
          {
            id: uuid.v4(),
            name: '',
            price: '',
            seller: {
              name: '',
              email: '',
            },
          },
        ],
      },
      {
        id: uuid.v4(),
        name: '',
        image: '',
        items: [
          {
            id: uuid.v4(),
            name: '',
            price: '',
            seller: {
              name: '',
              email: '',
            },
          },
        ],
      },
    ],
  },
  {
    id: uuid.v4(),
    name: 'Sports',
    description: 'You can share or rent some sports related equipment',
    image: 'https://image.flaticon.com/icons/png/512/2411/2411098.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
  },
  {
    id: uuid.v4(),
    name: 'Tools',
    description:
      'You can share or exchange items that you use in your labs or projects.',
    image: 'https://image.flaticon.com/icons/png/512/950/950111.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
  },
  {
    id: uuid.v4(),
    name: 'Clothes',
    description: 'You can share clothes.',
    image: 'https://image.flaticon.com/icons/png/512/2393/2393179.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
  },
  {
    id: uuid.v4(),
    name: 'Sports',
    description: 'You can share or rent some sports related equipment',
    image: 'https://image.flaticon.com/icons/png/512/2411/2411098.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
  },
  {
    id: uuid.v4(),
    name: 'Sports',
    description: 'You can share or rent some sports related equipment',
    image: 'https://image.flaticon.com/icons/png/512/2411/2411098.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
  },
  {
    id: uuid.v4(),
    name: 'Clothes',
    description: 'You can share clothes.',
    image: 'https://image.flaticon.com/icons/png/512/2393/2393179.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
  },
  {
    id: uuid.v4(),
    name: 'Clothes',
    description: 'You can share clothes.',
    image: 'https://image.flaticon.com/icons/png/512/2393/2393179.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
  },
]
