import { Notifications } from 'expo'

const createLocalNotif = async data => {
  const notifConfig = {
    android: {
      sound: true,
    },
    ios: {
      sound: true,
      _displayInForeground: true,
    },
  }

  let localNotification = Object.assign({}, data, { ...notifConfig })

  // console.log(localNotification)
  const result = await Notifications.presentLocalNotificationAsync(
    localNotification
  )
}

export default createLocalNotif
