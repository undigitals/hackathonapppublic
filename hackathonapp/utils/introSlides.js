import uuid from 'uuid'
export const introSlides = [
  {
    key: uuid.v4(),
    title: 'slide01Title',
    text: 'slide01Text',
    icon: 'colours',
    colors: {
      // light: ['#29ABE2', '#4F00BC'],
      light: ['#fff', '#fff'],
      dark: ['#000', '#000'],
    },
  },
  {
    key: uuid.v4(),
    title: 'slide02Title',
    text: 'slide02Text',
    icon: 'fingerprint',
    colors: {
      // light: ['#29ABE2', '#4F00BC'],
      light: ['#fff', '#fff'],
      dark: ['#000', '#000'],
    },
  },
  {
    key: uuid.v4(),
    title: 'slide02Title',
    text: 'slide03Text',
    icon: 'flash',
    colors: {
      // light: ['#29ABE2', '#4F00BC'],
      light: ['#fff', '#fff'],
      dark: ['#000', '#000'],
    },
  },
]
