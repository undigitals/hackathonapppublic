import uuid from 'uuid'
export default [
  {
    id: uuid.v4(),
    icon: 'colours',
    text: 'settingDisplay',
    to: 'Display',
  },
  {
    id: uuid.v4(),
    icon: 'globe',
    text: 'settingChangeLanguage',
    to: 'ChangeLang',
  },
  // {
  //   id: uuid.v4(),
  //   icon: 'list',
  //   text: 'settingDeposits',
  //   to: 'Deposits',
  // },
  {
    id: uuid.v4(),
    icon: 'help-with-circle',
    text: 'settingHelp',
    to: 'Help',
  },
  {
    id: uuid.v4(),
    icon: 'share',
    text: 'settingShare',
    to: 'Share',
  },
  {
    id: uuid.v4(),
    icon: 'text-document-inverted',
    text: 'settingTerms',
    to: 'Terms',
  },
  {
    id: uuid.v4(),
    icon: 'log-out',
    text: 'settingLogout',
    to: 'Logout',
  },
]
