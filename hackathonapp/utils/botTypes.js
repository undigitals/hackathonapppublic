import uuid from 'uuid'
export const botTypes = [
  {
    id: uuid.v4(),
    name: 'Academic',
    type: 'academic',
    description: 'Information about academics',
    image: 'https://image.flaticon.com/icons/png/512/947/947494.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
  },
  {
    id: uuid.v4(),
    name: 'Foreign affairs',
    type: 'foreigner',
    description: 'Information about foreign affairs',
    image: 'https://image.flaticon.com/icons/png/512/921/921495.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/141f7978-63f9-4d2a-aa21-77c61bb82a1c',
  },
  {
    id: uuid.v4(),
    name: 'Food',
    type: 'food',
    description: 'Information about food',
    image: 'https://image.flaticon.com/icons/png/512/706/706164.png',
    link:
      'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
  },
  // {
  //   id: uuid.v4(),
  //   name: 'University guide',
  //   type: '',
  //   description: 'Information about university',
  //   image: 'https://image.flaticon.com/icons/png/512/1705/1705312.png',
  //   link:
  //     'https://console.dialogflow.com/api-client/demo/embedded/21b07058-d385-42fe-9f7a-b83fef89c0f7',
  // },
]
