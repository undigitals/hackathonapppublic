import uuid from 'uuid'
export default [
  {
    id: uuid.v4(),
    icon: 'adjust',
    text: 'settingTheme',
    to: 'Theme',
  },
  {
    id: uuid.v4(),
    icon: 'feather',
    text: 'settingFont',
    to: 'Font',
  },
]
