import * as Haptics from 'expo-haptics'

const feedbackGenerator = type => {
  if (type === 'selection') {
    Haptics.selectionAsync()
  } else if (type === 'impact') {
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Strong)
  } else if (type === 'error') {
    Haptics.notificationAsync(Haptics.NotificationFeedbackType.Error)
  } else if (type === 'warning') {
    Haptics.notificationAsync(Haptics.NotificationFeedbackType.Warning)
  } else if (type === 'success') {
    Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success)
  } else {
  }
}

export default feedbackGenerator
