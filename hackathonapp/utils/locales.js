import uuid from 'uuid'
export default [
  {
    lang: 'en',
    name: 'English',
    imageSrc: null,
    id: uuid.v4(),
  },
  {
    lang: 'ko',
    name: '한국어',
    imageSrc: null,
    id: uuid.v4(),
  },
  {
    lang: 'ru',
    name: 'Русский язык',
    imageSrc: null,
    id: uuid.v4(),
  },
]
