import * as WebBrowser from 'expo-web-browser'

export default openBrowser = async link => {
  // console.log(link)
  let result = await WebBrowser.openBrowserAsync(link)
  // console.log(result)
}
