import uuid from 'uuid'
export default [
  {
    key: 'light',
    name: 'light',
    imageSrc: null,
    id: uuid.v4(),
  },
  {
    key: 'dark',
    name: 'dark',
    imageSrc: null,
    id: uuid.v4(),
  },
]
