import { AsyncStorage } from 'react-native'

const getBookmarks = async () => {
  try {
    const bookmarks = await AsyncStorage.getItem('bookmarks')
    if (!bookmarks) {
      return []
    }
    return JSON.parse(bookmarks).reverse()
  } catch (error) {
    console.log(error)
  }
}

const getBookmarkStatus = async productId => {
  try {
    const bookmarks = await AsyncStorage.getItem('bookmarks')
    if (!bookmarks) return false
    const productIds = JSON.parse(bookmarks).map(product => product.id)
    return productIds.includes(productId)
  } catch (error) {
    console.log(error)
  }
}

const saveToBookmark = async product => {
  try {
    // product.dateAdded = new Date().getTime();
    let updatedBookmarks = []

    const bookmarks = await AsyncStorage.getItem('bookmarks')
    if (bookmarks) updatedBookmarks = JSON.parse(bookmarks)

    updatedBookmarks.push(product)
    await AsyncStorage.setItem('bookmarks', JSON.stringify(updatedBookmarks))
  } catch (error) {
    console.log(error)
  }
}

const removeFromBookmarks = async product => {
  try {
    const bookmarks = await AsyncStorage.getItem('bookmarks')
    const updatedBookmarks = JSON.parse(bookmarks).filter(
      savedproduct => savedproduct.id != product.id
    )

    await AsyncStorage.setItem('bookmarks', JSON.stringify(updatedBookmarks))
  } catch (error) {
    console.log(error)
  }
}

export { getBookmarks, getBookmarkStatus, saveToBookmark, removeFromBookmarks }
