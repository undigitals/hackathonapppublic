import uuid from 'uuid'
export const servicesTemp = [
  {
    id: uuid.v4(),
    name: 'Books',
    desc: 'Some description',
    image: {
      url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
      family: null,
      name: null,
    },
    sections: [
      {
        id: 'kljglkjalkjlkjflkajlkjg',
        name: 'Text books',
        image: {
          url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
          family: null,
          name: null,
        },
        products: [
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
        ],
      },
      {
        id: 'kljglkjalkjg',
        name: 'Text books',
        image: {
          url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
          family: null,
          name: null,
        },
        products: [
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
        ],
      },
    ],
  },
  {
    id: uuid.v4(),
    name: 'Books',
    desc: 'Some description',
    image: {
      url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
      family: null,
      name: null,
    },
    sections: [
      {
        id: uuid.v4(),
        name: 'Text books',
        image: {
          url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
          family: null,
          name: null,
        },
        products: [
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
        ],
      },
      {
        id: uuid.v4(),
        name: 'Text books',
        image: {
          url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
          family: null,
          name: null,
        },
        products: [
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
        ],
      },
    ],
  },
  {
    id: uuid.v4(),
    name: 'Books',
    desc: 'Some description',
    image: {
      url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
      family: null,
      name: null,
    },
    sections: [
      {
        id: uuid.v4(),
        name: 'Text books',
        image: {
          url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
          family: null,
          name: null,
        },
        products: [
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
        ],
      },
      {
        id: uuid.v4(),
        name: 'Text books',
        image: {
          url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
          family: null,
          name: null,
        },
        products: [
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
        ],
      },
    ],
  },
  {
    id: uuid.v4(),
    name: 'Books',
    desc: 'Some description',
    image: {
      url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
      family: null,
      name: null,
    },
    sections: [
      {
        id: uuid.v4(),
        name: 'Text books',
        image: {
          url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
          family: null,
          name: null,
        },
        products: [
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
        ],
      },
      {
        id: uuid.v4(),
        name: 'Text books',
        image: {
          url: 'https://image.flaticon.com/icons/png/512/921/921495.png',
          family: null,
          name: null,
        },
        products: [
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
          {
            id: uuid.v4(),
            name: 'Linear Algebra',
            price: '$25.22',
            seller: {
              name: 'Wahid',
              email: 'wakhid2802@gmail.com',
            },
          },
        ],
      },
    ],
  },
]
