import React from 'react'
import { Platform } from 'react-native'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { BottomTabBar, TabBarIcon, TabBarLabel } from '../components'
import i18n from 'i18n-js'

import HomeStack from './HomeStack'
import GuideStack from './GuideStack'
import SharingStack from './SharingStack'
import AssistantStack from './AssistantStack'
import SettingsStack from './SettingsStack'

const MainTab = createBottomTabNavigator(
  {
    SharingStack: {
      screen: SharingStack,
      navigationOptions: () => {
        return {
          tabBarLabel: ({ focused, tintColor }) => (
            <TabBarLabel text="Sharing" color={tintColor} />
          ),
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="picasa" />
          ),
        }
      },
    },
    AssistantStack: {
      screen: AssistantStack,
      navigationOptions: () => {
        return {
          tabBarLabel: ({ focused, tintColor }) => (
            <TabBarLabel text="Assistant" color={tintColor} />
          ),
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="chat" />
          ),
        }
      },
    },
    HomeStack: {
      screen: HomeStack,
      navigationOptions: () => {
        return {
          tabBarLabel: ({ focused, tintColor }) => (
            <TabBarLabel text="Bulletin" color={tintColor} />
          ),
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="unread" />
          ),
        }
      },
    },
    GuideStack: {
      screen: GuideStack,
      navigationOptions: () => {
        return {
          tabBarLabel: ({ focused, tintColor }) => (
            <TabBarLabel text="Guide" color={tintColor} />
          ),
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="rocket" />
          ),
        }
      },
    },
    SettingsStack: {
      screen: SettingsStack,
      navigationOptions: () => {
        return {
          tabBarLabel: ({ focused, tintColor }) => (
            <TabBarLabel text="Settings" color={tintColor} />
          ),
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="cog" />
          ),
        }
      },
    },
  },
  {
    tabBarComponent: BottomTabBar,
    // initialRouteName: 'HomeStack',
    initialRouteName: 'SharingStack',
    tabBarOptions: {
      keyboardHidesTabBar: false,
    },
  }
)

MainTab.path = ''

export default MainTab
