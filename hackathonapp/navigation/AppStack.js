import { Platform } from 'react-native'
import { createStackNavigator } from 'react-navigation-stack'
import MainTab from './MainTab'
import HomeStack from './HomeStack'
import {
  Notifs,
  Picker,
  VideoRecord,
  VideoReecord,
  VideoPreview,
  VideoExample,
  Gallery,
  VideoUpload,
  VideoMessages,
  ChatBot,
  Bookmarks,
  ProductBookmarked,
  ChatBotOptimized,
} from '../screens'
import { PageTransitioner } from '../components'
import constants from '../constants'
import i18n from 'i18n-js'

const MainStack = createStackNavigator(
  {
    MainTab: {
      screen: MainTab,
      navigationOptions: {
        header: null,
      },
    },
    Bookmarks: {
      screen: Bookmarks,
      navigationOptions: () => {
        return {
          title: i18n.t('Bookmarks'),
        }
      },
    },
    ProductBookmarked: {
      screen: ProductBookmarked,
      navigationOptions: ({ navigation }) => {
        return {
          title: navigation.getParam('headerTitle'),
        }
      },
    },
    Notifs: {
      screen: Notifs,
      navigationOptions: () => {
        return {
          title: i18n.t('Notifs'),
        }
      },
    },
    VideoReecord: {
      screen: VideoReecord,
      headerMode: 'none',
      navigationOptions: {
        header: null,
      },
    },
    Gallery: {
      screen: Gallery,
      headerMode: 'none',
      navigationOptions: {
        header: null,
      },
    },
    VideoUpload: {
      screen: VideoUpload,
      mode: 'modal',
      navigationOptions: () => {
        return {
          title: i18n.t('VideoUpload'),
        }
      },
    },
    VideoMessages: {
      screen: VideoMessages,
      navigationOptions: () => {
        return {
          title: i18n.t('VideoMessages'),
        }
      },
    },
    ChatBot: {
      screen: ChatBot,
      navigationOptions: () => {
        return {
          title: i18n.t('ChatBot'),
        }
      },
    },
    ChatBotOptimized: {
      screen: ChatBotOptimized,
      navigationOptions: ({ navigation }) => {
        return {
          title: navigation.getParam('headerTitle'),
        }
      },
    },
  },
  {
    initialRouteName: 'MainTab',
    mode: 'card',
    headerMode: 'screen',
    headerLayoutPreset: 'center',
    gesturesEnabled: true,
    transitionConfig: Platform.OS === 'android' ? PageTransitioner : null,
    defaultNavigationOptions: ({ screenProps }) => {
      let currentTheme = constants[screenProps.theme]
      let font = screenProps.font
      return {
        headerTintColor: currentTheme.headerTintColor,
        headerPressColorAndroid: currentTheme.headerPressColorAndroid,
        headerBackTitle: null,
        headerTitleAllowFontScaling: false,
        headerStyle: {
          elevation: 0,
          backgroundColor: currentTheme.backgroundColor,
          borderBottomColor: currentTheme.headerBorderBottomColor,
        },
        headerTitleStyle: {
          fontFamily: font,
          fontWeight: Platform.OS === 'android' ? '200' : '700',
        },
      }
    },
  }
)

const AppStack = createStackNavigator(
  {
    Main: {
      screen: MainStack,
      headerMode: 'none',
      navigationOptions: {
        header: null,
      },
    },
    // VideoReecord: {
    //   screen: VideoReecord,
    //   headerMode: 'none',
    //   navigationOptions: {
    //     header: null,
    //   },
    // },
    VideoRecord: {
      screen: VideoRecord,
      headerMode: 'none',
      navigationOptions: {
        header: null,
      },
    },
    VideoPreview: {
      screen: VideoPreview,
      headerMode: 'none',
      navigationOptions: {
        // header: null,
      },
    },
    VideoExample: {
      screen: VideoExample,
      headerMode: 'none',
      navigationOptions: {
        header: null,
      },
    },
    Picker: {
      screen: Picker,
      navigationOptions: {
        headerStyle: {
          elevation: 0,
          elevation: 0,
          shadowOpacity: 0,
          borderBottomWidth: 0,
        },
      },
    },
  },
  {
    mode: 'card',
    headerMode: 'screen',
    gesturesEnabled: true,
    defaultNavigationOptions: {
      headerBackTitle: null,
      headerTitleAllowFontScaling: false,
      headerStyle: {
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
      },
    },
  }
)

export default AppStack
