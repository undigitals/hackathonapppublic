import { Platform } from 'react-native'
import { createStackNavigator } from 'react-navigation-stack'
import { PageTransitioner } from '../components'
import { Intro, Login, Register, ResetPassword, Help } from '../screens'
import i18n from 'i18n-js'
import constants from '../constants'

export default createStackNavigator(
  {
    Intro: {
      screen: Intro,
      navigationOptions: () => {
        return {
          title: i18n.t('Intro'),
          header: null,
        }
      },
    },
    Login: {
      screen: Login,
      navigationOptions: () => {
        return {
          title: i18n.t('Login'),
        }
      },
    },
    Register: {
      screen: Register,
      navigationOptions: () => {
        return {
          title: i18n.t('Register'),
        }
      },
    },
    ResetPassword: {
      screen: ResetPassword,
      navigationOptions: () => {
        return {
          title: i18n.t('ResetPassword'),
        }
      },
    },
    LoginHelp: {
      screen: Help,
      navigationOptions: () => {
        return {
          title: i18n.t('Help'),
        }
      },
    },
  },
  {
    initialRouteName: 'Intro',
    mode: 'card',
    headerMode: 'screen',
    headerLayoutPreset: 'center',
    gesturesEnabled: true,
    transitionConfig: Platform.OS === 'android' ? PageTransitioner : null,
    defaultNavigationOptions: ({ screenProps }) => {
      let currentTheme = constants[screenProps.theme]
      let font = screenProps.font
      return {
        headerTintColor: currentTheme.headerTintColor,
        headerPressColorAndroid: currentTheme.headerPressColorAndroid,
        headerBackTitle: null,
        headerTitleAllowFontScaling: false,
        headerStyle: {
          elevation: 0,
          backgroundColor: currentTheme.backgroundColor,
          borderBottomColor: currentTheme.headerBorderBottomColor,
        },
        headerTitleStyle: {
          fontFamily: font,
          fontWeight: Platform.OS === 'android' ? '200' : '700',
        },
      }
    },
  }
)
