import React from 'react'
import { Platform } from 'react-native'
import { createStackNavigator } from 'react-navigation-stack'
import i18n from 'i18n-js'
import { Assistant, ChatBot } from '../screens'
import { PageTransitioner } from '../components'
import constants from '../constants'

const AssistantStack = createStackNavigator(
  {
    Assistant: {
      screen: Assistant,
      navigationOptions: () => {
        return {
          title: i18n.t('Assistant'),
        }
      },
    },
    // ChatBot: {
    //   screen: ChatBot,
    //   navigationOptions: () => {
    //     return {
    //       title: i18n.t('ChatBot'),
    //     }
    //   },
    // },
  },
  {
    initialRouteName: 'Assistant',
    mode: 'card',
    headerMode: 'screen',
    headerLayoutPreset: 'center',
    gesturesEnabled: true,
    transitionConfig: Platform.OS === 'android' ? PageTransitioner : null,
    defaultNavigationOptions: ({ screenProps }) => {
      let currentTheme = constants[screenProps.theme]
      let font = screenProps.font
      return {
        headerTintColor: currentTheme.headerTintColor,
        headerPressColorAndroid: currentTheme.headerPressColorAndroid,
        headerBackTitle: null,
        headerTitleAllowFontScaling: false,
        headerStyle: {
          elevation: 0,
          backgroundColor: currentTheme.backgroundColor,
          borderBottomColor: currentTheme.headerBorderBottomColor,
        },
        headerTitleStyle: {
          fontFamily: font,
          fontWeight: Platform.OS === 'android' ? '200' : '700',
        },
      }
    },
  }
)

AssistantStack.path = ''

export default AssistantStack
