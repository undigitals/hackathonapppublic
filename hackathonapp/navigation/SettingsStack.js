import React from 'react'
import { Platform } from 'react-native'
import { createStackNavigator } from 'react-navigation-stack'
import i18n from 'i18n-js'
import {
  Settings,
  Display,
  Theme,
  Font,
  ChangePhone,
  ChangeLang,
  Help,
  Deposit,
  Deposits,
} from '../screens'
import { PageTransitioner } from '../components'
import constants from '../constants'

const SettingsStack = createStackNavigator(
  {
    Settings: {
      screen: Settings,
      navigationOptions: () => {
        return {
          title: i18n.t('Settings'),
        }
      },
    },
    Display: {
      screen: Display,
      navigationOptions: () => {
        return {
          title: i18n.t('Display'),
        }
      },
    },
    Theme: {
      screen: Theme,
      navigationOptions: () => {
        return {
          title: i18n.t('Theme'),
        }
      },
    },
    Font: {
      screen: Font,
      navigationOptions: () => {
        return {
          title: i18n.t('Font'),
        }
      },
    },
    ChangePhone: {
      screen: ChangePhone,
      navigationOptions: () => {
        return {
          title: i18n.t('ChangePhone'),
        }
      },
    },
    ChangeLang: {
      screen: ChangeLang,
      navigationOptions: () => {
        return {
          title: i18n.t('ChangeLang'),
        }
      },
    },
    Deposit: {
      screen: Deposit,
      navigationOptions: () => {
        return {
          title: i18n.t('Deposit'),
        }
      },
    },
    Deposits: {
      screen: Deposits,
      navigationOptions: () => {
        return {
          title: i18n.t('Deposits'),
        }
      },
    },
    Help: {
      screen: Help,
      navigationOptions: () => {
        return {
          title: i18n.t('Help'),
        }
      },
    },
  },
  {
    initialRouteName: 'Settings',
    mode: 'card',
    headerMode: 'screen',
    headerLayoutPreset: 'center',
    gesturesEnabled: true,
    transitionConfig: Platform.OS === 'android' ? PageTransitioner : null,
    defaultNavigationOptions: ({ screenProps }) => {
      let currentTheme = constants[screenProps.theme]
      let font = screenProps.font
      return {
        headerTintColor: currentTheme.headerTintColor,
        headerPressColorAndroid: currentTheme.headerPressColorAndroid,
        headerBackTitle: null,
        headerTitleAllowFontScaling: false,
        headerStyle: {
          elevation: 0,
          backgroundColor: currentTheme.backgroundColor,
          borderBottomColor: currentTheme.headerBorderBottomColor,
        },
        headerTitleStyle: {
          fontFamily: font,
          fontWeight: Platform.OS === 'android' ? '200' : '700',
        },
      }
    },
  }
)

SettingsStack.path = ''

export default SettingsStack
