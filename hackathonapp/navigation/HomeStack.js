import { Platform } from 'react-native'
import { createStackNavigator } from 'react-navigation-stack'
import { Home, Flights, UploadMsg } from '../screens'
import { PageTransitioner } from '../components'
import i18n from 'i18n-js'
import constants from '../constants'

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: () => {
        return {
          title: i18n.t('Bulletin'),
        }
      },
    },
    Flights: {
      screen: Flights,
      navigationOptions: () => {
        return {
          title: i18n.t('Flights'),
        }
      },
    },
    UploadMsg: {
      screen: UploadMsg,
      navigationOptions: () => {
        return {
          title: i18n.t('UploadMsg'),
        }
      },
    },
  },
  {
    initialRouteName: 'Home',
    mode: 'card',
    headerMode: 'screen',
    headerLayoutPreset: 'center',
    gesturesEnabled: true,
    transitionConfig: Platform.OS === 'android' ? PageTransitioner : null,
    defaultNavigationOptions: ({ screenProps }) => {
      let currentTheme = constants[screenProps.theme]
      let font = screenProps.font
      return {
        headerTintColor: currentTheme.headerTintColor,
        headerPressColorAndroid: currentTheme.headerPressColorAndroid,
        headerBackTitle: null,
        headerTitleAllowFontScaling: false,
        headerStyle: {
          elevation: 0,
          backgroundColor: currentTheme.backgroundColor,
          borderBottomColor: currentTheme.headerBorderBottomColor,
        },
        headerTitleStyle: {
          fontFamily: font,
          fontWeight: Platform.OS === 'android' ? '200' : '700',
        },
      }
    },
  }
)

HomeStack.path = ''

export default HomeStack
