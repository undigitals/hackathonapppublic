import { createDrawerNavigator } from 'react-navigation-drawer'
import AppStack from './AppStack'
import { DrawerContent } from '../components'

const MainDrawer = createDrawerNavigator(
  {
    MainDrawer: AppStack,
  },
  {
    initialRouteName: 'MainDrawer',
    drawerPosition: 'left',
    drawerType: 'slide',
    contentComponent: DrawerContent,
  }
)

export default MainDrawer
