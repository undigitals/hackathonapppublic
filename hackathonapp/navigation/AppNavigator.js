import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { AuthLoading, Maintenance } from '../screens'
import AuthStack from './AuthStack'
import AppStack from './AppStack'
import MainDrawer from './MainDrawer'

export default createAppContainer(
  createSwitchNavigator({
    AuthLoading,
    Maintenance,
    Auth: AuthStack,
    // App: MainDrawer,
    App: AppStack,
  })
)
