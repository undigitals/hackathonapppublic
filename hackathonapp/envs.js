export const envs = [
  {
    type: 'academic',
    dialogflowConfig: {
      type: 'service_account',
      project_id: 'anton-elhpjx',
      private_key_id: '4ef6299c5fdc0b1fcf41ea07fcf4a9efcf6adf43',
      private_key:
        '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCH1LZLkfOVkDdv\nBbCR2S8QizGCPq4HOD7LvU+nzUpcVHSpfrh2c1cgyUbqz6rzxhMAnjQyGRaLOFW6\n+lRT4y+SjHqk/SchplF0/1pvlrjFZj2jjjtbxlul6+U6OHVKpJ4lC7eGMTZSajJ6\nF6pUgwmnJ5OX5pYbrFyAS2Mn52i//0sUFLdro4YSLFcIUjYyEDfuc+0OhWGodWsQ\nv56wjaHfzK0YE3gJ3I1/P4SfCsv5QWZQBoOhLWJBkaj6ey6wzNTgYsjIs6kZX6RG\n1PnV/lZOCZAmBAkaqOm3/D3OU2vZq8xXE8aXX1AahEcEhBPWa4Aep9WPUyUU5rqF\nFQ9UqmY9AgMBAAECggEABnqmhJ2xi7GFe2W/LoEFTJnSVdigjrA0cQQWn+ko28hp\niJY5vU8ZWYzp6MeluDThpJA0HcZyYiStrjQRQEkgQUv/8HPLR8C6HPiublxqc69B\ntgk4TdN9FqgiCiNpF0Z/7Q6m6+wLOWYrnAUZ9yfWMFaw1+uQ8g2VAfrWmXyUoHSl\nHg5xaZmt3sVWbko5E6k0PqE3vOewh4Bn7Nd2tB2LnejoRE8b+npEtcl9JCYClnk9\nTDMr+DsmcAxGGKEfyIfBvhPpvWnvCpoNQibesW2w3JrLowbOvn5lKcmQNu+GuS5F\neeHEyhOJAteiUGaSV27o/e6l7AFT0hQWB3LoP92f0QKBgQC/zJ5E01dy9BsBhzrt\nhDPp3j31sV5xEZUoM/c82KqnABfbgq6ZReKjy5l451ahibfDiIHj0oXlAKwWv5i/\n6lOC1NDHUDzsSkC5f92QvuFapWy4R45GZKk80xhv0VOyaTez0f7of1mdKKgqboSL\nxwwCYJ9nOoKavg8HFXTlMeQ9lQKBgQC1TCI7uWolxKZsyJ5VV0EH1sA3tDM+x595\nke1Lh5i10ZYYU/keaZS2eWp3tm4lVdA6eKrk/RnvS5lX4d38J+vOe/QHLRn9wyHo\nzaeWZps1/XavzlcWKG4kU1vfkgVpNbzqvQJvqwgCZb2FTB37vTcuqivJg0R2or85\nGJBs3OxMCQKBgGVPNrZSD9oHeJBZFtZIw4cfvgieNK1yxR+kO07nf1SQ5wkqwQDy\n+iDctWc4gvj2npto6nslQ1ZQAZZOg+ezZYMw2Eq/Zj7FheS11pnTSYOw53l7SnqC\nRyZ6tmix4AzYKPUR0hyK0BIEXLd1Aww2CnDjCq/jJwOHqrZqLgG2VcH1AoGACP+3\nb0l7yFYjgKDQoyHKm44ASoOKS6Jy+wDQS4B2hwfZMGnyopzRAeGy9DA89WN2C4Dz\n0pvxjpwtB1yxvRNqM/yZF0YY1Sy7YSfcGq4LJDnpiz5tui7VkL4GgGlqFVfSgcBR\n/6L4qc3NA3HMxzMUKx/NPppiapUXcskSWiT5IakCgYBM5dDZ+5vgY8v/9iBqBAc6\neIvbue82Rd122Byh4Zp9K/KPDxDv1bTphLoGlyScn5H7pGq4PWyDc++or85ICaIm\nxsiJRwUd9HgaGw2GvSENhML/2Rz/C3gqVMKv+21f5JlOtFAs0U7rxeSHeEeCFlK+\nh8hH9x4gltkJKqru7Ra7nA==\n-----END PRIVATE KEY-----\n',
      client_email: 'academic@anton-elhpjx.iam.gserviceaccount.com',
      client_id: '109990830709720723237',
      auth_uri: 'https://accounts.google.com/o/oauth2/auth',
      token_uri: 'https://oauth2.googleapis.com/token',
      auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
      client_x509_cert_url:
        'https://www.googleapis.com/robot/v1/metadata/x509/academic%40anton-elhpjx.iam.gserviceaccount.com',
    },
  },
  {
    type: 'foreigner',
    dialogflowConfig: {
      type: 'service_account',
      project_id: 'anton-foreign-efrfaj',
      private_key_id: 'bda219f643b944bd46d343f872df8a1b87262858',
      private_key:
        '-----BEGIN PRIVATE KEY-----MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDmtSxE7k0p7Bvg\nRdPdB3KUsL2uBRlFNt08hSj4KjyZM4apjCveFrcWJdQ+XwVucSy0uofc8tkqvHnC\ntinNeMLadHULjc5+WBskdbtXNURc8PPuEyzwt37eT04ZNsxVsfMoEXUMRowr1SKc\nXDV/FWvqdHzUcyWliZ9gmaPzwwYIKGx4nnq9XjO8demlWIkiEYRsWws42khkU60D\n9Jxx/y5U5ztHDsUkreEvV1/7g/POocG6Kf0P9y5KzYNOxYHz6YgoM0GOkEwCbYVm\nPvdlaK44JorMoya5RMwzgLKuE8qlOmcZSUne3BYXubAL8IoXbetP/F1JfAxpP8mI\n/FxAdoIJAgMBAAECggEAEQmjbK5EFtLb3qjNfALdKn+wbFnuSzMhbMMhOs2EtuQs\nH1iNk0vOZtVxmo8SI706KdkmKDIriUlMvS1OgkD3M+Kak7k73Q8E5yEGLisuAqad\nPxFdteO5O/HzoK9QMQpwnH6IjDjoCGMnuJAjN8DFXNXQM7DSwIo8scm65sk56G9u\n4i4K3Ath2jYwGNWI8M2ycVeGME8/9TirGSE9NNVF8YWMnoM8FRK8vyqVKAgFInyk\nXxyBVn/OXCQUil9h07U7yrnjNhUI1v0Nyd2lDeNIYkDzRXQc6a+nEo6mj0oEArDi\nqjDrJ4lri8JdPipPx2fTMNVEZOQ5duMmPv6u1oxOwQKBgQD8rTVz1hTKFD1KMOG8\nMbxB9IQPZrYrNfHu3vsPriwcFF6ooHhlGkT6dY/E89UToDHSunUUnl5pcB7ZMr2/\nexl64MZwBJiWHwvL4aI/6E7fu7ZvNmswGVxbBT9qvpA4Z+pP+4tL93rU+pfcyanJ\nMJeJN6pTQZS+VSsVt8qfM0ckyQKBgQDpvf4KUS/9StW2+MO/Jg2CXKdv4QHsewnL\n2slVHeCjHpvpTrtRCRqD8CuZA5Z01TJ4wsrobiaqFFiwZKUAtBcifChN6fvJlwj0\nOliUk7OlvhufIek2HVX6dl7lOboMo2q3f+9xTQgOviBMPS0EV9wSH86jfoS8+KSK\nVzfRIDFTQQKBgQCpphn3dktrCn/EKmHFm5cMqYFpXcr95DBV83yZoJWZHMwZZ/St\n3R9nEju/Pv9XN/17wRkPCO3W2SK9LeZcg+gtqnFRkKRqVtnyv8HSKsWaTkxFC0KY\nfs6VhkZ4Aw6N8KdPoFJ8ZOMh0EbaRMQsguWbibZgQ+38hO5ynH38uc8QkQKBgQCg\nXCqHpX+JBOhuaWjSIScvgqWUygfp19e2tdleveq5vmw0ukEkrVw1nHYsuhtk+Edy\nNiHDKJ76HI9kJQhW3Tbp/8ge/CI0AZyxAb534IVldnG40SkQaTvtY3xjQyFzPkIe\ntnTthG28v4+k/mt+CVEAVjYMUOZ8RxGLn565qJ5ugQKBgHPUsO+vc990dpR6wtMW\nNj3eRwZsr/B78RR0Xpn4lJLzyY+o8RfxgI2KsBZfhVZmwg0YntoxNatBsxn4xx51\nBn85z/19w4graj5eo5yvmpG2BzEZPhd+LWGgtPV05LA6r8wD4up0EX2SNsYD6DPU\nFT1ePUqqaQwmi9IH/A5wC8hx\n-----END PRIVATE KEY-----\n',
      client_email:
        'anton-bot-932@anton-foreign-efrfaj.iam.gserviceaccount.com',
      client_id: '112078549613117044779',
      auth_uri: 'https://accounts.google.com/o/oauth2/auth',
      token_uri: 'https://oauth2.googleapis.com/token',
      auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
      client_x509_cert_url:
        'https://www.googleapis.com/robot/v1/metadata/x509/anton-bot-932%40anton-foreign-efrfaj.iam.gserviceaccount.com',
    },
  },
  {
    type: 'food',
    dialogflowConfig: {
      type: 'service_account',
      project_id: 'meal-master-tywxug',
      private_key_id: '50a31161475911b58f93c3183f42aa0ad479b9ed',
      private_key:
        '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDPxW7XFooyy/YN\nSGbI2TzPCf+tCagqz79++4ghg+GbZX5+RNcEgbSvhMCszpvLFcasxsX0NMFB7IBm\nEEs/4tS4R+nEy5U/GWMrw5L5qkaiZNi+x+8GEI8g52MmD4FNr/vdIzG29+h6hvSY\nz0zd7Wttbxxws0AguaOdgPvtsu6/7D3+y6/HL1GXJyRvRhf410NzULez1tHr/X9d\nEl8/VHo8MA8lV0keIvTtHekpcLcJKq6iNxtpSoW3hSvoSqBIUW7ZoYKmBpBWizCs\npVNiwYSjgM4p6eZBHG5ib2eNTahGiM0yUPg6UQTBbuzyga1dIk60BY0Pm2fvD75k\nNMcjgtlrAgMBAAECggEAKfMC3gp+/KKRmzzzA7pYvLSYGqfIfFxVnZ3LbnjiJC3p\nzDS59P77LDTxcfKFIgvTgS7g6N1954GIqxK/Hrb9m/dpC4LPL15A16U1F5N5j3At\nEtfrOp49R7dwHU8kHDtb7tocP4TuO07kM7YnHufAuOemJlsKp5DTqAlS1Ds8WSNM\nCGJA0+k0gXC7PgsOjQUXs1HQlU/TeWQ16utVGQdYgO53Iwa1t7kkpls+xhvU4Cn9\nECPvPHcZIqoODr0YE47f4kflyYsbleQViY1QuZANNIQ15Pyhh0lqMuQ48krlOEuF\noiQziTAypUbcF0MrLKIpqIgYt7gtRCbGQASX9XzpgQKBgQD+53FavXqo4PKYsIWP\neITkAq6z0d+fgQfs56qV9lWhhpWwwLzeZ/VdgiBt+bz/Pe8225/4e9ueY+U1pMsl\nk0EetowWIrxgMV7MNJkH2GvVt3zAekmEqUC2z5fs1i9OUqroHOb6STl9neQ6MlWh\nlt9Eeeb795RLqkLu7Pha3i2oLQKBgQDQqh0sDsYkBGu13jRfLKwNPuuc+SyUXh5y\nSC/BxYEjdgKIJ1p61rQB6eikVKSlm5VKsdS/3OeFUbSwIng6Tsbh6fm3tM0HkxIr\nP1g/BnNE93EfxY5NFkDpe6fjhxBGstisK9Izi3yFy3HoVvBBVLUkpEMR3w2DuviQ\nzgKxGyOu9wKBgHyVY8qktzGZGBjWlcCkd9b1FKvglLphlx0LsU0wI3QZ0kmcOfM9\nviegtPbrS5s4ixU1zH7nElTcoCdSztMjoH1BXUBoY8dj+eA2iODTvwiqcHemjQ4g\npvdyiN+BY4eVUeC2V5HWfhnONMDY3w9mLONL7UATJlDA+4vJGC1ZT9lVAoGAJ2KU\nkwZO2+mBsxg35mb2KqHfswdNJMwHDycU7Iws7daOb8iNyXXREDNoV+wqD6AoM6sR\nLw2xHXbolf5LlyXbYDTbj5Mux+k4mrUgIPVEuJXD4m1ViLNUmT1f2RBbQBztzMGQ\nsW34mQBnhaUzQAGPF1Vx22Owl1apKBvaYjdvKs0CgYBLXJF968ElhS/jx2NBTn0Z\nI/UNOmMYascMG/vwroAe20H42/1tPXllJFOHWp9ujORwlTLi/KRpDXRRbi2Gou1K\nb/F2eFat60hx6o6aRP3brYlxeOnBEftdUann2EnMWqTOlIvC95Xs4e1ZgxLCt9DX\n3t99VhsH5ni23yINxH8Pqw==\n-----END PRIVATE KEY-----\n',
      client_email: 'meal-master@meal-master-tywxug.iam.gserviceaccount.com',
      client_id: '110500634679409858833',
      auth_uri: 'https://accounts.google.com/o/oauth2/auth',
      token_uri: 'https://oauth2.googleapis.com/token',
      auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
      client_x509_cert_url:
        'https://www.googleapis.com/robot/v1/metadata/x509/meal-master%40meal-master-tywxug.iam.gserviceaccount.com',
    },
  },
]
