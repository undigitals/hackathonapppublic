import React, { useState, useEffect } from 'react'
import { StatusBar, View, AppState, Platform, AsyncStorage } from 'react-native'
import { AppLoading, Updates } from 'expo'
import { Asset } from 'expo-asset'
import * as Font from 'expo-font'
import { Entypo } from '@expo/vector-icons'
import { ActionSheetProvider } from '@expo/react-native-action-sheet'
import { Provider } from 'react-redux'
import store from './store'
import AppNavigator from './navigation/AppNavigator'
import { ThemeContext } from './context'
import { LocaleProvider } from './locale'
import constants from './constants'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks'

const client = new ApolloClient({
  uri: 'http://138.197.72.22:4466',
})

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false)
  const [theme, setTheme] = useState('light')
  const [font, setFont] = useState('default')
  const [appState, setAppState] = useState(AppState.currentState)
  const [startedBackground, setStartedBackground] = useState(null)

  const changeTheme = nextTheme => {
    setTheme(nextTheme)
    AsyncStorage.setItem('userTheme', nextTheme)
  }

  const changeFont = nextFont => {
    if (nextFont === 'default') {
      setFont(null)
    } else {
      setFont(nextFont)
    }
    AsyncStorage.setItem('userFont', nextFont)
  }

  const handleAppStateChange = nextAppState => {
    if (appState.match(/inactive|background/) && nextAppState === 'active') {
      handleCheckForUpdateAsync()
      const now = Date.now()
      if (startedBackground) {
        if (now - startedBackground > 10 * 60000) {
          Updates.reloadFromCache()
        }
      } else {
      }
    } else if (
      appState.match(/inactive|active/) &&
      nextAppState === 'background'
    ) {
      setStartedBackground(Date.now())
    }
    setAppState(nextAppState)
  }

  useEffect(() => {
    handleTheme(changeTheme)
    handleFont(changeFont)
    AppState.addEventListener('change', state => handleAppStateChange(state))
    return () => {
      AppState.removeEventListener('change', state =>
        handleAppStateChange(state)
      )
    }
  }, [])

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    )
  } else {
    return (
      <Provider store={store}>
        <ThemeContext.Provider
          value={{
            theme: theme,
            font: font,
            changeTheme: changeTheme,
            changeFont: changeFont,
          }}
        >
          <ApolloProvider client={client}>
            <LocaleProvider>
              <ActionSheetProvider>
                <View style={{ flex: 1 }}>
                  <StatusBar
                    barStyle={
                      Platform.OS === 'android' && theme === 'light'
                        ? 'light-content'
                        : constants[theme].statusBarStyle
                    }
                  />
                  <AppNavigator screenProps={{ theme: theme, font: font }} />
                </View>
              </ActionSheetProvider>
            </LocaleProvider>
          </ApolloProvider>
        </ThemeContext.Provider>
      </Provider>
    )
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/robot-dev.png'),
      require('./assets/images/robot-prod.png'),
    ]),
    Font.loadAsync({
      ...Entypo.font,
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
    }),
  ])
}

function handleLoadingError(error) {
  console.warn(error)
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true)
}

async function handleTheme(changeTheme) {
  try {
    const userTheme = await AsyncStorage.getItem('userTheme')
    if (userTheme !== null) {
      changeTheme(userTheme)
    } else {
      changeTheme('light')
    }
  } catch (error) {
    console.log(error)
    changeTheme('light')
  }
}

async function handleFont(changeFont) {
  try {
    const userFont = await AsyncStorage.getItem('userFont')
    if (userFont !== null) {
      changeFont(userFont)
    } else {
      changeFont('default')
    }
  } catch (error) {
    console.log(error)
    changeFont('default')
  }
}

async function handleCheckForUpdateAsync() {
  try {
    const update = await Updates.checkForUpdateAsync()
    if (update.isAvailable) {
      await Updates.fetchUpdateAsync()
      setTimeout(() => {
        Updates.reloadFromCache()
      }, 5000)
    }
  } catch (error) {}
}
